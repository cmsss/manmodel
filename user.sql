-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2017 at 09:28 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kemenagv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(25) NOT NULL DEFAULT '',
  `nama` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `stm` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sid` varchar(100) NOT NULL DEFAULT '',
  `aktif` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `tingkat` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `induk` varchar(25) NOT NULL DEFAULT '',
  `jumlahlogin` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tslogin` bigint(18) UNSIGNED NOT NULL DEFAULT '0',
  `tslogout` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `domain` varchar(60) NOT NULL DEFAULT '',
  `moderator` tinyint(1) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`

--

-- stm , sid , induk , jumlah_login , tslogin,tslogout,ip,domain , moderator

INSERT INTO `tb_user` (`id_user`, `usename`, `display_name`, `password`, `stm`, `sid`, `status`, `level`, `induk`, `jumlahlogin`, `tslogin`, `tslogout`, `ip`, `domain`, `moderator`) VALUES
('admingorontalo', 'admingorontalo' 'Admin Gorontalo', 'c776fe066d5fb3d0f4801eff23bbbdc8', 1467271884, '0', 1, 1, 'admin2', 1935, 1491955888, 1467311433, '10.1.7.240', 'gorontalo.kemenag.go.id', 1),
('kepeg','kepeg' , 'Kepegawaian', '1d10d87459a9940a4ab4c1e54c7c7a62', 0, '', 1, 2, 'admingorontalo', 0, 1484618372, 0, '', 'gorontalo.kemenag.go.id', 0),
('adminsukri', 'adminsukri', 'sukri', '46d045ff5190f6ea93739da6c0aa19bc', 1469005545, '0', 1, 1, 'admingorontalo', 559, 1492561688, 1469005592, '10.1.7.240', 'gorontalo.kemenag.go.id', 1),
('admingorontalo1','admingorontalo1', 'admingorontalo1', '60d9d56df9e449732fcd97020130c502', 1358126992, '0', 1, 1, 'admin2', 12, 1358126874, 1358126992, '182.10.241.157', 'gorontalo.kemenag.go.id', 1),
('pepen', 'pepen', 'adminbonbol', '121068f30e8668cc0d2cd4a1a7804f47', 1468460763, '1849fkdzc1468460554', 1, 2, 'admingorontalo', 586, 1491896213, 1468237054, '10.1.7.240', 'gorontalo.kemenag.go.id', 1),
('adminarie','adminarie', 'Arie', '46d045ff5190f6ea93739da6c0aa19bc', 1468830472, '0', 1, 1, 'admingorontalo', 439, 1492583920, 1468830513, '10.1.7.240', 'gorontalo.kemenag.go.id', 1),
('umard','umard', 'umar', '7a0748012a3a9b42fc76a76d3b634be0', 1469002602, '4791ywncs1469002293', 1, 1, 'admingorontalo', 226, 1491485009, 1468506111, '10.1.7.240', 'gorontalo.kemenag.go.id', 1),
('adminayiz','adminayiz', 'Ayiz', '3bcba9047458549e999754da4ef3fc79', 0, '', 1, 1, 'umard', 0, 1492579681, 0, '', 'gorontalo.kemenag.go.id', 0),
('dindaun', 'dindaun', 'dinda', '9c6a42346730ab5360a936cfe6da340d', 1468210699, '0', 1, 1, 'admingorontalo', 13, 1492660471, 1468250282, '10.1.7.240', 'gorontalo.kemenag.go.id', 1),
('humasgorut','humasgorut', 'Humas Gorut', '7beed3c5d5b7762db932aa94d95b398a', 1457917111, '0', 1, 1, 'admingorontalo', 1, 1457917021, 1457916906, '10.1.7.1', 'gorontalo.kemenag.go.id', 1),
('maryam','maryam', 'maryam', 'd112032c5ec0002ca50f1845d2ed91ab', 1464309023, '0', 1, 2, 'admingorontalo', 7, 1464308757, 1464308875, '10.1.7.240', 'gorontalo.kemenag.go.id', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sid` (`sid`),
  ADD KEY `user_induk_idx` (`induk`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
