<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!function_exists('list_album')) {
    function list_album(){
        $CI = & get_instance();
        $CI->load->model('album/m_album');
        $album = $CI->m_album->get_all();
        if(!empty($album)){
        	foreach ($album as $a) {
        		$album_a['']='Album';
        		$album_a[$a->id_album]=$a->album;
        	}
        }
        return $album_a;
    }
}
if (!function_exists('list_kategori')) {
    function list_kategori(){
        $CI = & get_instance();
        $CI->load->model('kategori/m_kategori');
        $kategori = $CI->m_kategori->get_all();
        if(!empty($kategori)){
        	foreach ($kategori as $k) {
        		$kategori_a['']='Pilih Kategori';
        		$kategori_a[$k->id_kategori]=$k->kategori;
        	}
        }
        return $kategori_a;
    }

}

if (!function_exists('list_id_kategori')) {
    function list_id_kategori(){
        $CI = & get_instance();
        $CI->load->model('dokumen/m_kategory');
        $kategori = $CI->m_kategory->get_all();
        if(!empty($kategori)){
            foreach ($kategori as $k) {
                $kategori_a[$k->id_kategori]= $k->kategori;
            }
        }
        return $kategori_a;
    }
}
