<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!function_exists('get_front')) {

    function get_front($kat, $except = FALSE) {
        $CI = & get_instance();
        $CI->load->model('admin/m_article_detail');
        $one_art = $CI->m_article_detail->get_artikel($kat, $except);
        return $one_art;
    }

}
if (!function_exists('get_kat')) {

    function get_kat($kat) {
        $CI = & get_instance();
        $CI->load->model('admin/m_kategori');
        $kategori = $CI->m_kategori->get_by(array('url_kategori' => $kat));
        return $kategori->id_kategori;
    }

}
if (!function_exists('latest_post')) {

    function latest_post() {
        $CI = & get_instance();
        $CI->load->model('admin/m_article');
        $latest = $CI->m_article->get_latest();
        return $latest;
    }

}
if (!function_exists('latest_komen')) {

    function latest_komen() {
        $CI = & get_instance();
        $CI->load->model('admin/m_komentar');
        $latest = $CI->m_komentar->get_komen();
        return $latest;
    }

}
if (!function_exists('view_komentar')) {

    function view_komentar($id) {
        $CI = & get_instance();
        $CI->load->model('komentar/m_komentar');
        $latest = $CI->m_komentar->get_many_by(array('id_berita' => $id, 'status' => '1'));
        return $latest;
    }

}
if (!function_exists('jumlah_komentar')) {

    function jumlah_komentar($id) {
        $CI = & get_instance();
        $CI->load->model('admin/m_komentar');
        $jml = $CI->m_komentar->get_jml_komen($id);
        return $jml;
    }

}
if (!function_exists('popular')) {

    function popular() {
        $CI = & get_instance();
        $CI->load->model('admin/m_article');
        $popular = $CI->m_article->get_popular();
        return $popular;
    }

}
 if (!function_exists('statistik')) {

     function statistik($date = FALSE) {
         $CI = & get_instance();
         $CI->load->model('admin/m_statistik');
         $popular = $CI->m_statistik->get_stat($date);
         return $popular;
     }

 }
if (!function_exists('insert_statistik')) {

    function insert_statistik($date = FALSE) {
        $CI = & get_instance();
        $CI->load->model('admin/m_statistik');
        $data = array(
            'ip' => $CI->input->ip_address(),
            'date' => date('Y-m-d')
        );
        $CI->m_statistik->insert($data);
    }

}
if (!function_exists('get_kategori')) {

    function get_kategori($id_kat) {
        $CI = & get_instance();
        $CI->load->model('admin/m_kategori');
        $jml_kat = count($id_kat['kategori']);
        for ($i = 0; $i < $jml_kat; $i++) {
            $id_kat[] = $_POST['kategori'][$i];
            $nama_akt = $CI->m_kategori->get_by(array('id_kategori' => $id_kat['kategori'][$i]));
            $nama_arr[] = $nama_akt->nama_kategori;
        }
        $nama_kategori = join(',', $nama_arr);
        return $nama_kategori;
    }

}
if (!function_exists('insert_article_detail')) {

    function insert_article_detail($data) {
        $CI = & get_instance();
        $CI->load->model('admin/m_article_detail');
        $CI->load->model('admin/m_article');
        $id_art = $CI->m_article->get_last_id();
        $jml_kat = count($data['kategori']);
        for ($i = 0; $i < $jml_kat; $i++) {
            $id_kat = $data['kategori'][$i];
            $data_ins = array(
                'id_article' => $id_art,
                'id_kategori' => $id_kat
            );
            $CI->m_article_detail->insert($data_ins);
        }
        $CI->m_article_detail->insert($data);
    }
}
if (!function_exists('iklan')){
    function iklan(){
        $CI =& get_instance();
        $CI->load->model('admin/m_iklan');
        return $CI->m_iklan->get_all();
    }
}
if (!function_exists('hallo_polisi')){
    function hallo_polisi(){
        $CI =& get_instance();
        $CI->load->model('admin/m_hallo');
        return $CI->m_hallo->get_all();
    }
}
if (!function_exists('get_one_foto')){
    function get_one_foto($id){
        $CI =& get_instance();
        $CI->load->model('admin/m_foto');
        $foto = $CI->m_foto->get_by(array('id_album'=>$id));
        return $foto->url;
    }
}
