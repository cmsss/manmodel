<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
define('STATUS_ACTIVATED', '1');
define('STATUS_NOT_ACTIVATED', '0');
define('ALLOW', '1');
define('NOT_ALLOW', '0');

Class Autentifikasi {

    private $ci;
    private $error = array();

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function login($username, $password) {
        if ((strlen($username) > 0) AND (strlen($password) > 0)) {
            if ($userr = $this->ci->m_user->get_by_username($username)) {
                $user = $userr->username;
                $nl = $userr->display_name;
                $status = $userr->status;
                $level = $userr->level;
                $pass = $userr->password;
                if ($pass == md5($password)) {
                    $this->ci->session->set_userdata(array(
                        'user_login' => $user,
                        'user_pass' => $pass,
                        'nama_lengkap' => $nl,
                        'level' => $level,
                        'id_login' => $userr->id_user,
                        'status' => ($status == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED
                    ));
                    
                    if ($status == 0) {
                        echo "<script>alert('User anda belum diaktifkan oleh Administrator, untuk mengaktifkan kembali silahkan hubungi Administrator. Terimakasih !!!');
			window.location=('" . site_url(uri_string()) . "');</script>";
                    } else {
                        return true;
                    }
                }
                echo "<script>alert('Password Salah');
			window.location=('" . site_url(uri_string()) . "');</script>";
            } else {
                echo "<script>alert('Anda bukan member. Silahkan daftar menjadi member!');
			window.location=('" . site_url(uri_string()) . "');</script>";
            }
        }
        return FALSE;
    }

    public function logout() {
        $this->ci->session->set_userdata(array('user_login' => '', 'user_pass' => '', 'nama_lengkap' => '', 'level' => '', 'status' => '','id_login'=>''));
        $this->ci->session->sess_destroy();
    }

    public function sudah_login($activated = TRUE) {
        return $this->ci->session->userdata('status') === ($activated ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED);
    }

    public function role($level = array()) {
        foreach ($level as $key => $val) {
            $status = $this->ci->session->userdata('roles') == $val ? ALLOW : NOT_ALLOW;
            if ($status == 1) {
                break;
            }
        }
        return $status;
    }

}

?>