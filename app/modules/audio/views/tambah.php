<div class="container-fluid">
	<section class="content-header">
		<h4>Tambah Audio</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('adminweb/audio/simpan',array('class'=>'form-horizontal')); ?>
			
			<div class="row form-group">
				<label class="control-label col-sm-2">File Audio</label>
				<div class="col-sm-10">
				<span class="pull-right">Format : mp3</span>
					<input type="file" name="nama" value="" required="required" class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">durasi</label>
				<div class="col-sm-10">
					<?php echo form_input('durasi',set_value('durasi'),'class="form-control" required placeholder="durasi"'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Keterangan</label>
				<div class="col-sm-10">
					<?php echo form_input('keterangan',set_value('keterangan'),'class="form-control" required placeholder="Keterangan"'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Kategori</label>
				<div class="col-sm-10">
					<?php echo form_dropdown('kategori', $kategori, '' , 'class="form-control" required'); ?>
				</div>
			</div>
			
			<div class="row form-group">
				<label class="control-label col-sm-2">Tampil</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="tampil" value="1" checked style="margin-right: 10px;" >
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="tampil" value="0" style="margin-right: 10px; padding-left: 10px;">
						<label class="">Tidak</label>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Setuju</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="setuju" value="1" checked style="margin-right: 10px;" >
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="setuju" value="0" style="margin-right: 10px; padding-left: 10px;">
						<label class="">Tidak</label>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Lengket</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="lengket" value="1" checked style="margin-right: 10px;" >
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="lengket" value="0" style="margin-right: 10px; padding-left: 10px;">
						<label class="">Tidak</label>
					</div>
				</div>
			</div>

		<div class="row form-group">

			<div class="col-sm-12">
				<a href="<?= site_url('adminweb/audio.asp') ?>" class="btn btn-default pull-right">Kembali</a>
				<label class="col-sm-2"></label>
				<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
			</div>
		</div>

		<?php echo  form_close(); ?>
	</section>
</div>
</div>


