
<script type="text/javascript">
	function cek() {
		if($("#kategori1").is(':checked')){
			$("#url1").show();
			$("#url2").hide();
			$("#url3").show();
		}if($("#kategori2").is(':checked')){
			$("#url1").hide();
			$("#url2").show();
			$("#url3").hide();
		}
	}
</script>

<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah Galery</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">
			<?php echo form_open_multipart('',array('class'=>'form-horizontal')); ?>
				<div class="row form-group">
						<label class="control-label col-sm-2">Judul</label>
						<div class="col-sm-10">
							<?php echo form_input('judul',set_value('judul'),'class="form-control" placeholder="Judul"'); ?>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Deskripsi</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="deskripsi" placeholder="Deskripsi"></textarea>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Kategori</label>
						<div class="col-sm-10">
							<input type="radio" name="kategori" id="kategori1" value="Foto" onChange="cek()" ></input>
							<label>Foto</label>
							<input type="radio" name="kategori" id="kategori2" value="Video" onChange="cek()" ></input>
							<label>Video</label>
						</div>
				</div>
				<div class="row form-group" id="url3">
						<label class="control-label col-sm-2">Album</label>
						<div class="col-sm-10">
							<?php echo form_dropdown('album',$l_album,'','class="form-control"'); ?>
						</div>
				</div>
				<div class="row form-group" id="url1">
						<label class="control-label col-sm-2">File</label>
						<div class="col-sm-10">
							<input class="form-control" name="file"  type="file"/>
						</div>
				</div>
				<div class="row form-group" id="url2">
						<label class="control-label col-sm-2">Url</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="url" placeholder="URL" ></input>
							
						</div>
				</div>
				
				<div class="row form-group">
						
						<div class="col-sm-10">
						<a href="<?= site_url('adminweb/galery.asp') ?>" class="btn btn-default">Kembali</a>
							<?php echo form_submit('submit','Tambah Galery','class="btn btn-primary"'); ?>
						</div>
				</div>
				
			<?php echo  form_close(); ?>
		</section>
	</div>
</div>



