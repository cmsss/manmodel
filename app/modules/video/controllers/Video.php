<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Video extends MX_Controller {

    private $module;
    private $redirect;

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

        $this->load->model('m_video');
        $this->load->model('m_fm');
        $this->load->model('m_kategori1');
        $this->module = "video";
        $this->redirect = "adminweb/video.asp";

    }

    public function index() {
        $data['list']   = $this->m_video->get_all_v();
        $data['module'] = $this->module;
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

    

    public function tambah()
    {
        $data['kategori'] = drop_list('m_kategori1' ,'id' ,'nama' , 'Pilih kategori' , 'get_all' ,'asc');
        $data['module'] = $this->module;
        $data['view_file']   = 'tambah';
        echo Modules::run('template/render_master' , $data);
    }


    public function edit($id)
    {
        $cek =  $this->m_video->get_by_v(array('a.id' =>$id));
        if ($cek) {
            $data['e'] = $cek;
            $data['kategori'] = drop_list('m_kategori1' ,'id' ,'nama' , 'Pilih kategori' , 'get_all' ,'asc');
            $data['module'] = $this->module;
            $data['view_file']   = 'edit';
            echo Modules::run('template/render_master' , $data);
        }else{
            redirect($this->redirect,'refresh');
        }
    }


    

    public function simpan()
    {
        if ($this->input->post()) {
            // die(print_r('simpan'));
            $tipenyafile = pathinfo($_FILES["nama"]['name'] ,PATHINFO_EXTENSION );
            $namanyafile = pathinfo($_FILES["nama"]['name']  ,PATHINFO_BASENAME);
            
            // die(print_r($_FILES['nama']));


            // $folder = str_replace( array(':' , ' '), '', $this->input->post('dir'));
            // if ($tipenyafile == 'jpg') {
            //     $path = './files/file/foto';    
            
            //     if (!is_dir($path)) { //create the folder if it's not already exists
            //         mkdir($path, 0777, TRUE);
            //     }
            // }

            
           
            // chmod('./files/file/', 0777);
            
          

            // pdf/doc/docx/ppt/pptx/zip/rar/jpg
            // $idincrement = auto_inc('m_fm','id');
            $idincrement = auto_inc('m_video','id');
            $nmfile = $idincrement;
            $config['upload_path']= './files/gorontalo/file/video/';
            $config['allowed_types']="flv|f4v|mp4|MP4|FLV";
            // $config['max_size']="10240000";
            // $config['max_width']="0";
            // $config['max_height']="0";
            // $config['encrypt_name']= TRUE;
            $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            $this->upload->initialize($config);


         
            // $idincrement = auto_inc('m_fm','id');
            if ($this->upload->do_upload('nama')) {
                $file = $this->upload->data();
            
                // die(print_r($file));
                // $idincrement = auto_inc('m_fm', 'id');
                // $data= array(
                //     'id'=> $idincrement,
                //     'nama'=>$file['file_name'],
                //     'asli' =>$file['file_name'],
                //     'ukuran' => $file['file_size'],
                //     'mime' => $file['file_type'],
                //     'tanggal' => date('Y-m-d'),
                //     'domain' => 'gorontalo.kemenag.go.id',
                //     'dir' => '',
                //     );
                // $this->m_fm->insert($data);

                $idincrement_video = auto_inc('m_video' , 'id');
                $data = array(
                    'id' => $idincrement_video,
                    'keterangan' => $this->input->post('keterangan'),
                    'durasi' => $this->input->post('durasi'),
                    'ts' => date_timestamp_get(date_create()),
                    'tampil' => $this->input->post('tampil'),
                    'setuju' => $this->input->post('setuju'),
                    'lengket' => $this->input->post('lengket'),
                    'kategori' => $this->input->post('kategori'),
                    'iduser' => $this->session->userdata('id_login'),
                    );
                $this->m_video->insert($data);
                    echo "<script>alert('Berhasil Upload video');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }else{
                     echo "<script>alert('Gagal upload video');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }
        }
    }


    public function simpan_perubahan(){
        if ($this->input->post()) {

            $cek = $cek =  $this->m_video->get_by_v(array('a.id' =>$this->input->post('id')));



            // $tipenyafile = pathinfo($_FILES["nama"]['name'] ,PATHINFO_EXTENSION );
            // $namanyafile = pathinfo($_FILES["nama"]['name']  ,PATHINFO_BASENAME);
            
            // die(print_r($tipenyafile));


            // $folder = str_replace( array(':' , ' '), '', $this->input->post('dir'));
            // if ($tipenyafile == 'jpg') {
            //     $path = './files/file/foto';    
            
            //     if (!is_dir($path)) { //create the folder if it's not already exists
            //         mkdir($path, 0777, TRUE);
            //     }
            // }

            
           
            // chmod('./files/file/', 0777);
            
          

            // pdf/doc/docx/ppt/pptx/zip/rar/jpg
            // $idincrement = auto_inc('m_fm','id');
            /*$idincrement = auto_inc('m_fm','id');
            $nmfile = str_replace(array(':' , ' '), '-',$idincrement.$namanyafile);
            $config['upload_path']= './files/file/video';
            $config['allowed_types']="mp4|flv";
            $config['max_size']="10240000";
            // $config['max_width']="0";
            // $config['max_height']="0";
            // $config['encrypt_name']= TRUE;
            $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            $this->upload->initialize($config);*/

            if ($this->upload->do_upload('nama') != '') {

                $patfile = realpath(APPPATH . '../files/file/video');
                if ($cek->video != '') {
                    unlink($patfile . '/' . $cek->video);
                }

                $file = $this->upload->data();
            
                // die(print_r($file));
                // $idincrement = auto_inc('m_fm', 'id');
                $data= array(
                    // 'id'=> $idincrement,
                    'nama'=>$file['file_name'],
                    'asli' =>$file['file_name'],
                    'ukuran' => $file['file_size'],
                    'mime' => $file['file_type'],
                    // 'tanggal' => date('Y-m-d'),
                    'domain' => 'gorontalo.kemenag.go.id',
                    'dir' => '',
                    );
                $this->m_fm->update($this->input->post('ts'),$data);

                // $idincrement_foto = auto_inc('m_foto' , 'id');
                $data = array(
                    'keterangan' => $this->input->post('keterangan'),
                    'durasi' => $this->input->post('durasi'),
                    'tampil' => $this->input->post('tampil'),
                    'setuju' => $this->input->post('setuju'),
                    'lengket' => $this->input->post('lengket'),
                    'kategori' => $this->input->post('kategori'),
                    );
                $this->m_video->update($this->input->post('id') ,$data);
                // $this->m_foto->update($this->input->post('id'),$data);
                    echo "<script>alert('Berhasil Upload File video');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }else{

                        $data = array(
                            'keterangan' => $this->input->post('keterangan'),
                            'durasi' => $this->input->post('durasi'),
                            'tampil' => $this->input->post('tampil'),
                            'setuju' => $this->input->post('setuju'),
                            'lengket' => $this->input->post('lengket'),
                            'kategori' => $this->input->post('kategori'),
                            );
                        $this->m_video->update($this->input->post('id') , $data);


                     echo "<script>alert('Berhasil Menyimpan ');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }




        }
    }


    public function tampil_a($id){
        $cek = $this->m_video->get_by(array('id' => $id));
        if ($cek->tampil == '1') {
            $data = array(
                 'tampil' => '0' 
                );
            $this->m_video->update($id , $data);
            $tampil = '0';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->tampil == '0') {
            $data = array(
                 'tampil' => '1' 
                );
            $this->m_video->update($id , $data);
            $tampil = '1';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }


    public function setuju_a($id){
        $cek = $this->m_video->get_by(array('id' => $id));
        if ($cek->setuju == '1') {
            $data = array(
                 'setuju' => '0' 
                );
            $this->m_video->update($id , $data);
            $setuju = '0';
            if ($setuju == '1') {
                
                 echo   '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="setujuA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->setuju == '0') {
            $data = array(
                 'setuju' => '1' 
                );
            $this->m_video->update($id , $data);
            $setuju = '1';
            if ($setuju == '1') {
                
                 echo   '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }


    public function  hapus($id)
    {
        $cek = $this->m_video->get_by_v(array('a.id' => $id));
        if ($cek) {
            
            // $tipenyafile = pathinfo($cek->nama ,PATHINFO_EXTENSION );
            // die(print_r($tipenyafile));  


            // if ($tipenyafile == 'jpg') {
            //     $patfile = realpath(APPPATH . '../files/file/foto/' . $cek->dir);    
            
            // }elseif ($tipenyafile == 'pdf' || $tipenyafile == 'doc' || $tipenyafile == 'docx' || $tipenyafile == 'ppt' || $tipenyafile == 'pptx' || $tipenyafile == 'rar' || $tipenyafile == 'zip' ) {
            //     $patfile = realpath(APPPATH . '../files/file/file/' . $cek->dir);
            // }else {
            //     redirect('adminweb/file.asp','refresh');
            // }
            $patfile = realpath(APPPATH . '../files/file/video');

            if ($cek->video != '') {
                unlink($patfile . '/' . $cek->video);
            }
            $this->m_fm->delete($cek->ts);
            $this->m_video->delete($id);
            redirect($this->redirect,'refresh');
        }else{
            redirect($this->redirect,'refresh');
        }
    }

}