<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah Bidang</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">

		<?php echo form_open('',array('class'=>'form-horizontal')); ?>
			<div class="row form-group">
					<label class="control-label col-sm-2">Nama</label>
					<div class="col-sm-10">
						<?php echo form_input('nama',set_value('nama'),'class="form-control" placeholder="Nama Bidang"required'); ?>
					</div>
			</div>
			<div class="row form-group">
					
					<div class="col-sm-12">
						<a href="<?= site_url('adminweb/bidang.asp') ?>" class="btn btn-default pull-right">Kembali</a>
						<label class="col-sm-2"></label>
						<?php echo form_submit('submit','Tambah Bidang','class="btn btn-primary "'); ?>
					</div>
			</div>
		<?php echo  form_close(); ?>
		</section>
	</div>
</div>


