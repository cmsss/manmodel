  <section class="content-header">
  <h1></i>Bidang</h1>
  </section>
  <section class="content">
  	<div class="row">
  		<div class="col-xs-12">
  			<div class="box">
  				<div class="box-header">
  					<h3 class="box-title">List Bidang</h3>
  					<div class="pull-right">
  						<a href="<?php echo site_url('adminweb/bidang/post.asp'); ?>" class="btn btn-info">Tambah</a>
  					</div>
  				</div><!-- /.box-header -->
  				<div class="box-body">
  					<?php echo form_open(); ?>

  					<table class="table table-hover table-condensed" id="list_kategori">
  						<thead>
  							<tr>
  								<td>No</td>
  								<td>Nama</td>
                  <td>Flag</td>
  								<td>Aksi</td>
  							</tr>
  						</thead>
  						<tbody>

  							<?php
  							$no=1;
  							if (!empty($list)) {
  								foreach ($list as $row) {?>
  								<tr>
  									<td><?php echo $no++ ?></td>
                    <td><?php echo $row->nama; ?> </td>
  									<td><?php echo $row->flag; ?></td>

  									<td>
  										<a href="<?php echo site_url('adminweb/bidang/update/'.$row->id); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
  										<a href="<?php echo site_url('adminweb/bidang/delete/'.$row->id); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
  									</td>
  								</tr>
  								<?php }
  							}else {
  								echo "<center><b><h4>Belum ada Data bidang</h4></b></center>";
  							} ?>

  						</tbody>
  					</table>
  					<?php echo form_close(); ?>
  				</div>
  			</div>
  		</div>
  	</div>
  </section>
