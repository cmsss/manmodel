<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Bidang extends MX_Controller {



	public function __construct() {
		parent::__construct();
		if (!$this->autentifikasi->sudah_login())
			redirect('adminpage/site-login.asp','refresh');
		$this->load->model('m_bidang');

	}

	public function index() {
		$data['l_kategori']=list_kategori();
		$data['list'] = $this->m_bidang->get_all();
		$data['module'] = "bidang";
		$data['view_file'] = "list";
		echo Modules::run('template/render_master',$data);
	}

	public function add(){

		if ($this->input->post()) {
			$nama = $this->input->post('nama');
			$nama_kecil = strtolower($nama);
			$replece = str_replace(array(' ','/','-','.',','), '-', $nama_kecil);
			$data  = array(
				'id' => auto_inc('m_bidang', 'id'),
				'nama' => ucwords($nama),
				'flag' => $replece,
				);

			$this->m_bidang->insert($data);
			echo "<script>alert('Berhasil Simpan data Bidang');
			window.location=('".site_url('adminweb/bidang.asp')."');</script>";
		}
		$data['module'] = 'bidang';
		$data['view_file'] = 'add_bidang';
		echo Modules::run('template/render_master' ,$data);
	}

	public function update($id){
		$cek = $this->m_bidang->get_by(array('id' => $id));
		if ($cek) {
			if ($this->input->post()) {
				$flagtes = $this->input->post('flagtes');
				$nama = $this->input->post('nama');
				$nama_kecil = strtolower($nama);
				$replece = str_replace(array(' ','/','-','.',','), '-', $nama_kecil);


				$esekusi_bidang = $this->db->query("UPDATE t_profil_pejabat SET devisi = '$replece'  WHERE devisi =  '$flagtes'");


				
				$data = array(
					'nama' => ucwords($nama),
					'flag' => $replece,
					);
				$this->m_bidang->update($id , $data);
				echo "<script>alert('Berhasil Update data');
				window.location=('".site_url('adminweb/bidang.asp')."');</script>";
			}else{
				$data['edit'] = $cek; ;
				$data['module'] = 'bidang';
				$data['view_file'] = 'edit_bidang';
				echo Modules::run('template/render_master' , $data);	
			}

			
		}else{
			redirect('adminweb/bidang.asp','refresh');
		}
	}


	public function delete($id){
		$cek = $this->m_bidang->get_by( array('id' => $id ));
		if ($cek) {
			$this->m_bidang->delete($id);
			echo "<script>alert('Berhasil hapus data Bidang');
			window.location=('".site_url('adminweb/bidang.asp')."');</script>";
		}else {
			redirect('adminweb/bidang.asp','refresh');
		}
	}


}
