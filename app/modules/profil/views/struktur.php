<section class="content-header">
  <h1></i>Profil</h1>
</section>
<section class="content">
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
          <h3 class="box-title">STRUKTUR</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="pull-right">
            <a href="<?php echo site_url('adminweb/profil/struktur/update/'.$list->id_profil); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
        </div>
    <?php echo form_open(); ?>
      <div class="col-md-12">
          <center>
            <img src="<?php echo base_url('uploads/'.$list->img);?>" height="80%" width="80%" />
            <p>
              <?php echo $list->isi; ?>
            </p>
          </center>
        </div>
    <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
</section>
