
<div class="container-fluid">
  <section class="content-header">
    <h4>STRUKTUR</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">
			<?php echo form_open_multipart('',array('class'=>'form-horizontal')); ?>

				<div class="row form-group">
						<label class="control-label col-sm-2">Deskripsi</label>
						<div class="col-sm-10">
							<textarea name="isi" class="ckeditor" rows="8" cols="40"><?php echo $edit->isi; ?></textarea>
						</div>
				</div>
        <div class="row form-group">
          <div class="col-sm-2">
         </div>
          <div class="col-sm-2">
              <img src="<?php echo base_url('uploads/' . $edit->img); ?>" class="img-responsive" style="width:100px; height: 100px;">
          </div>
        </div>
				<div class="row form-group">
            <label class="control-label col-sm-2">Ganti Gambar</label>
						<div class="col-sm-10">
							<input type="file" class="form-control" name="img"  />
						</div>
						<?= $this->session->flashdata('pesan') ?>
				</div>
				<div class="row form-group">

						<div class="col-sm-10">
						<a href="<?= site_url('adminweb/struktur.asp') ?>" class="btn btn-default">Kembali</a>
							<?php echo form_submit('submit','Edit Struktur','class="btn btn-primary"'); ?>
						</div>
				</div>

			<?php echo  form_close(); ?>
		</section>
	</div>
</div>
