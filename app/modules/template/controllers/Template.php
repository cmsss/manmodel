<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_berita1');
        $this->load->model('m_opini');
        $this->load->model('m_artikel1');
        $this->load->model('m_teksberjalan');
        $this->load->model('m_foto');
        $this->load->model('m_video');
        $this->load->model('m_audio');
        $this->load->model('m_banner');
        $this->load->model('m_majalah');
        $this->load->model('m_renungan');
        $this->load->model('m_kategori1');
        $this->load->model('m_seting');
        // $this->load->model('iklan/m_iklan');
        // $this->load->model('aplikasi/m_aplikasi');
        // $this->load->model('runningtext/m_runtext');
    }

    public function render_master($data){
        // $this->load->model('komentar/m_komentar');
        // $data['jk']=$this->m_komentar->countstatus0();
        $this->load->view('master',$data);
    }
    public function render_login($data){
        $this->load->view('login_page',$data);
    }
    public function render_main($data){
        $this->load->view('main2',$data);
    }

    public function frontend($data = NULL)
    {

        
        if (!empty($data)) {


            /*$opini = $this->m_opini->get_many_by_v(array('a.setuju' => '1') ,  4);
            $data['opini1'] = $opini;

            $artikel1 = $this->m_artikel1->get_many_by_v(array('a.infopenting' => '1') , 5);
            $data['infopenting1'] = $artikel1;

            $beritapopuler = $this->m_berita1->gat_all_terpopuler(5);
            $data['terpopuler1'] = $beritapopuler;

            $jumlah_foto = count($this->m_foto->get_all_v_k());
            $data['jumlah_foto1'] = $jumlah_foto;

            $jumlah_audio = count($this->m_audio->get_all_v_k());
            $data['jumlah_audio1'] = $jumlah_audio;

            $jumlah_video = count($this->m_video->get_all_v_k());
            $data['jumlah_video1'] = $jumlah_video;

            $data['list_kategori'] = drop_list('m_kategori1' , 'id' , 'nama' , 'Kategori Berita' );*/

            $teksberjalan = $this->m_teksberjalan->get_many_by(array('tampil' => '1'));
            $data['teksberjalan1'] = $teksberjalan;

            $seting = $this->m_seting->get_by(array('id' => '1'));
            $data ['kop'] = $seting;

            $banner = $this->m_banner->get_limit(4);
            $data['banner1'] = $banner;

            $list_foto = $this->m_foto->get_slide_foto(6);
            $data['list_foto'] = $list_foto;

            $this->load->view('v3/v_index', $data );
        }else{
            // $aplikasi       = $this->m_aplikasi->get_all();
            // $kirim['aplikasis'] = $aplikasi;

            // $raning_text    = $this->m_runtext->get_limit('10');
            // $kirim['raning_texts'] = $raning_text;

            // $iklan          =  $this->m_iklan->get_all();
            // $kirim['iklans'] =  $iklan;

            // $berita_s = $this->m_berita->get_head_v_berita('3');
            // $kirim['berita_ss'] = $berita_s;

            // $berita_p = $this->m_berita->get_populer_v_berita('10');
            // $kirim['berita'] = $berita_p;


            // $berita = $this->m_berita->get_v_berita('9' ,
            //     array(
            //         'status' => '1' ,
            //         ));
            // $kirim['berita_cc'] = $berita;


            $seting = $this->m_seting->get_by(array('id' => '1'));
            $kirim ['kop'] = $seting;


            $banner = $this->m_banner->get_limit(4);
            $kirim['banner1'] = $banner;

            $banner = $this->m_banner->get_limit_g_10(9);
            $kirim['bannerc1'] = $banner;

            $jumlah_foto = count($this->m_foto->get_all_v_k());
            $kirim['jumlah_foto1'] = $jumlah_foto;

            $jumlah_audio = count($this->m_audio->get_all_v_k());
            $kirim['jumlah_audio1'] = $jumlah_audio;

            $jumlah_video = count($this->m_video->get_all_v_k());
            $kirim['jumlah_video1'] = $jumlah_video;

            $berita = $this->m_berita1->get_many_by_v(array('a.setuju' => '1') , 6 );
            $kirim['berita1'] = $berita;

            $slide = $this->m_berita1->get_many_by_v(array('a.setuju' => '1' , 'a.tampil' => '1') , 3);
            $kirim['slide1'] = $slide;

            $opini = $this->m_opini->get_many_by_v(array('a.setuju' => '1') ,  3);
            $kirim['opini1'] = $opini;

            $artikel1 = $this->m_artikel1->get_many_by_v(array('a.infopenting' => '1') , 4);
            $kirim['infopenting1'] = $artikel1;

            $beritapopuler = $this->m_berita1->gat_all_terpopuler(6);
            $kirim['terpopuler1'] = $beritapopuler;

            $teksberjalan = $this->m_teksberjalan->get_many_by(array('tampil' => '1'));
            $kirim['teksberjalan1'] = $teksberjalan;

            $majalah = $this->m_majalah->get_limit_v(1);
            $kirim['majalah1'] = $majalah;

            $list_foto = $this->m_foto->get_slide_foto(6);
            $kirim['list_foto'] = $list_foto;






            $this->load->view('v3/v_index', $kirim );
        }
       
        
    }
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */