<footer id="footer"  >
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow slideInUp">
               
                <div class="footer-btn-social">
                    <a class="btn btn-twitter" href="https://twitter.com/kemenagGtlo" role="button" target="_blank"><i class="icon-social-twitter icons"></i> Follow us</a>
                    <a class="btn btn-facebook" href="https://www.facebook.com/kanwilkemenaggorontalo/" role="button" target="_blank"><i class="icon-social-facebook icons"></i> Follow us</a>
                    <a class="btn btn-warning" href="<?= site_url('rss'); ?>" role="button" target="_blank"><i class="icon-feed icons"></i> RSS</a>
                </div>
                <div class="footer-copyright">
                    <p>Inmas Kanwil Provinsi Gorontalo <span id="copyright-year"><?php echo date('Y'); ?> </span>. <a href="https://www.kemenag.go.id/" target="_blank">Kementrian Agama</a></p>
                </div>
            </div>
        </div>
    </div>
</footer> 