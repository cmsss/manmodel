<div class="panel panel-theme shadow wow zoomIn">
    <div class="panel-heading">
        <h3 class="panel-title">Berita Terpopuler</h3>
    </div>
    <div class="panel-body no-padding">
        <div class="list-group no-margin">
            <?php 

                if (!empty($terpopuler1)) {
                    foreach ($terpopuler1 as $kepo ) {

                        $judul = $kepo->judul;
                        
                        $judul_fix = flag($judul);
                        ?>
                            <a href="<?= site_url('berita/'.$kepo->id.'/'.$judul_fix) ?>" class="list-group-item">
                                <?= $kepo->judul ?>
                            </a>

                        <?php
                    }
                }

             ?>
            
        </div>
    </div>
</div>