<div class="panel panel-theme shadow  wow zoomIn">
    <div class="panel-heading">
        <h3 class="panel-title">OPINI</h3>
    </div>
    <div class="panel-body ">
    <div class="row">
        
    <!-- </div> -->
        <?php 

            if (!empty($opini1)) {
                foreach ($opini1 as $op1) {
                    ?>

                         <?php 
                            $tgl = explode(' ', $op1->tgl_input);

                            $judulo = $op1->judul;
                            
                            $judul_fixo = flag($judulo);

                         ?>

                        <div class="col-sm-6">
                            <div  class="cbp cbp-caption-active cbp-caption-revealBottom cbp-ready">
                                <div class="cbp-item art">
                                    <a href="<?= site_url('opini/'.$op1->id. '/' .$judul_fixo) ?>" class="cbp-caption">
                                        <div class="cbp-caption-defaultWrap">
                                            <!-- 340x210 -->
                                            <div class=" fileinput-preview fileinput-exists" style=" max-height: 250px; line-height: 10px;">
                                                <img src="<?= base_url('files/gorontalo/file/fotoopini/'.$op1->id. '.jpg') ?>" class="img-responsive"  alt="..." onerror="this.onerror=null;this.src='<?= base_url('uploads/kemenag/kemenag.jpg') ?>';">
                                            </div>
                                        </div>
                                        <div class="cbp-caption-activeWrap">
                                            <div class="cbp-l-caption-alignCenter">
                                                <div class="cbp-l-caption-body">
                                                    <div class="cbp-l-caption-text">VIEW POST</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="<?= site_url('opini/'.$op1->id. '/' .$judul_fixo) ?>"  class="blog-title"><?= $op1->judul ?></a>
                                    <div class="blog-date"><?php echo nama_hari($tgl['0']).' '.  tgl_indo($tgl['0']) ?></div>
                                    <div class="blog-split">|</div>
                                    <a href="#" class="blog-comments"><?= $op1->counters ?> dibaca</a>
                                    <div class="blog-desc"><?php //echo substr($op1->isi , 0 ,100); ?></div>
                                </div>
                            </div>
                        </div>


                    <?php
                }
            }

         ?>
        
        <!-- <div class="col-sm-6">
            <div  class="cbp cbp-caption-active cbp-caption-revealBottom cbp-ready">
            <div class="cbp-item art">
                <a href="https://wrapbootstrap.com/user/djavaui" target="_blank" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="http://img.djavaui.com/?create=340x210,37BC9B?f=ffffff" alt="...">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-text">VIEW POST</div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="https://wrapbootstrap.com/user/djavaui" target="_blank" class="cbp-l-grid-blog-title">Berlin wall breakthrough</a>
                <div class="cbp-l-grid-blog-date">14 june 2015</div>
                <div class="cbp-l-grid-blog-split">|</div>
                <a href="#" class="cbp-l-grid-blog-comments">7 comments</a>
                <div class="cbp-l-grid-blog-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</div>
            </div>
        </div>
        </div> -->
        <div class="clearfix">
    
        </div>
        
     </div>
     <div class="pull-right">
        <a class="btn btn-primary" href="<?= site_url('opini-gorontalo') ?>">Selengkapnya</a>
    </div>   
    </div>

</div> 