<div class="panel panel-theme shadow wow zoomIn">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo form_dropdown('kategori', $list_kategori, '' , 'class="form-control" onchange="beritaKategori(this)"'); ?></h3>
    </div>
    <div class="panel-body no-padding">
        <div class="list-group no-margin">
            <div id="putKategori">
                <?php 

                    if (!empty($terpopuler1)) {
                        foreach ($terpopuler1 as $kepo ) {

                            $judul = $kepo->judul;
                            
                            $judul_fix = flag($judul);
                            ?>
                                <a href="<?= site_url('berita/'.$kepo->id.'/'.$judul_fix) ?>" class="list-group-item">
                                    <?= $kepo->judul ?>
                                </a>

                            <?php
                        }
                    }

                 ?>
            </div>
            
            
        </div>
    </div>
</div>



<script>
    function beritaKategori(data) {
        var data = data.value;

        $.ajax({
            url: '<?= site_url(); ?>berita-kategori',
            type: 'GET',
            dataType: 'html',
            data: {data: data},
            beforeSend : function(){

            },
            success : function(data){
               $("#putKategori").html(data);
            }
        });
        
    }
</script>