<!DOCTYPE html>
 <html lang="en">

    <!-- START @HEAD -->
    <head>
        <!-- START @META SECTION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Kemenetrian Agama Provinsi Gorotntalo">
        <meta name="keywords" content="">
        <meta name="author" content="KEMENAG">
        <title>Gorontalo Kemenag</title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->
        <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
        <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
        <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
        <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
        <!-- <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon.png" rel="shortcut icon"> -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/kemenag/logo.png"  >
        <!--/ END FAVICONS -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/spinkit/css/spinners/3-wave.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet">
        <!-- RS5.0 Main Stylesheet -->
        <link href="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/slider-revolution/revolution/css/settings.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/slider-revolution/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/slider-revolution/revolution/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- RS5.0 Layers and Navigation Styles -->
        <link href="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/slider-revolution/revolution/css/layers.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/slider-revolution/revolution/css/navigation.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/admin/css/components.css" rel="stylesheet">

        <!-- date picker -->
        <link href="<?php echo base_url(); ?>tmp/intranet/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- date picker -->


        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="<?= base_url() ?>asset/kemenagv2/assets/kemenag/assets/css/reset.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/kemenag/assets/css/section.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/kemenag/assets/css/component.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/kemenag/assets/css/plugin.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/kemenag/assets/css/theme.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/kemenag/assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>asset/kemenagv2/assets/kemenag/assets/css/pendekar.css" rel="stylesheet">
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->

        
        <div id="fb-rooot">
            
        </div>
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=736855586388639";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.8&appId=1765316997073959";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script> -->
        <script>
            !function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + "://platform.twitter.com/widgets.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, "script", "twitter-wjs");
        </script>

    </head>
    <!--/ END HEAD -->

    <!-- START @BODY -->
    <body id="home" data-spy="scroll" data-target=".navbar-fixed-top">
        <div class="">



            <!-- START @WRAPPER -->
            <section id="wrapper">
                <!--[if lt IE 9]>
                <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
                <![endif]-->

                <!-- START @PRELOADER -->
                <div id="loader-wrapper">
                    <div class="loader-brand">
                        <div class="sk-wave">
                            <div class="sk-rect sk-rect1"></div>
                            <div class="sk-rect sk-rect2"></div>
                            <div class="sk-rect sk-rect3"></div>
                            <div class="sk-rect sk-rect4"></div>
                            <div class="sk-rect sk-rect5"></div>
                        </div>
                        <div class="loader-text">
                            GORONTALO KEMENAG
                        </div>

                    </div>
                    <div class="loader-section section-left"></div>
                    <div class="loader-section section-right"></div>
                </div><!-- /#loader-wrapper -->
                <!--/ END PRELOADER -->

                <!-- START @HEADER -->
                <div style="background: #6abd2a">
                    <header id="header">
                        <?= $this->load->view('head') ?>
                    </header> <!-- /#header -->
                </div>
                <!--/ END HEADER -->

                <!-- START @BANNER -->
                <!--                <div class="container">
                                    <div class="row">
                
                                        
                                    </div>
                
                                </div>-->

                <!--/ END BANNER -->

                <!-- START @FEATURES -->

                <section id="features">


                    <div class="container">
                        <div class="row">
                           <?php 
                            if ($this->uri->segment(1) == '') {
                                $this->load->view('beranda');
                            }else {
                                ?>
                                <!--kiri-->    

                                    <div class="col-md-12" style="margin-top: 10px;">

                                        <div class="col-sm-12">
                                            <?php
                                                $this->load->view($module .'/'.$view);

                                            ?>
                                        </div>  
                                    </div>

                                    <!--kiri-->

                                   

                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </section> <!-- /#features -->
                <!--/ END FEATURES -->

                <!-- START @TESTIMONIALS -->
                <?= $this->load->view('lain') ?>
                <!--/ END TESTIMONIALS -->

                <!-- START @FOOTER -->
                <?= $this->load->view('foot') ?>
                <!--/ END FOOTER -->

                <!-- START @BACK TOP -->
                <div id="back-top" class="animated pulse circle">
                    <i class="icon-arrow-up icons"></i>
                </div><!-- /#back-top -->
                <!--/ END BACK TOP -->
            </section>
            <!--/ END WRAPPER -->
        </div>
        <!-- tes/ -->
        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
        <!--/ END CORE PLUGINS -->


        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url() ?>asset/kemenagv2/assets/admin/js/apps.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/admin/js/pages/blankon.project.portfolio.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/admin/js/demo.js"></script>
        <!-- tes -->


        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/counter-up/jquery.counterup.min.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="<?= base_url() ?>asset/kemenagv2/assets/global/plugins/bower_components/wow/dist/wow.min.js"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
        <!-- RS5.0 Core JS Files -->
        <script src="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/slider-revolution/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
        <script src="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/slider-revolution/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script src="<?= base_url() ?>asset/kemenagv2/assets/commercial/plugins/slider-revolution/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <script type="text/javascript" src="<?php echo base_url(); ?>asset/plugin/easy-ticker/jquery.easing.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>asset/plugin/easy-ticker/jquery.easy-ticker.js"></script>

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url() ?>asset/kemenagv2/assets/kemenag/assets/js/main.js"></script>

        <script src="<?php echo base_url(); ?>asset/plugin/toas/jquery.toaster.js"></script>
        <!--/ END PAGE LEVEL SCRIPTS -->
        <!--/ END JAVASCRIPT SECTION -->



        <!-- date picker -->
        <script src="<?php echo base_url(); ?>tmp/intranet/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
        <!-- <script src="<?php //echo base_url(); ?>tmp/intranet/plugins/datepicker/locales/bootstrap-datepicker.id.js" type="text/javascript"></script> -->
        <!-- date picker -->

        <!-- datepicker -->
        <script>
            $('.datepicker').datepicker({
                    format: "yyyy-mm-dd",
                    language: 'id',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 3,
                    minView: 2,
                    forceParse: 0,

                });
        </script>
        <!-- date picker -->


        <!-- START GOOGLE ANALYTICS -->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/58c8001941acfb239f8cd448/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55892530-1', 'auto');
            ga('send', 'pageview');

        </script>
        <!--/ END GOOGLE ANALYTICS -->

    </body>
    <!--/ END BODY -->

</html>
