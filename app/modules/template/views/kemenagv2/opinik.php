<div class="panel panel-theme shadow wow zoomIn">
    <div class="panel-heading">
        <h3 class="panel-title">Opini</h3>
    </div>
    <div class="panel-body">
        <div class="isi">
            <?php

                if (!empty($opini1)) {
                    foreach ($opini1 as $b1) {
                        ?>

                             <?php 
                                $tgl = $b1->tgl_input;
                                $judul = $b1->judul;
                                
                                $judul_fix = flag($judul);

                                
                                // waktu ts
                                $date = new DateTime();
                                $tgl_n = $date->setTimestamp($b1->ts);
                                $tgl_pecah1 = explode(' ', $date->format('Y-m-d H:i:s'));

                                // waktu ts nama_hari($tgl_pecah1[0]).' '.  tgl_indo($tgl_pecah1[0]) .' '.$tgl_pecah1[1]


                             ?>
                            <div class="media">
                                <a class="pull-left" href="<?= site_url('opini/' .$b1->id. '/' .$judul_fix) ?>">
                                        <img class="media-object thumbnail img-responsive" style="width: 100px; " src="<?= base_url('files/gorontalo/file/fotoopini/' . $b1->id. '.jpg'); ?>"  onerror="this.onerror=null;this.src='<?= base_url('uploads/kemenag/200x200.png') ?>';" alt="...">
                                </a>

                                <div class="media-body">
                                    <a  href="<?= site_url('opini/' .$b1->id. '/' .$judul_fix) ?>">
                                        <h4 class="media-heading"><?= $b1->judul ?></h4>
                                    </a>
                                    <small><?php echo nama_hari($tgl_pecah1[0]).' '.  tgl_indo($tgl_pecah1[0]) .' '.$tgl_pecah1[1] ?></small>
                                    
                                </div>
                            </div><!-- media -->

                        <?php
                    }
                }else{
                    echo "Belum Ada Opini Untuk Di tampilkan";
                }


            ?>
            
            <!-- <div class="media">
                <a class="pull-left" href="detail.html">
                    <img class="media-object thumbnail" src="http://img.djavaui.com/?create=100x100,6880B0?f=ffffff" alt="...">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">About The Author</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div> -->
            <div class="pull-right">
                <a href="<?= site_url('opini-gorontalo') ?>" class="btn btn-primary">Selengkapnya</a>
            </div>
        </div>
    </div>

</div>