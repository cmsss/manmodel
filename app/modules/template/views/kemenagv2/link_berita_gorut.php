<?php if (!empty($berita_gorut)): ?>
    <div class="panel panel-theme shadow wow zoomIn">
        <div class="panel-heading">
            <h3 class="panel-title"><b>Gorontalo Utara</b></h3>
        </div>
        <div class="panel-body no-padding">
            <div class="list-group no-margin">
                <?php 

                    foreach ($berita_gorut as $kepo ) {

                        $judul = $kepo->judul;
                        $judul_fix = flag($judul);
                        ?>
                            <a href="<?= site_url('berita/'.$kepo->id.'/'.$judul_fix) ?>" class="list-group-item">
                                <?= $kepo->judul ?>
                            </a>

                        <?php
                    }

                 ?>
                 
                
            </div>
        </div>
    </div>
<?php endif ?>
