<div class="panel panel-theme shadow blog-list-slider wow zoomIn">
    <div class="panel-heading">
        <h3 class="panel-title">Informasi Penting</h3>
    </div>
    <div id="carousel-blog-list" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">

            <?php 
                $no_info = 0; 
                
                if (!empty($infopenting1)) { ?>
                    <?php 
                        foreach ($infopenting1 as $info1) {
                            if ($no_info == 0) {
                                $aktiveinfo = "active";
                            }else{
                                $aktiveinfo = '';
                            }
                            ?>

                                <li data-target="#carousel-blog-list"  class="<?= $aktiveinfo ?>" data-slide-to="<?= $no_info++ ?>"></li>
                            <?php
                        }
                        

                     ?>
                    
            <?php }?>
        </ol>

        <div class="carousel-inner">

            <?php
                $no_infoa = 0; 
                if (!empty($infopenting1)) {
                   
                    foreach ($infopenting1 as $infoa1) {

                        $judul = $infoa1->judul;
                        $judul_fix = flag($judul);
                        


                        if ($no_infoa == 0) {
                            $aktiveinfoa = "active";
                        }else{
                            $aktiveinfoa = '';
                        }
                        $no_infoa++
                        ?>

                            <div class="item <?= $aktiveinfoa ?>">
                                <div class="blog-list">
                                    <div class="media">
                                        
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="<?= site_url('artikel/'.$infoa1->id.'/'.$judul_fix) ?>" title="info Penting"><?= $infoa1->judul ?></a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <?php
                    }
                }

            ?>
           
           
        </div>

    </div>
    <div class="panel-body">
        <a class="btn btn-info pull-right" href="<?= site_url('artikel-gorontalo') ?>">Selengkapnya</a>
        
    </div>

</div>
