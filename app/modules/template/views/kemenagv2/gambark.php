<div class="panel panel-tab rounded shadow wow zoomIn">
    <!-- Start tabs heading -->
    <div class="panel-heading no-padding">
        <ul class="nav nav-tabs">
           
            <li class="active nav-border nav-border-top-info">
                <a href="#tab6-21" data-toggle="tab" class="text-center">
                    <i class="fa fa-image "></i>
                </a>
            </li>
            
            <li class="nav-border nav-border-top-danger">
                <a href="#tab6-41" data-toggle="tab" class="text-center">
                    <i class="fa fa-play "></i>
                   
                </a>
            </li>
            <li class="nav-border nav-border-top-warning">
                <a href="#tab6-42" data-toggle="tab" class="text-center">
                    <i class="fa fa-soundcloud "></i>
                   
                </a>
            </li>
            
        </ul>
    </div><!-- /.panel-heading -->
    <!--/ End tabs heading -->

    <!-- Start tabs content -->
    <div class="panel-body">
        <div class="tab-content">
            
            <div class="tab-pane fade in active" id="tab6-21">
                <div class="panel rounded shadow ">
                    <div class="panel-heading text-center bg-twitter">
                        <a href="<?= site_url('foto-gorontalo') ?>" title="">
                            <p class="inner-all no-margin">
                                
                                 <i class="fa fa-image fa-5x"></i>
                            </p>
                        </a>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body text-center">
                        <p class="h4 no-margin inner-all text-strong">
                            <span class="block counter"><?= $jumlah_foto1; ?></span>
                            <span class="block">Foto</span>
                        </p>
                    </div><!-- /.panel-body -->
                </div>
            </div>
           
            <div class="tab-pane fade" id="tab6-41">
                <div class="panel rounded shadow ">
                    <div class="panel-heading text-center bg-youtube">
                        <a href="<?= site_url('video-gorontalo') ?>" title="">
                            <p class="inner-all no-margin">
                                <i class="fa fa-play fa-5x"></i>
                            </p>
                        </a>
                    </div>
                    <div class="panel-body text-center">
                        <p class="h4 no-margin inner-all text-strong">
                            <span class="block counter"><?= $jumlah_video1; ?></span>
                            <span class="block">Video</span>
                        </p>
                    </div>
                </div>
            </div>
             <div class="tab-pane fade" id="tab6-42">
                <div class="panel rounded shadow ">
                    <div class="panel-heading text-center bg-soundcloud">
                        <a href="<?= site_url('audio-gorontalo') ?>" title="">
                            <p class="inner-all no-margin">
                                <i class="fa fa-soundcloud fa-5x"></i>
                            </p>
                        </a>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body text-center">
                        <p class="h4 no-margin inner-all text-strong">
                            <span class="block counter" data-counter="34282"><?= $jumlah_audio1; ?></span>
                            <span class="block">Audio</span>
                        </p>
                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->
            </div>
            
        </div>
    </div><!-- /.panel-body -->
    <!--/ End tabs content -->
</div><!-- /.panel -->








