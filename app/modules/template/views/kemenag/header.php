<div class="container">
    <div class="logo fl">
        <a href="<?= base_url() ?>asset/kemenag/index.html"><img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/logo.png" alt=""></a>
    </div>
    <div class="h-right">
        <a href="<?= base_url() ?>asset/kemenag/artikel/14928/kontak-kami.html">Kontak Kami</a> |
        <a href="<?= base_url() ?>asset/kemenag/sitemap.html">Peta Situs</a> |
        <span class="bhs">
            Bahasa
            <div class="subbhs">
                <a href="<?= base_url() ?>asset/kemenag/index9bb4.html?lang=id">Indonesia</a>
                <a href="<?= base_url() ?>asset/kemenag/index9ed2.html?lang=en">English</a>
                <a href="<?= base_url() ?>asset/kemenag/indexd6cc.html?lang=ar">Arabic</a>
            </div>
        </span>
        <div class="clearfix"></div>
        <form action="https://gorontalo.kemenag.go.id/search" class="search" method="get">
            <input type="text" class="input" name="q" placeholder="Search">
            <input type="image" src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/ico_search.png" class="btn">
            <div class="clearfix"></div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>