<!-- MENU TAB BERITA -->
<div class="tab-content" id="tab2">
    <a href="#berita">
        Berita
        <span></span>
    </a>
    <a href="#opini">
        Opini
        <span></span>
    </a>
    <a href="#majalah">
        Majalah Internal
        <span></span>
    </a>
    <a href="#renungan">
        Renungan
        <span></span>
    </a>
    <a href="#banner">
        Banner
        <span></span>
    </a>
</div>
<!-- MENU TAB BERITA -->

<!--ISI MENU id BERITA-->
<div id="berita" class="list-tab">
    <div class="t">Berita</div>
    <?php 
    if (!empty($berita_cc)) {
        foreach ($berita_cc as $b_c) {
            $date = $b_c->create_date;
            $tgl =nama_hari($date).' '. tgl_indo($date);


            ?>

        <a href="<?= site_url('berita-kemenag/'.$b_c->flag) ?>" class="list-4">
        <div class="date"><?= $tgl ?></div>
            <h2><?= $b_c->judul?></h2>
            <div class="ratio4_3 box_img">
                <div class="img_con lqd">
                    <img src="<?= base_url('image/berita/'.$b_c->img) ?>" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/img-default.jpg';">
                </div>
            </div>
            <p>
                <?php
                  echo substr($b_c->isi , 0 ,100) . '[...]';            
                 ?> 
            </p>
            
        </a>



        <?php  }
    }
    ?>
    

    

    
    
    <div class="clearfix pt10"></div>
    <div align="center">
        <a href="<?= site_url() ?>data-berita-kemenag" class="btn_more">Selengkapnya</a>
    </div>
</div>
<!--ISI MENU id BERITA-->


<!--ISI MENU id OPINI-->
<div id="opini" class="list-tab">
    <div class="t">Opini</div>
    <a href="<?= base_url() ?>asset/kemenag/opini/398/membangun-masyarakat-tamaddun-beradab-dengan-5-nilai-dasar-budaya-kerja.html" class="list-4">
        <div class="date">Kamis, 15 Desember 2016, 08:59</div>
        <h2>MEMBANGUN MASYARAKAT TAMADDUN (BERADAB)  DENGAN 5 NILAI DASAR BUDAYA KERJA</h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/398.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        Oleh: Dr. Waris Masuara, S.Ag. M.H.IDi penghujung 2014 Menteri Agama RI Lukman Hakim Saifuddin meliris konsep 5... 
    </a>
    <a href="<?= base_url() ?>asset/kemenag/opini/390/potensi-siswa-madrasah-harus-lebih-baik.html" class="list-4">
        <div class="date">Jumat, 9 Desember 2016, 14:22</div>
        <h2>POTENSI SISWA MADRASAH HARUS LEBIH BAIk</h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/390.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        Oleh :Marisavavan Israil, S.Pd.I (Pengembang Potensi Siswa pada Bidang Pendidikan... 
    </a>
    <a href="<?= base_url() ?>asset/kemenag/opini/389/mengkritisi-pelibatan-guru-dalam-politik-praktis-pilkada.html" class="list-4">
        <div class="date">Jumat, 9 Desember 2016, 14:20</div>
        <h2>MENGKRITISI PELIBATAN GURU DALAM POLITIK PRAKTIS PILKADA</h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/389.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        Oleh : Prof. Yulianto KadjiGuru Besar Kebijakan Publik Universitas Negeri GorontaloAbstrakDinamika Politik di daerah... 
    </a>
    <a href="<?= base_url() ?>asset/kemenag/opini/388/menyibak-prosesi-adat-pernikahan-gorontalo.html" class="list-4">
        <div class="date">Jumat, 9 Desember 2016, 14:15</div>
        <h2>MENYIBAK PROSESI ADAT PERNIKAHAN GORONTALO</h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/388.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        Oleh : Maryam Hamid(Kasubbag Informasi dan Humas Kanwil Kemenag Prov. Gorontalo)Gorontalo... 
    </a>
    <a href="<?= base_url() ?>asset/kemenag/opini/317/pelembagaan-da-i-adalah-solusi-problematika-dakwah.html" class="list-4">
        <div class="date">Senin, 14 November 2016, 14:17</div>
        <h2>PELEMBAGAAN DA’I ADALAH SOLUSI  PROBLEMATIKA DAKWAH </h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/317.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        Oleh : Thalib Manhia, S.Sos.IDai adalah isim fail (satu nama sebagai pelaku) dari pekerjaan daa, yadu,... 
    </a>
    <a href="<?= base_url() ?>asset/kemenag/opini/261/menggagas-pegawai-asn-teladan-di-lingkungan-kementerian-agama.html" class="list-4">
        <div class="date">Selasa, 25 Oktober 2016, 09:54</div>
        <h2>MENGGAGAS PEGAWAI ASN TELADAN DI LINGKUNGAN KEMENTERIAN AGAMA</h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/261.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        Oleh: Arfan A. Tilome... 
    </a>
    <a href="<?= base_url() ?>asset/kemenag/opini/260/solusi-hukum-bagi-perkawinan-tidak-tercatat-sirri.html" class="list-4">
        <div class="date">Selasa, 25 Oktober 2016, 09:49</div>
        <h2>SOLUSI HUKUM BAGI PERKAWINAN TIDAK TERCATAT (SIRRI)</h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/260.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        OLEH:MANSUR BASIR(Staf pada Bidang Bimas Islam Kanwil Kemenag Prov.... 
    </a>
    <a href="<?= base_url() ?>asset/kemenag/opini/73/memutus-mata-rantai-gagal-paham-manasik-lewat-madrasah-bermanasik.html" class="list-4">
        <div class="date">Senin, 5 September 2016, 07:57</div>
        <h2>MEMUTUS MATA RANTAI GAGAL PAHAM MANASIK LEWAT “MADRASAH BERMANASIK”</h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/73.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        Oleh: Dra. Hj. Maryam Hamid, M.Pd.IAbstrakSecara... 
    </a>
    <a href="<?= base_url() ?>asset/kemenag/opini/72/menangkal-gerakan-isis-bagi-umat-islam.html" class="list-4">
        <div class="date">Senin, 5 September 2016, 07:47</div>
        <h2>MENANGKAL GERAKAN ISIS BAGI UMAT ISLAM</h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd">
                <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/fotoopinitb/72.jpg" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';">
            </div>
        </div>
        OLEH : Dr. H. SABARA KARIM NGOU, M.Pd.IBelakangan ini informasi gerakan ISIS marak diberbagai media massa baik elektronik maupun media cetak. Bagi umat islam yang kurang paham... 
    </a>
    <div class="clearfix pt10"></div>
    <div align="center">
        <a href="<?= base_url() ?>asset/kemenag/opini.html" class="btn_more">Selengkapnya</a>
    </div>
</div>
<!--ISI MENU id OPINI-->


<!--ISI MENU id MAJALAH-->
<div id="majalah" class="list-tab">
    <div class="t">Majalah Internal</div>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/12/1480898022133100918.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/12/1480898022674963060.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Desember 2016</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14775255631466628838.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14775255631376561193.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi September 2016</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476839879674699430.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14768398792117943449.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Maret 2016</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476838842435600467.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476838842397018978.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Desember 2015</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14766910141746531241.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476691014569051817.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Oktober 2015</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14766909711673922543.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476690971264085905.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Agustus 2015</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476690837648515425.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476690837672589542.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi April 2015</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14766907051190043525.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14766907051492142008.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Mei 2014</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/147669063158688245.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14766906311632198641.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Desember 2013</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764336141023409546.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764336141584618756.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Desember 2012</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764279861274313244.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476427986384974529.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Agustus 2012</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764275341447904946.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764275341803636718.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi November 2011</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/147642664511427765.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476426645986163362.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Februari 2011</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764250131505327166.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764250131818257786.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Mei 2009</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764246541228298604.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476424654931415385.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi Juli 2008</h2>
    </a>
    <a href="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/14764243962005361813.pdf" class="list-5" target="_blank">
        <div class="ratio16_9 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/files/gorontalo/file/2016/10/1476424396129162265.jpg"/> </div>
        </div>
        <h2>Majalah Salam Edisi April 2008</h2>
    </a>
    <div class="clearfix pt10"></div>
    <div align="center">
        <a href="<?= base_url() ?>asset/kemenag/majalah.html" class="btn_more">Selengkapnya</a>
    </div>
</div>
<!--ISI MENU id MAJALAH-->


<!--ISI MENU id RENUNGAN-->
<div id="renungan" class="list-tab">
    <div class="t">Renungan</div>
    <a class="list-4">
        <h2></h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg"/> </div>
        </div>
        &quot;Andaikata berlaku keatas seseorang pemimpin itu TANDA-TANDA KEKUFURAN, PERBUATAN MERUBAH SYARIAT DAN PERLAKUAN BID&#039;AH, MAKA WAJIBLAH DILUCUTKAN kepemimpinan daripadanya dan gugurlah tanggungjawab ketaatan rakyat kepadanya. Malah berkewajiban untuk memecatnya dan digantikan dengan seorang yang adil.&quot; (Syarah Muslim -- Imam Nawawi)
    </a>
    <a class="list-4">
        <h2></h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg"/> </div>
        </div>
        &quot;TUHAN, karena kuasa-Mulah raja bersukacita; betapa besar kegirangannya karena kemenangan yang dari pada-Mu! Apa yang menjadi keinginan hatinya telah Kaukaruniakan kepadanya, dan permintaan bibirnya tidak Kautolak.&quot; (Mzm 21:2-3)
    </a>
    <a class="list-4">
        <h2></h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg"/> </div>
        </div>
        Keserakahan, kebencian, dan kebodohan merupakan 3 racun dalam kehidupan manusia. Atasi keserakahan dengan berdana, kebencian dengan hati yang welas asih, dan atasi kebodohan dengan kebijaksanaan.
    </a>
    <a class="list-4">
        <h2></h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg"/> </div>
        </div>
        &quot;ketika kebodohan tinggal dalam kegelapan, bijaksana dalam kesombongan mereka sendiri, dan kesombongan dengan pengetahuan yang sia-sia berputar-putar sempoyongan ke sana kemari, seperti orang buta yang dipimpin oleh orang buta&quot; (Upanishad.Mundaka 1.2.8-9)
    </a>
    <a class="list-4">
        <h2></h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg"/> </div>
        </div>
        Niat adalah ukuran dalam menilai benarnya suatu perbuatan, oleh karenanya, ketika niatnya benar, maka perbuatan itu benar, dan jika niatnya buruk, maka perbuatan itu buruk. (Imam An Nawawi)
    </a>
    <a class="list-4">
        <h2></h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg"/> </div>
        </div>
        Bagi orang berilmu yang ingin meraih kebahagiaan di dunia maupun di akhirat, maka kuncinya hendakalah ia mengamalkan ilmunya kepada orang-orang. (Syaikh Abdul Qodir Jailani).
    </a>
    <a class="list-4">
        <h2></h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg"/> </div>
        </div>
        Tanda penyertaan Tuhan dalam hidup kita adalah hidup kita menjadi berkat bagi banyak orang.
        (Yohanes 4:34) Kata Yesus kepada mereka: &quot;Makanan-Ku ialah melakukan kehendak Dia yang mengutus Aku dan menyelesaikan pekerjaan-Nya&quot;.
    </a>
    <a class="list-4">
        <h2></h2>
        <div class="ratio4_3 box_img">
            <div class="img_con lqd"> <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg"/> </div>
        </div>
        Jauhilah kebiasaan menggunjing, karena dapat menyebabkan tiga bencana : pertama, doa tak terkabul. Kedua, amal kebaikan tak diterima. Ketiga, dosa bertambah
    </a>
    <div class="clearfix"></div>
</div>
<!--ISI MENU id RENUNGAN-->



<!--ISI MENU id BANNER-->
<div id="banner" class="list-tab">
    <div class="t">Banner</div>
    <div class="row">
        
    <?php

        if (!empty($aplikasis)) {
            foreach ($aplikasis as $a_s) {

                ?>
                    <div class="col-sm-4">
                        <a href="<?= $a_s->link ?>" class="">
                            <div class=" box_img">
                            <img class="img-responsive" src="http://localhost/kemenag/uploads/aplikasi/84736033c486888b3ea383bc6966487f.png" style="height: 20x; width: 100%" >
                                <div class="img_con"> <img src="<?= base_url('uploads/aplikasi/' .$a_s->img) ?>"/> </div>
                            </div>
                        </a>
                    </div>
                    

                <?php
                
            }
        }

    ?>
    </div>

    
    
    
    <div class="clearfix"></div>
</div>
<!--ISI MENU id BANNER-->
<!-- e:tab-content -->