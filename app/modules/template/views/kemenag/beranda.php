<!-- ==================== RANING TEXT ======================= -->
<?= $this->load->view('raning_text') ?>
<!-- ==================== END Raning TEXT ======================= -->
<div class="row">
    <div class="col-sm-8">


        <!-- ==============SLIDE HOT============ -->
        <?= $this->load->view('slide') ?>
        <!--==============END SLIDE HOT============ -->


        <!-- =============BANER================= -->
        <?= $this->load->view('bener') ?>
        <!-- =============END BANER2================= -->


        <!-- berita terpopuler mobile -->
        <?= $this->load->view('berita_terpopuler_m') ?>
        <!-- end Berita terpopuler Mobile -->

        <!-- isi data tab -->
        <?= $this->load->view('berita_c') ?>
        <!-- end isi tab -->

        <div class="clearfix pt15"></div>

    </div>

    <!--DATA KANAN-->
    <div class="col-sm-4">


        <!-- INFO PENTING -->
        <?= $this->load->view('invo_penting') ?>
        <!-- INFO PENTING -->




        <div class="clearfix pt15"></div>



        <!-- MEDIA SOSIAL -->
        <?= $this->load->view('media_sosial') ?>
        <!-- MEDIA SOSIAL -->


        <div class="clearfix"></div>

        <!--TAMPILAN DESKTOP -->
        <?= $this->load->view('berita_terpopuler_d') ?>
        <!--TAMPILAN DESKTOP -->

    </div>
    <!--DATA KANAN-->
</div>
