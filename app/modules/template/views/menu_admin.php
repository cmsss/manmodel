<ul class="sidebar-menu">
    <li class="header">MENU UTAMA</li>
    <li <?php
    if ($this->uri->segment('2') == 'dashboard.asp') {
        echo 'class="active"';
    } else {
        echo '';
    }
    ?>>
        <a href="<?php echo site_url('adminweb/dashboard.asp'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <?php if ($this->session->userdata('level') == '1'){ ?>
    <li <?php
        if ($this->uri->segment('2') == 'artikel.asp') {
            echo 'class="active"';
        } else {
            echo '';
        }
        ?>><a href="<?php echo site_url('adminweb/artikel.asp'); ?>"><i class="fa fa-pencil-square-o"></i><span>Artikel</span></a>
    </li>
    <li class="treeview <?php
        if ($this->uri->segment('2') == 'kategori.asp'  or $this->uri->segment('2') == 'berita.asp' or $this->uri->segment('2') == 'berita') {
            echo 'active';
        } else {
            echo '';
        }
        ?>">
            <a href="#">
                <i class="fa fa-file-text"></i> <span>Management Berita</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li <?php
                if ($this->uri->segment('2') == 'berita.asp') {
                    echo 'class="active"';
                } else {
                    
                }
                ?>><a href="<?php echo site_url('adminweb/berita.asp'); ?>"><i class="fa fa-circle-o"></i> List Berita</a></li>
                <li <?php
                if ($this->uri->segment('2') == 'kategori.asp') {
                    echo 'class="active"';
                } else {
                    
                }
                ?>><a href="<?php echo site_url('adminweb/kategori.asp'); ?>"><i class="fa fa-circle-o"></i> List Kategori</a></li>
                
            </ul>
            
        </li>

        <li <?php
                if ($this->uri->segment('2') == 'opini.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><!-- <a href="<?php echo site_url('adminweb/opini.asp'); ?>"><i class="fa fa fa-circle-o"></i><span>Opini</span></a> -->
        </li>

       <!--  <li class="treeview <?php
           if ($this->uri->segment('2') == 'struktur.asp' or $this->uri->segment('2') == 'visi_misi.asp' or $this->uri->segment('2') == 'profilpejabat.asp' or  $this->uri->segment('2') == 'profil') {
               echo 'active';
           } else {
               echo '';
           }
           ?>">
               <a href="#">
                   <i class="fa fa-user-md"></i> <span>Profil</span> <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="treeview-menu">
                   <li <?php
                   if ($this->uri->segment('2') == 'struktur.asp') {
                       echo 'class="active"';
                   } else {

                   }
                   ?>><a href="<?php echo site_url('adminweb/struktur.asp'); ?>"><i class="fa fa-circle-o"></i> Struktur</a></li>
                   <li <?php
                   if ($this->uri->segment('2') == 'visi_misi.asp') {
                       echo 'class="active"';
                   } else {

                   }
                   ?>><a href="<?php echo site_url('adminweb/visi_misi.asp'); ?>"><i class="fa fa-circle-o"></i> Visi Misi</a></li>
                   <li <?php
                   if ($this->uri->segment('2') == 'profilpejabat.asp') {
                       echo 'class="active"';
                   } else {

                   }
                   ?>><a href="<?php echo site_url('adminweb/profilpejabat.asp'); ?>"><i class="fa fa-circle-o"></i> Personil</a></li>
               </ul>

           </li> -->

           
          <!--  <li <?php
                if ($this->uri->segment('2') == 'bidang.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><a href="<?php echo site_url('adminweb/bidang.asp'); ?>"><i class="fa fa-archive"></i><span>Bidang</span></a>
            </li> -->
        <!-- <li <?php
                if ($this->uri->segment('2') == 'agenda.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><a href="<?php echo site_url('adminweb/agenda.asp'); ?>"><i class="fa fa-calendar"></i><span>Agenda</span></a>
        </li> -->
        <li <?php
                if ($this->uri->segment('2') == 'foto.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><a href="<?php echo site_url('adminweb/foto.asp'); ?>"><i class="fa fa-picture-o"></i><span>Foto</span></a>
        </li>
        <li <?php
                if ($this->uri->segment('2') == 'video.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><a href="<?php echo site_url('adminweb/video.asp'); ?>"><i class="fa fa-video-camera"></i><span>video</span></a>
        </li>
        <li <?php
                if ($this->uri->segment('2') == 'audio.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><a href="<?php echo site_url('adminweb/audio.asp'); ?>"><i class="fa fa-file-audio-o"></i><span>Audio</span></a>
        </li>
        <li <?php
                if ($this->uri->segment('2') == 'file.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><a href="<?php echo site_url('adminweb/file.asp'); ?>"><i class="fa fa-paperclip"></i><span>File</span></a>
        </li>

        <li <?php
                if ($this->uri->segment('2') == 'banner.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><a href="<?php echo site_url('adminweb/banner.asp'); ?>"><i class="fa fa-bold"></i><span>Banner</span></a>
        </li>
        <li <?php
                if ($this->uri->segment('2') == 'majalah.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><!-- <a href="<?php echo site_url('adminweb/majalah.asp'); ?>"><i class="fa fa-maxcdn"></i><span>Majalah</span></a> -->
        </li>

        <li <?php
                if ($this->uri->segment('2') == 'renungan.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><!-- <a href="<?php echo site_url('adminweb/renungan.asp'); ?>"><i class="fa fa-home"></i><span>Renungan</span></a> -->
        </li>
        <li <?php
                if ($this->uri->segment('2') == 'renungan.asp') {
                    echo 'class="active"';
                } else {
                    echo '';
                }
                ?>><a href="<?php echo site_url('adminweb/teksberjalan.asp'); ?>"><i class="fa fa-text-width"></i><span>Teks Berjalan</span></a>
        </li>
        
        
        
            
         
        <li class="header">MENU ADMIN</li>
        <li class="treeview <?php
           if ($this->uri->segment('2') == 'user.asp' or $this->uri->segment('2') == 'user' or $this->uri->segment('2') == 'level-user.asp' or  $this->uri->segment('2') == 'level-user') {
               echo 'active';
           } else {
               echo '';
           }
           ?>">
               <a href="#">
                   <i class="fa fa-user-md"></i> <span>User</span> <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="treeview-menu">
                   <li <?php
                   if ($this->uri->segment('2') == 'user.asp') {
                       echo 'class="active"';
                   } else {

                   }
                   ?>><a href="<?php echo site_url('adminweb/user.asp'); ?>"><i class="fa fa-circle-o"></i> List</a></li>
                   <li <?php
                   if ($this->uri->segment('2') == 'level-user.asp') {
                       echo 'class="active"';
                   } else {

                   }
                   ?>><!-- <a href="<?php echo site_url('adminweb/level-user.asp'); ?>"><i class="fa fa-circle-o"></i> Level User</a> --></li>
               </ul>

           </li>

            <li <?php
            if ($this->uri->segment('2') == 'seting.asp') {
                echo 'class="active"';
            } else {
                echo '';
            }
            ?>>
                <!-- <a href="<?php echo site_url('adminweb/seting.asp'); ?>">
                    <i class="fa fa-fw fa-gears"></i> <span> Seting </span>
                </a> -->
            </li>
            
            <li <?php
            if ($this->uri->segment('2') == 'menu.asp') {
                echo 'class="active"';
            } else {
                echo '';
            }
            ?>>
                <a href="<?php echo site_url('adminweb/menu.asp'); ?>">
                    <i class="fa fa-cubes"></i> <span> Menu </span>
                </a>
            </li>
    <?php }else{ 
      ?>
      <li <?php
                if ($this->uri->segment('2') == 'berita.asp') {
                    echo 'class="active"';
                } else {
                    
                }
                ?>><a href="<?php echo site_url('adminweb/berita.asp'); ?>"><i class="fa fa-circle-o"></i> List Berita</a></li>
        <li <?php
                if ($this->uri->segment('2') == 'file.asp') {
                    echo 'class="active"';
                } else {
                    
                }
                ?>><a href="<?php echo site_url('adminweb/file.asp'); ?>"><i class="fa fa-circle-o"></i> File</a></li>
        <li <?php
                if ($this->uri->segment('2') == 'foto.asp') {
                    echo 'class="active"';
                } else {
                    
                }
                ?>><a href="<?php echo site_url('adminweb/foto.asp'); ?>"><i class="fa fa-circle-o"></i> Foto</a></li>
      <?php
    } ?>
</ul>

