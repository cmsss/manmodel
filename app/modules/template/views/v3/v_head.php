<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="<?php echo NAMA_WEB ?>">
    <meta name="keywords" content="<?php echo NAMA_WEB ?>">
    <meta name="author" content="<?php echo NAMA_WEB ?>">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title><?php echo NAMA_WEB ?></title>

    <?php
    if(!empty($l_berita)){
        ?>
        <meta property="og:url"                content="<?php echo curPageURL()?>" />
        <meta property="og:type"               content="website" />
        <meta property="og:title"              content="<?= $l_berita->judul ?>" />
        <meta property="og:description"        content="<?= substr(strip_tags($l_berita->isi), 0,100) ?>" />
        <meta property="og:image"              content="<?= base_url('files/gorontalo/file/fotoberita/'.$l_berita->id.'.jpg') ?>" />
        
        <?php
    
    }
    ?>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url('asset') ?>/alfademy/img/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url('asset') ?>/alfademy/style.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <style type="text/css">
        .marginTop {
            margin-top: -87px;
        }
        .marginRightMedia{
            margin-right: 10px;
        }

        .marginBottomMedia{
            margin-bottom: 16px;
        }
    </style>

</head>