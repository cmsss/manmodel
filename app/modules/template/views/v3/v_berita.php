<!-- ##### Top Popular Courses Area Start ##### -->
<div class="top-popular-courses-area section-padding-100-70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms">
                    <!-- <span>The Best</span> -->
                    <h3>Berita Terpopuler</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <?php if (!empty($terpopuler1)): ?>
                <?php foreach ($terpopuler1 as $b1): ?>
                    <?php 
                        $tgl = $b1->tgl_input;
                        $judul = $b1->judul;
                        
                        $judul_fix = flag($judul);


                     ?>
                    <!-- Single Top Popular Course -->
                    <div class="col-12 col-lg-6">
                        <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp" data-wow-delay="400ms">
                            <div class="popular-course-content">
                                <h5><?php echo strip_tags($b1->judul); ?></h5>
                                <span><?php echo ts($b1->ts) ?></span>
                                
                                <p><?php echo substr(strip_tags($b1->isi), 0,80); ?> . . . . .</p>
                                <a href="<?= site_url('berita/' .$b1->id. '/' .$judul_fix) ?>" class="btn academy-btn btn-sm">Baca Selengkapnya</a>
                            </div>
                            <div class="popular-course-thumb bg-img" style="background-image: url(<?= base_url('files/gorontalo/file/fotoberita/' . $b1->id. '.jpg'); ?>);"></div>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div>
<!-- ##### Top Popular Courses Area End ##### -->