<?php

function menu_kemenag(){
    $d = query_sql("SELECT * FROM menu WHERE parent = '0' and hapus = '0' ORDER BY id asc LIMIT 6" );
    return $d;
}


function sub_menu($id)
{
    $d = query_sql("SELECT * FROM menu WHERE parent = '$id' and hapus = '0' ORDER BY id asc");
    return $d;
}



?>

<?php 
    function buatUrlBAru($url)
    {
        // $url = $l->url;
        $pecah = explode('punyaku/', $url);
        if (!empty($pecah[1])) {
            $urlGet = site_url().$pecah[1];
        }else{
            $urlGet = $url;
        }

        return $urlGet;
    }

 ?>

<!-- Navbar Area -->
<div class="academy-main-menu">
    <div class="classy-nav-container breakpoint-off">
        <div class="container">
            <!-- Menu -->
            <nav class="classy-navbar justify-content-between" id="academyNav">

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">

                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul>
                            <li><a href="<?php echo site_url('') ?>">Home</a></li>
                            <?php if (!empty(menu_kemenag())): ?>
                                <?php foreach (menu_kemenag() as $m1): ?>
                                <?php 

                                    if (!empty(sub_menu($m1->id))) {
                                        $url1 = "#";
                                       
                                    }else {
                                        $url1 = buatUrlBAru($m1->url);
                                    }

                                 ?>
                                    <li><a href="<?php echo $url1 ?>"><?= $m1->nama ?></a>
                                        <?php if (!empty(sub_menu($m1->id))): ?>
                                            <ul class="dropdown" style="width: max-content;">
                                            <?php foreach (sub_menu($m1->id) as $sub1): ?>
                                                <?php 

                                                    if (!empty(sub_menu($sub1->id))) {
                                                        $url2 = "#";
                                                    }else {
                                                        $url2 = buatUrlBAru($sub1->url);
                                                    }

                                                 ?>
                                                <li><a href="<?= $url2 ?>"><?= $sub1->nama ?></a></li>
                                            <?php endforeach ?>
                                            </ul>
                                        <?php endif ?>
                                    </li>
                                <?php endforeach ?>
                            <?php endif ?>
                            
                        </ul>
                    </div>
                    <!-- Nav End -->
                </div>

                <!-- Calling Info -->
                <div class="calling-info">
                    <div class="call-center">
                        <!-- <a href="tel:+654563325568889"><i class="icon-telephone-2"></i> <span>(+65) 456 332 5568 889</span></a> -->
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>