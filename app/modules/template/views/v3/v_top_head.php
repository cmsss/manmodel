<!-- Top Header Area -->
<div class="top-header">
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-12 h-100">
                <div class="header-content h-100 d-flex align-items-center justify-content-between">
                    <div class="academy-logo">
                        <h3><b><?php echo NAMA_WEB ?></b></h3>
                        <!-- <a href="index.html"><img src="<?php echo base_url('asset') ?>/alfademy/img/core-img/logo.png" alt=""></a> -->
                    </div>
                    <div class="login-content">

                        <?= form_open('cari-data', 'class="navbar-form"'); ?>
                            <div class="form-group has-feedback" >
                                <input name="data" type="text" class="form-control typeahead rounded" placeholder="Cari Berita">
                                <button type="submit" class="btn fa fa-search " 
                                style="top: 4px;
                                right: 5px;
                                width: 37px;
                                height: 26px;
                                line-height: 12px;
                                position: absolute;
                                pointer-events: auto">
                                </button>
                            </div>

                        <?= form_close(); ?>
                    <!-- <input type="text" class="form-control" id="name" placeholder="Cari Berita"> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>