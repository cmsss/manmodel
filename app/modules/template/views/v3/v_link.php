<!-- ##### Partner Area Start ##### -->
<div class="partner-area section-padding-0-100">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="partners-logo d-flex align-items-center justify-content-between flex-wrap">
                    <?php if (!empty($banner1)): ?>
                        <?php foreach ($banner1 as $b1): ?>
                            <?php 
                                if ($b1->grup == '2') {
                                    $link = "#";
                                }else {
                                    $link = $b1->url;
                                    
                                }
                             ?>
                            <a href="<?php echo $link ?>"><img src="<?= base_url('files/gorontalo/file/banner/'.$b1->filegambar) ?>" alt="benner"></a>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Partner Area End ##### -->