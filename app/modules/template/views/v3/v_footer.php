<!-- ##### Footer Area Start ##### -->
<footer class="footer-area">
    <div class="main-footer-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="footer-widget mb-100">
                        <div class="widget-title">
                            <a href="#"><h5 style="color: #ffffff"><?php echo NAMA_WEB; ?></h5></a>
                        </div>
                        <p>MAN MODEL GORONTALO adalah sekolah SMA Negeri yang terletak di Provinsi Gorontalo, Kota Gorontalo. Sekolah ini menggunakan Agama Islam sebagai pegangan utama pendidikan Agamanya.</p>
                        <div class="footer-social-info">
                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-dribbble"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a> -->
                        </div>
                    </div>
                </div>
                <!-- Footer Widget Area -->
                <!-- <div class="col-12 col-sm-6 col-lg-3">
                    <div class="footer-widget mb-100">
                        <div class="widget-title">
                            <h6>Usefull Links</h6>
                        </div>
                        <nav>
                            <ul class="useful-links">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Services &amp; Features</a></li>
                                <li><a href="#">Accordions and tabs</a></li>
                                <li><a href="#">Menu ideas</a></li>
                            </ul>
                        </nav>
                    </div>
                </div> -->
                <!-- Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="footer-widget mb-100">
                        <div class="widget-title">
                            <h6>Gallery</h6>
                        </div>
                        <div class="gallery-list d-flex justify-content-between flex-wrap">
                            <?php if (!empty($list_foto)): ?>
                                <?php foreach ($list_foto as $l_foto): ?>
                                    <a href="<?= base_url('files/gorontalo/file/foto/'.$l_foto->id.'.jpg') ?>" class="gallery-img" title="<?php echo strip_tags($l_foto->keterangan) ?>"><img src="<?= base_url('files/gorontalo/file/foto/'.$l_foto->id.'.jpg') ?>" alt="<?php echo strip_tags($l_foto->keterangan) ?>"></a>
                                <?php endforeach ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <!-- Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="footer-widget mb-100">
                        <div class="widget-title">
                            <h6>Contact</h6>
                        </div>
                        <div class="single-contact d-flex mb-30">
                            <i class="icon-placeholder"></i>
                            <p>Jl Poigar No 26, Kelurahan Molosipat U, Kecamatan Sipatan, Molosipat U, Kec. Sipatana, Gorontalo, 96126</p>
                        </div>
                        <div class="single-contact d-flex mb-30">
                            <i class="icon-telephone-1"></i>
                            <p>Telepon: (0435) 824177</p>
                        </div>
                        <!-- <div class="single-contact d-flex">
                            <i class="icon-contract"></i>
                            <p>office@yourbusiness.com</p>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> by <?php echo NAMA_WEB; ?>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<!-- ##### Footer Area Start ##### -->