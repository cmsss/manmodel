<!-- ##### All Javascript Script ##### -->
<!-- jQuery-2.2.4 js -->
<script src="<?php echo base_url('asset') ?>/alfademy/js/jquery/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="<?php echo base_url('asset') ?>/alfademy/js/bootstrap/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="<?php echo base_url('asset') ?>/alfademy/js/bootstrap/bootstrap.min.js"></script>
<!-- All Plugins js -->
<script src="<?php echo base_url('asset') ?>/alfademy/js/plugins/plugins.js"></script>
<!-- Active js -->
<script src="<?php echo base_url('asset') ?>/alfademy/js/active.js"></script>