<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin Panel | <?php echo NAMA_WEB ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        <link href="<?php echo base_url(); ?>tmp/intranet/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <!-- <link href="<?php //echo base_url(); ?>asset/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css -->
        <!-- Ionicons -->
        <link href="<?php echo base_url(); ?>asset/dist/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css -->
        <link href="<?php echo base_url(); ?>tmp/intranet/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>tmp/intranet/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>tmp/intranet/plugins/select2/select2-bootstrap.css" rel="stylesheet" type="text/css" />

        <!-- Theme style -->
        <link href="<?php echo base_url(); ?>tmp/intranet/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>tmp/intranet/dist/css/jquery-autocomplete.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url(); ?>tmp/intranet/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>tmp/frontend/img/logo110.png">


        <!-- select2 -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/plugin/select2/select2.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/plugin/select2/select2-metronic.css">

        <!-- select2 -->
    </head>
    <body class="skin-green-light sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo site_url('adminweb/dashboard.asp'); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>MM</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>MAN</b>MODEL</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo site_url(); ?>" target="_blank"><i class="fa fa-globe"></i> Lihat Site</a></li>
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url(); ?>tmp/frontend/img/logo110.png" class="user-image" alt="User Image"/>
                                    <span class="hidden-xs"> <?php echo $this->session->userdata('nama_lengkap'); ?> </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url(); ?>tmp/frontend/img/logo.png" class="img-circle" alt="User Image" />
                                        <p>
                                            <?php echo $this->session->userdata('nama_lengkap'); ?> 
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="col-xs-4 text-center">
                                            <a href="#">&nbsp;</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">&nbsp;</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">&nbsp;</a>
                                        </div>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?= site_url('adminweb/user/profile.asp'); ?>" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo site_url('adminweb/logout.asp') ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>tmp/frontend/img/logo.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p> <?php echo $this->session->userdata('nama_lengkap'); ?> </p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <!-- <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form> -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <?php echo $this->load->view('menu_admin'); ?>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php $this->load->view($module . '/' . $view_file); ?>
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.0
                </div>
                <strong>Copyright &copy; 2016 <a href="http://bappeda.kota.gorontalo.web.id/">KEMENAG Gorontalo</a>.</strong> All rights reserved.
            </footer>
            <script src="<?php echo base_url(); ?>asset/dist/js/jquery-1.9.1.js"></script>
            <script src="<?php echo base_url(); ?>asset/dist/js/jquery-ui.js"></script>
            
                <script type="text/javascript">
                    $(function() {
                        $("#url1").hide();
                        $("#url2").hide();
                        $('#url3').hide();
                        });
                </script>
           
            
            <!-- Bootstrap 3.3.2 JS -->
            <script src="<?php echo base_url(); ?>tmp/intranet/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <!-- SlimScroll -->
            <script src="<?php echo base_url(); ?>tmp/intranet/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>tmp/intranet/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>tmp/intranet/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <!-- FastClick -->
            <script src='<?php echo base_url(); ?>tmp/intranet/plugins/fastclick/fastclick.min.js'></script>
            <script src="<?php echo base_url(); ?>tmp/intranet/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>tmp/intranet/plugins/datepicker/locales/bootstrap-datepicker.id.js" type="text/javascript"></script>
            <!-- AdminLTE App -->
            <script src="<?php echo base_url(); ?>tmp/intranet/dist/js/app.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>tmp/intranet/dist/js/admin.js" type="text/javascript"></script>
            <!-- <script type="text/javascript" src="<?php echo site_url(); ?>tmp/ckeditor/ckeditor.js"></script> -->
            <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
            <script src="<?php echo base_url(); ?>tmp/intranet/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>tmp/intranet/plugins/select2/select2.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>asset/plugin/toas/jquery.toaster.js"></script>

            <!-- table menegemnet -->

            <script type="text/javascript" src="<?php echo base_url(); ?>asset/plugin/select2/select2.min.js"></script>

            <script type="text/javascript" src="<?php echo base_url(); ?>asset/plugin/data-tables/jquery.dataTables.js"></script>

            <script type="text/javascript" src="<?php echo base_url(); ?>asset/plugin/data-tables/DT_bootstrap.js"></script>

            <!-- AdminLTE App -->
            <script src="<?= base_url() ?>asset/dist/js/app.min.js" type="text/javascript"></script>

            <script type="text/javascript" src="<?php echo base_url(); ?>asset/dist/js/table-managed.js"></script>


            <!-- end table meegement -->

            <script type="text/javascript">

                 


                $('.datepicker').datepicker({
                    language: 'id',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 3,
                    minView: 2,
                    forceParse: 0
                });
                $('.select-multiple').select2();
            </script> 
            <script type="text/javascript">
                $(function () {
                    $(".select2kemenag").select2();
                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.
                    CKEDITOR.replace('editor1');
                    //bootstrap WYSIHTML5 - text editor
                    $(".textarea").wysihtml5();
                });
            </script>

            

    </body>
</html>
