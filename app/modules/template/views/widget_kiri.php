<div class="panel panel-default">
    <div class="panel-heading text-center">A P L I K A S I</div>
      <div class="panel-body panel-widget">
      <?php $aplikasi=widget('m_aplikasi'); ?>
      <?php if (!empty($aplikasi)) {
        foreach ($aplikasi as $a) {?>
          <a  href="<?php echo $a->link; ?>" target="_blank"><img src="<?php echo base_url('uploads/aplikasi/'.$a->img); ?>" class="img-responsive" style="width: 100%;"></a>
          <?php }
          } ?>
          
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading text-center">L I N K  &nbsp; T E R K A I T</div>
      <div class="panel-body panel-widget">
        <?php $link=widget('m_link'); ?>
        <?php if (!empty($link)) {
          foreach ($link as $l) {?>
            <a href="<?php echo $l->link; ?>"  target="_blank"><img src="<?php echo base_url('uploads/'.$l->img); ?>" class="img-responsive" style="width: 100%; padding: 2px;"></a>
          <?php }
        } ?>
    </div>
</div>

<div class="panel panel-dafult">
  <div class="panel-heading text-center">P E N G U N J U N G</div>
    <div class="panel-body panel-widget">
        <div class="widget-list">
            <ul>
                <li>Pengunjung hari ini : <?php
                    $hari = insert_statistik();
                    $hari = statistik(date('Y-m-d'));
                    echo number_format($hari, 0, ",", ".")
                    ?></li>
                <li>Pengunjung bulan ini : <?php
                    $bulan = statistik(date('m')) + 453;
                    echo number_format($bulan, 0, ",", ".")
                    ?></li>
                <li>Pengunjung tahun ini : <?php
                    $tahun = $hari + $bulan * 12;
                    echo number_format($tahun, 0, ",", ".")
                    ?></li>
                <li>Total Pengunjung : <?php
                    $total = $tahun + $hari + $bulan;
                    echo number_format($total, 0, ",", ".")
                    ?></li>
            </ul>
        </div>
    </div>
</div>

<div class="panel panel-dafault">
    <div class="panel-heading text-center">
      <a href="<?php echo site_url('hasil-poling-jejak-pendapat.html'); ?>">J E J A K &nbsp; P E N D A P A T</a>
    </div>
      <div class="panel-body panel-widget">
          <?= form_open(site_url('jejakpendapat.html')); ?>
                <input type="radio" name="pendapat" value="1" required="required"></input> Kawasan Ekonomi Kecamatan<br/>
                <input type="radio" name="pendapat" value="2" required="required"></input> Infrastruktur Perkotaan<br/>
                <input type="radio" name="pendapat" value="3" required="required"></input> Kartu Sejahtera (Gratis dari Lahir sampai Mati)<br/>
                <input type="radio" name="pendapat" value="4"  required="required"></input> Penataan Birokrasi<br/>
                <input type="radio" name="pendapat" value="5"  required="required"></input> Penguatan organisasi Agama<br/>
                 <input type="radio" name="pendapat" value="6"  required="required"></input> Kawasan Ciber City dan Technopark<br/>
                 <button type="submit" name="submit" class="btn btn-raised btn-info">Kirim</button>
          <?= form_close(); ?>
    </div>
</div>
