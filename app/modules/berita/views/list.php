  <section class="content-header">
    <h1>Berita</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Berita</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('adminweb/berita/post.asp'); ?>" class="btn btn-info">Buat Tulisan</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>
			<div class="table-responsive">
				<table class="table table-hover table-condensed" id="list_kategori">
					<thead>
						<tr>
							<th>No</th>
							<th>Judul</th>
							<th>Kategori</th>
							<th>Penulis</th>
							<th>URL</th>
							<th>Headline</th>
							<th>status</th>
							<th>aksi</th>
						</tr>
					</thead>
					<tbody>
					
						<?php 
							$no=1;
							foreach ($list as $row) {?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td><?php echo $row->judul; ?></td>
							<td><?php echo $row->kategori; ?></td>
							<td><?php echo $row->penulis; ?></td>
							<td><?php echo $row->flag; ?></td>
							<td>
								<?php if ($row->headline === '1'){
									echo 'Ya';
								}else{
									echo 'Tidak';
								} ?>
							</td>
							<td><?php 
								if ($row->status === '1'){
									echo 'Aktif';
								}else{
									echo 'Tidak Aktif';
								}
							?></td>
							<td>
								<a href="<?php echo site_url('adminweb/berita/update/'.$row->id_berita); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
								<a href="<?php echo site_url('adminweb/berita/delete/'.$row->id_berita); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<?php } ?>
						
					</tbody>
				</table>
			</div>
			
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
