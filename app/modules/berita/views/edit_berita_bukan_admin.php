<section class="content-header">
    <h1>POST Berita</h1>
</section>
<section class="content">
    <div class="row">
        <?php echo form_open_multipart(site_url(uri_string())); ?>
        <div class="col-md-8">
            <p> <?php echo form_input('judul', $edit->judul, 'class="form-control" placeholder="Tuliskan Judul Disini" required'); ?> </p>
            <?php // echo form_textarea('isi', '', 'class="ckeditor" placeholder="Tuliskan Isi Artikel Disini"'); ?>
            <textarea class="ckeditor" name="isi" rows="30" cols="80" style="width: 100%;"><?php echo $edit->isi; ?></textarea>
            <div class="clearfix"></div>
            <br/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Gambar</h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-12">
                        <img src="<?php echo base_url('image/berita/' . $edit->img); ?>" class="img-responsive" style="width:100px; height: 100px;">
                    </div>
                    <br/><br/><br/><br/>
                    <label class="control-label col-lg-3" for="sc_name">Ganti Gambar : </label>
                    <div class="col-lg-9">
                        <input type="file" name="img" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Kategori</h3>
                </div>
                <div class="panel-body">
                    <?php echo form_dropdown('kategori',$kategori,$edit->kategori,'class="form-control" required="required"'); ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Terbitkan</h3>
                </div>
                <div class="panel-body">
                    Rubah Tanggal Posting : <input type="text" class="form-control datepicker" placeholder="Kosongkan jika ingin tgl posting hari ini" name="tgl_post" value="<?= $tgl; ?>">
                    <?php echo form_label('Headline'); ?>
                    
                        <input type="radio" name="headline" value="1" <?php if ($edit->headline == '1') {
                           echo 'checked="true"';
                        } ?> > Yes
                        <input type="radio" name="headline" value="0" <?php if ($edit->headline == '0') {
                           echo 'checked="true"';
                        } ?>> No
                   
                    
                </div>
                <div class="panel-footer">
                    <?php echo form_submit('Terbitkan', 'Simpan', 'class="btn btn-primary"'); ?>
                    <a href="<?php echo site_url('adminweb/berita.asp'); ?>" class="btn btn-default">Keluar</a>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>
