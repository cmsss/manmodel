<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Renungan extends MX_Controller {

    private $module;
    private $redirect;

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

        $this->load->model('m_renungan');
        $this->module = "renungan";
        $this->redirect = 'adminweb/renungan.asp';

    }

    public function index() {
        $data['list']   = $this->m_renungan->get_all();
        $data['module'] = $this->module;
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

    

    public function tambah()
    {
        $data['module'] = $this->module;
        $data['view_file']   = 'tambah';
        echo Modules::run('template/render_master' , $data);
    }


    public function edit($id)
    {
        $cek =  $this->m_renungan->get_by(array('id' => $id));
        if ($cek) {
            $data['e'] = $cek;
            $data['module'] = $this->module;
            $data['view_file']   = 'edit';
            echo Modules::run('template/render_master' , $data);
        }else{
            redirect($this->redirect,'refresh');
        }
    }


    

    public function simpan()
    {
        if ($this->input->post()) {
            
            $data = array(
                'id' => auto_inc('m_renungan' , 'id'),
                'isi'  => $this->input->post('isi'),
                'tampil' => $this->input->post('tampil'),
                'domain' => NAMA_DOMAIN,
                );
            $this->m_renungan->insert($data);


            echo "<script>alert('Berhasi Simpan Data');
            window.location=('" . site_url($this->redirect) . "');</script>";
               
                     
               
        }else{
            redirect($this->redirect,'refresh');
        }
    }


    public function simpan_perubahan(){
        if ($this->input->post()) {
            $data = array(
                'isi'  => $this->input->post('isi'),
                'tampil' => $this->input->post('tampil'),
                'domain' => NAMA_DOMAIN,
                );
            $this->m_renungan->update( $this->input->post('id') , $data);


            echo "<script>alert('Berhasi Simpan Data');
            window.location=('" . site_url($this->redirect) . "');</script>";
        }else{
             redirect($this->redirect,'refresh');
        }

    }


    public function tampil_a($id){
        $cek = $this->m_renungan->get_by(array('id' => $id));
        if ($cek->tampil == '1') {
            $data = array(
                 'tampil' => '0' 
                );
            $this->m_renungan->update($id , $data);
            $tampil = '0';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->tampil == '0') {
            $data = array(
                 'tampil' => '1' 
                );
            $this->m_renungan->update($id , $data);
            $tampil = '1';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }


    public function setuju_a($id){
        $cek = $this->m_renungan->get_by(array('id' => $id));
        if ($cek->setuju == '1') {
            $data = array(
                 'setuju' => '0' 
                );
            $this->m_renungan->update($id , $data);
            $setuju = '0';
            if ($setuju == '1') {
                
                 echo   '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="setujuA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->setuju == '0') {
            $data = array(
                 'setuju' => '1' 
                );
            $this->m_renungan->update($id , $data);
            $setuju = '1';
            if ($setuju == '1') {
                
                 echo   '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }


    public function  hapus($id)
    {
        $cek = $this->m_renungan->get_by(array('id' => $id));
        if ($cek) {
            
            // $tipenyafile = pathinfo($cek->nama ,PATHINFO_EXTENSION );
            // die(print_r($tipenyafile));  


            // if ($tipenyafile == 'jpg') {
            //     $patfile = realpath(APPPATH . '../files/file/foto/' . $cek->dir);    
            
            // }elseif ($tipenyafile == 'pdf' || $tipenyafile == 'doc' || $tipenyafile == 'docx' || $tipenyafile == 'ppt' || $tipenyafile == 'pptx' || $tipenyafile == 'rar' || $tipenyafile == 'zip' ) {
            //     $patfile = realpath(APPPATH . '../files/file/file/' . $cek->dir);
            // }else {
            //     redirect('adminweb/file.asp','refresh');
            // }
           
            $this->m_renungan->delete($id);
            redirect($this->redirect,'refresh');
        }else{
            redirect($this->redirect,'refresh');
        }
    }

}