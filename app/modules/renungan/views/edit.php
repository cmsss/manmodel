<div class="container-fluid">
	<section class="content-header">
		<h4>Edit Renungan</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('adminweb/renungan/simpan-perubahan',array('class'=>'form-horizontal')); ?>
			
			<?= form_hidden('id', $e->id); ?>
			<div class="row form-group">
				<label class="control-label col-sm-2"></label>
				<div class="col-sm-10">
					<textarea class="ckeditor" name="isi" rows="30" required cols="80" style="width: 100%;"><?= $e->isi ?></textarea>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Tampil</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="tampil" value="1" style="margin-right: 10px;" <?= $t1 = ($e->tampil == '1' ? 'checked ' : ''); ?> >
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="tampil" value="0" style="margin-right: 10px; padding-left: 10px;" <?= $t0 = ($e->tampil == '0' ? 'checked ' : ''); ?>>
						<label class="">Tidak</label>
					</div>
				</div>
			</div>
			

			<div class="row form-group">

				<div class="col-sm-12">
					<a href="<?= site_url('adminweb/renungan.asp') ?>" class="btn btn-default pull-right">Kembali</a>
					<label class="col-sm-2"></label>
					<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
				</div>
			</div>

		<?php echo  form_close(); ?>
	</section>
</div>
</div>


