  <section class="content-header">
    <h1>Artikel</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Artikel</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('adminweb/artikel/post.asp'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Artikel</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>
			<div class="table-responsive">
				<table class="table table-hover table-condensed" id="list_kategori">
					<thead>
						<tr>
							<th>No</th>
							<th>Judul</th>
							<th>Penulis</th>
							<th>Tampil</th>
							<th>Setuju</th>
							<th>Copy</th>
							<th>Url</th>
							<th>aksi</th>
						</tr>
					</thead>
					<tbody>
					
						<?php 
							$no=1;
							foreach ($list as $row) {
								$judul = $row->judul;
			                    $judul_sterlir = str_replace(array('.' ,',' ,'(' , ')' ,'{' ,'}','?' ,'!' , '=' , '/'), '', $judul);
			                    $judul_sterlir2 = str_replace(' ', '-', $judul_sterlir);
			                    $judul_fix = strtolower($judul_sterlir2);


						?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td><?php echo $row->judul;?></td>
							<td><?php echo $row->iduser; ?></td>
							<td>
								<div id="tampil<?= $row->id ?>">
								<?php
									if ($row->tampil == '1') {
										?>
											<a href="#" onclick="tampilA('<?= $row->id ?>');"><small class="label  bg-green">Ya</small></a>
										<?php
									}else {
										?>
											<a href="#" onclick="tampilA('<?= $row->id ?>');"><small class="label  bg-red">Tidak</small></a>
										<?php
									}

								?>
								</div>
								
								
							</td>
							<td>
								<div id="setuju<?= $row->id ?>">
								<?php
									if ($row->setuju == '1') {
										?>
											<a href="#" onclick="setujuA('<?= $row->id ?>');"><small class="label  bg-green">Ya</small></a>
										<?php
									}else {
										?>
											<a href="#" onclick="setujuA('<?= $row->id ?>');"><small class="label  bg-red">Tidak</small></a>
										<?php
									}

								?>	
								</div>
								
							</td>
							<td>
								<button type="button" class="copyyy btn btn-default" data-klik='<?= site_url("artikel/".$row->id ."/".$judul_fix); ?>' >Copy URl <i class="fa fa-fw fa-copy"></i></button>
							</td>
							<td>
								<a  class="btn btn-default" target=" _parent" href="<?= site_url("artikel/".$row->id ."/".$judul_fix); ?>">Lihat  <i class="fa fa-fw fa-eye"></i></a>
							</td>
							
							
							<td>
								<a href="<?php echo site_url('adminweb/artikel/edit/'.$row->id); ?>"  class="btn btn-warning"><i class="fa fa-pencil"></i></a>
								<a href="<?php echo site_url('adminweb/artikel/delete/'.$row->id); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-danger"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<?php } ?>
						
					</tbody>
				</table>
			</div>
			
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>


<script>
	function tampilA(data) {
		// alert(data);
		var nilai = data;
		$.ajax({
			url: '<?php echo site_url();?>adminweb/artikel/tampila/' + nilai,
			// type: 'POST',
			dataType: 'html',
			beforeSend : function(){

			},
			success : function(data){
				$("#tampil" + nilai).html(data);
			}
		});
		
		
	}
	function setujuA(data) {
		// alert(data);
		var nilai = data;
		$.ajax({
			url: '<?php echo site_url();?>adminweb/artikel/setujua/' + nilai,
			// type: 'POST',
			dataType: 'html',
			beforeSend : function(){

			},
			success : function(data){
				$("#setuju" + nilai).html(data);
			}
		});
		
		
	}

</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


    <script>

    jQuery(document).ready(function() {
        $('.copyyy').click(function() {
            var data = $(this).data('klik');
            var $temp = $("<input>");
            var tes = $("body").append($temp);
            var tes2 = $temp.val(data).select();
            document.execCommand("copy");
            $temp.remove();
            $.toaster({priority: 'info', title: 'Update', message: 'Berhail Copy URL'});

        });    
    });

 </script>