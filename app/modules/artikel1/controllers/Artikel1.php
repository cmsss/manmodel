<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artikel1 extends MX_Controller {

    private $module;
    private $redirect;

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

        $this->load->model('m_artikel1');
        $this->load->model('m_fm');
        $this->load->model('m_kategori1');
        $this->module = "artikel1";
        $this->redirect = "adminweb/artikel.asp";

    }

    public function index() {
        $data['list']   = $this->m_artikel1->get_all_v();
        // $data['list']   = 'sdf';
        $data['module'] = $this->module;
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

    

    public function tambah()
    {
        $data['kategori'] = drop_list('m_kategori1' ,'id' ,'nama' , 'Pilih kategori' , 'get_all' ,'asc');
        $data['module'] = $this->module;
        $data['view_file']   = 'tambah';
        echo Modules::run('template/render_master' , $data);
    }


    public function edit($id)
    {
        $cek =  $this->m_artikel1->get_by_v(array('a.id' => $id));
        if ($cek) {
            $data['e'] = $cek;
            $data['kategori'] = drop_list('m_kategori1' ,'id' ,'nama' , 'Pilih kategori' , 'get_all' ,'asc');
            $data['module'] = $this->module;
            $data['view_file']   = 'edit';
            echo Modules::run('template/render_master' , $data);
        }else{
            redirect($this->redirect,'refresh');
        }
    }


    

    public function simpan()
    {
        if ($this->input->post()) {
            // die(print_r('simpan'));
            $tipenyafile = pathinfo($_FILES["nama"]['name'] ,PATHINFO_EXTENSION );
            $namanyafile = pathinfo($_FILES["nama"]['name']  ,PATHINFO_BASENAME);
            
            // die(print_r($tipenyafile));


            // $folder = str_replace( array(':' , ' '), '', $this->input->post('dir'));
            // if ($tipenyafile == 'jpg') {
            //     $path = './files/file/foto';    
            
            //     if (!is_dir($path)) { //create the folder if it's not already exists
            //         mkdir($path, 0777, TRUE);
            //     }
            // }

            
           
            // chmod('./files/file/', 0777);
            
          

            // pdf/doc/docx/ppt/pptx/zip/rar/jpg
            // $idincrement = auto_inc('m_fm','id');
            $idincrement = auto_inc('m_fm','id');
            $nmfile = str_replace(array(':' , ' '), '-',$idincrement.$namanyafile);
            $config['upload_path']= './files/file/fotoberita';
            $config['allowed_types']="jpg|png";
            $config['max_size']="10240000";
            // $config['max_width']="0";
            // $config['max_height']="0";
            // $config['encrypt_name']= TRUE;
            $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            $this->upload->initialize($config);


         
            $idincrement = auto_inc('m_fm','id');
            if ($this->upload->do_upload('nama')) {
                $file = $this->upload->data();
            
                // die(print_r($file));
                $idincrement = auto_inc('m_fm', 'id');
                $data= array(
                    'id'=> $idincrement,
                    'nama'=>$file['file_name'],
                    'asli' =>$file['file_name'],
                    'ukuran' => $file['file_size'],
                    'mime' => $file['file_type'],
                    'tanggal' => date('Y-m-d'),
                    'domain' => NAMA_DOMAIN,
                    'dir' => '',
                    );
                $this->m_fm->insert($data);

                $idincrement_berita = auto_inc('m_artikel1' , 'id');
                $data = array(
                    'id' => $idincrement_berita,
                    'judul' => $this->input->post('judul'),
                    'isi' => $this->input->post('isi'),
                    'ts' => $idincrement,
                    'tampil' => $this->input->post('tampil'),
                    'setuju' => $this->input->post('setuju'),
                    'lengket' => $this->input->post('lengket'),
                    'kategori' => $this->input->post('kategori'),
                    'tgl_input' => date('Y-m-d'),
                    'caption' => $this->input->post('caption'),
                    'iduser' => $this->session->userdata('id_login'),
                    );
                $this->m_artikel1->insert($data);
                    echo "<script>alert('Berhasil Sidmpan Artikel ...');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }else{
                    $data = array(
                        'id' => auto_inc('m_artikel1' , 'id'),
                        'judul' => $this->input->post('judul'),
                        'isi' => $this->input->post('isi'),
                        'tampil' => $this->input->post('tampil'),
                        'setuju' => $this->input->post('setuju'),
                        'domain' => NAMA_DOMAIN,
                        'kategori' => $this->input->post('kategori'),
                        'lengket' => $this->input->post('lengket'),
                        'infopenting' => $this->input->post('infopenting'),
                        'caption' => $this->input->post('caption'),
                        'iduser' => $this->session->userdata('id_login'),
                        'ts' => date_timestamp_get(date_create()),

                        );

                    $this->m_artikel1->insert($data);



                     echo "<script>alert('Berhasil Simpan Artikel');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }
        }else{
            redirect($this->redirect,'refresh');
        }
    }


    public function simpan_perubahan(){
        if ($this->input->post()) {


           $cek =  $this->m_artikel1->get_by_v(array('a.id' => $this->input->post('id')));


            // $tipenyafile = pathinfo($_FILES["nama"]['name'] ,PATHINFO_EXTENSION );
            // $namanyafile = pathinfo($_FILES["nama"]['name']  ,PATHINFO_BASENAME);
            
            // die(print_r($tipenyafile));


            // $folder = str_replace( array(':' , ' '), '', $this->input->post('dir'));
            // if ($tipenyafile == 'jpg') {
            //     $path = './files/file/foto';    
            
            //     if (!is_dir($path)) { //create the folder if it's not already exists
            //         mkdir($path, 0777, TRUE);
            //     }
            // }

            
           
            // chmod('./files/file/', 0777);
            
          

            // pdf/doc/docx/ppt/pptx/zip/rar/jpg
            // $idincrement = auto_inc('m_fm','id');
            /*$idincrement = auto_inc('m_fm','id');
            $nmfile = str_replace(array(':' , ' '), '-',$idincrement.$namanyafile);
            $config['upload_path']= './files/file/fotoberita';
            $config['allowed_types']="jpg";
            $config['max_size']="10240000";
            // $config['max_width']="0";
            // $config['max_height']="0";
            // $config['encrypt_name']= TRUE;
            $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            $this->upload->initialize($config);*/

            if ($this->upload->do_upload('nama') != '') {

                $patfile = realpath(APPPATH . '../files/file/fotoberita/');
                if ($cek->foto != '') {
                    unlink($patfile . '/' . $cek->foto);
                }

                $file = $this->upload->data();
            
                // die(print_r($file));
                // $idincrement = auto_inc('m_fm', 'id');
                $data= array(
                    // 'id'=> $idincrement,
                    'nama'=>$file['file_name'],
                    'asli' =>$file['file_name'],
                    'ukuran' => $file['file_size'],
                    'mime' => $file['file_type'],
                    // 'tanggal' => date('Y-m-d'),
                    'domain' => NAMA_DOMAIN,
                    'dir' => '',
                    );
                $this->m_fm->update($this->input->post('ts'),$data);

                // $idincrement_foto = auto_inc('m_foto' , 'id');
                     $data = array(
                        'judul' => $this->input->post('judul'),
                        'isi' => $this->input->post('isi'),
                        'tampil' => $this->input->post('tampil'),
                        'setuju' => $this->input->post('setuju'),
                        'kategori' => $this->input->post('kategori'),
                        'lengket' => $this->input->post('lengket'),
                        'infopenting' => $this->input->post('infopenting'),
                        'caption' => $this->input->post('caption'),

                        );

                    $this->m_artikel1->update($this->input->post('id') ,$data);
                    echo "<script>alert('Berhasil update artikel ... ');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }else{

                        $data = array(
                            'judul' => $this->input->post('judul'),
                            'isi' => $this->input->post('isi'),
                            'tampil' => $this->input->post('tampil'),
                            'setuju' => $this->input->post('setuju'),
                            'kategori' => $this->input->post('kategori'),
                            'lengket' => $this->input->post('lengket'),
                            'infopenting' => $this->input->post('infopenting'),
                            'caption' => $this->input->post('caption'),

                            );

                        $this->m_artikel1->update($this->input->post('id') ,$data);


                     echo "<script>alert('Berhasil update Artikel');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }




        }
    }


    public function tampil_a($id){
        $cek = $this->m_artikel1->get_by(array('id' => $id));
        if ($cek->tampil == '1') {
            $data = array(
                 'tampil' => '0' 
                );
            $this->m_artikel1->update($id , $data);
            $tampil = '0';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->tampil == '0') {
            $data = array(
                 'tampil' => '1' 
                );
            $this->m_artikel1->update($id , $data);
            $tampil = '1';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }


    public function setuju_a($id){
        $cek = $this->m_artikel1->get_by(array('id' => $id));
        if ($cek->setuju == '1') {
            $data = array(
                 'setuju' => '0' 
                );
            $this->m_artikel1->update($id , $data);
            $setuju = '0';
            if ($setuju == '1') {
                
                 echo   '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="setujuA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->setuju == '0') {
            $data = array(
                 'setuju' => '1' 
                );
            $this->m_artikel1->update($id , $data);
            $setuju = '1';
            if ($setuju == '1') {
                
                 echo   '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }


    public function  hapus($id)
    {
        $cek = $this->m_artikel1->get_by_v(array('a.id' => $id , 'a.hapus' => '0'));
        if ($cek) {
            
            // $tipenyafile = pathinfo($cek->nama ,PATHINFO_EXTENSION );
            // die(print_r($tipenyafile));  


            // if ($tipenyafile == 'jpg') {
            //     $patfile = realpath(APPPATH . '../files/file/foto/' . $cek->dir);    
            
            // }elseif ($tipenyafile == 'pdf' || $tipenyafile == 'doc' || $tipenyafile == 'docx' || $tipenyafile == 'ppt' || $tipenyafile == 'pptx' || $tipenyafile == 'rar' || $tipenyafile == 'zip' ) {
            //     $patfile = realpath(APPPATH . '../files/file/file/' . $cek->dir);
            // }else {
            //     redirect('adminweb/file.asp','refresh');
            // }
            // $patfile = realpath(APPPATH . '../files/file/foto');

            // if ($cek->foto != '') {
            //     unlink($patfile . '/' . $cek->foto);
            // }
            // $this->m_fm->delete($cek->ts);
            // $this->m_foto->delete($id);
            $data = array('hapus' => '1');
            $this->m_artikel1->update($id , $data);
            redirect($this->redirect,'refresh');
        }else{
            redirect($this->redirect,'refresh');
        }
    }

}

