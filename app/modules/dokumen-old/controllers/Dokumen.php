<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dokumen extends MX_Controller {

    
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_dokumen');
        $this->load->model('m_mdokumen');
        $this->load->model('m_kategory');
    }

    public function index() {
        $data['list'] = query_sql("select * from v_dokumen");
        $data['module'] = "dokumen";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){
         $id_dokumen = auto_inc('m_dokumen','id_dokumen');
         $id_kategori= explode(',',$this->input->post('sub_kat'));
         
        if ($this->input->post()){
            $data= array(
                'id_dokumen'=>$id_dokumen,
                'nama_dokumen'=>$this->input->post('nama_dokumen'),
                'ket_dokumen'=>$this->input->post('ket_dokumen'),
                'file'=>$this->input->post('file'),
                'link'=>$this->input->post('link')
                );
            $this->m_dokumen->insert($data);
            $data1 = array(
                'dokumen_id'=>$id_dokumen,
                'kategori'=>$id_kategori['1'],
                'create_date'=> date('Y-m-d H:i:s'),
                );
            $this->m_mdokumen->insert($data1);
            redirect('adminweb/dokumen.asp','refresh');

        }
        $data['main_kat'] = drop_list('m_main_kat','id_main_kat','main_kat','Pilih Kategori');
        $data['sub_kat'] = array(''=>'Pilih Sub Kategori');
        $data['module']="dokumen";
        $data['view_file']="add";
        echo Modules::run('template/render_master',$data);
    }
    public function edit($id){
        if ($this->m_dokumen->get_by(array('id_dokumen'=>$id))){
            $id_kategori= explode(',',$this->input->post('sub_kat'));
            if ($this->input->post()){
                $data= array(
                    'nama_dokumen'=>$this->input->post('nama_dokumen'),
                    'ket_dokumen'=>$this->input->post('ket_dokumen'),
                    'file'=>$this->input->post('file'),
                    'link'=>$this->input->post('link')
                    );
                $this->m_dokumen->update($id,$data);
                $data1 = array(
                    'kategori'=>$id_kategori['1'],
                    'create_date'=> date('Y-m-d H:i:s'),
                    );
                $this->m_mdokumen->update($id,$data1);
                redirect('adminweb/dokumen.asp','refresh');

            }
            $edit = query_sql("select * from v_dokumen where id_dokumen = '$id'","row");
            $data['edit'] = $edit;
            $data['main_kat'] = drop_list('m_main_kat','id_main_kat','main_kat','Pilih Kategori');
            // $sub = query_sql("select ")
            $result = query_sql("select * from v_kategori where id_main_kat = '$edit->id_main_kat'");
            if (!empty($result)) {
                // jika hasil query array maka looping hasil query
                foreach ($result as $row) {
                   $list_sub[''] = "Pilih Sub Kategori";
                   $list_sub[$row->id_sub_kat.','.$row->id_kategori] = $row->sub_kat;
                }
            }else{
                $list_sub = array(''=>'Tidak ada data');
            }
            $data['sub_kat'] = $list_sub;
            $data['module']="dokumen";
            $data['view_file']="edit";
            echo Modules::run('template/render_master',$data);
        }
    }
    public function delete($id){
        if ($this->m_dokumen->get_by(array('id_dokumen'=>$id))){
            $this->m_dokumen->delete($id);
            $this->m_mdokumen->delete($id);
        }
        redirect('adminweb/dokumen.asp','refresh');
    }

    public function grab_sub(){
        if ($this->input->post()){
            $id = $this->input->post('id_main');
            $result = query_sql("select * from v_kategori where id_main_kat = '$id'");
            if (!empty($result)) {
                // jika hasil query array maka looping hasil query
                foreach ($result as $row) {
                    echo '<option value="'. $row->id_sub_kat.','.$row->id_kategori. '">' . $row->sub_kat. '</option>';
                }
            } else {
                echo '<option value="">Tidak ada data</option>';
            }
        }
    }

}
