<div class="container-fluid">
  <section class="content-header">
    <h4>Edit Dokumen</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">
			<?php echo form_open('',array('class'=>'form-horizontal')); ?>
				<div class="row form-group">
						<label class="control-label col-sm-2">Kategori</label>
						<div class="col-sm-5">
							<?php echo form_dropdown('kategori',$main_kat,$edit->id_main_kat,'class="form-control" placeholder="Kategori" id="mainkat" onchange="get_sub();" required '); ?>
						</div>
						<div class="col-sm-5">
							<?php echo form_dropdown('sub_kat',$sub_kat,$edit->id_sub_kat.','.$edit->id_kategori,'class="form-control" placeholder="Kategori" id="sub" required '); ?>
						</div>
				</div>

				<div class="row form-group">
						<label class="control-label col-sm-2">Nama Dokumen</label>
						<div class="col-sm-10">
							<?php echo form_input('nama_dokumen',set_value('nama_dokumen',$edit->nama_dokumen),'class="form-control" placeholder="Nama Dokumen" required '); ?>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Deskripsi</label>
						<div class="col-sm-10">
							<?= form_textarea('ket_dokumen',set_value('ket_dokumen',$edit->ket_dokumen),'class="form-control" placeholder="Deskripsi" style="height:70px;"') ?>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Link</label>
						<div class="col-sm-10">
							<?php echo form_input('link',set_value('link',$edit->link),'class="form-control" placeholder="LINK" required '); ?>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Ganti File</label>
						<div class="col-sm-10">
							<input type="file" class="form-control" name="file"  />
						</div>
						<?= $this->session->flashdata('pesan') ?>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">&nbsp;</label>
						<div class="col-sm-10">
							<a href="<?= site_url('adminweb/dokumen.asp') ?>" class="btn btn-default">Kembali</a>
							<?php echo form_submit('submit','Simpan Perubahan','class="btn btn-primary"'); ?>
						</div>
				</div>
				
			<?php echo  form_close(); ?>
		</section>
	</div>
</div>
