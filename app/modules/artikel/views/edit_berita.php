<section class="content-header">
    <h1>POST Artikel</h1>
</section>
<section class="content">
    <div class="row">
        <?php echo form_open_multipart(site_url(uri_string())); ?>
        <div class="col-md-8">
            <p> <?php echo form_input('judul', $edit->judul, 'class="form-control" placeholder="Tuliskan Judul Disini" required'); ?> </p>
            <?php // echo form_textarea('isi', '', 'class="ckeditor" placeholder="Tuliskan Isi Artikel Disini"'); ?>
            <!-- <textarea class="ckeditor" name="isi" rows="30" cols="80" style="width: 100%;"><?php echo $edit->isi; ?></textarea> -->
            <textarea class="ckeditor" id="ckedtor" name="isi"><?php echo $edit->isi; ?></textarea>
            <div class="clearfix"></div>
            <br/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Gambar</h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-12">
                            <img src="<?php echo base_url('image/artikel/' . $edit->img); ?>" class="img-responsive" style="width:100px; height: 100px;" onError="this.src='<?= base_url() ?>/asset/kemenag/assets/themes/img-default.jpg';">
                        
                    </div>
                    <br/><br/><br/><br/>
                    <label class="control-label col-lg-3" for="sc_name">Ganti Gambar : </label>
                    <div class="col-lg-9">
                        <input type="file" name="img" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Status</h3>
                </div>
                <div class="panel-body">
                    <?php if ($edit->status == '0') { ?>
                        <input type="radio" name="status" value="0" checked="true"> Tidak Aktif
                        <input type="radio" name="status" value="1"> Aktif
                    <?php } elseif ($edit->status == '1') { ?>
                        <input type="radio" name="status" value="0"> Tidak Aktif
                        <input type="radio" name="status" value="1" checked="true"> Aktif
                    <?php } else { ?>
                        <input type="radio" name="status" value="0"> Tidak Aktif
                        <input type="radio" name="status" value="1"> Aktif
                    <?php } ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Terbitkan</h3>
                </div>
                <div class="panel-body">
                    Rubah Tanggal Posting : <input type="text" class="form-control datepicker" placeholder="Kosongkan jika ingin tgl posting hari ini" name="tgl_post" value="<?= $tgl; ?>">
                    
                    
                </div>
                <div class="panel-footer">
                    <?php echo form_submit('Terbitkan', 'Simpan', 'class="btn btn-primary"'); ?>
                    <a href="<?php echo site_url('adminweb/artikel.asp'); ?>" class="btn btn-default">Keluar</a>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>
