<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artikel extends MX_Controller {

    private $level;
    private $id_login;

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

        $this->level = $this->session->userdata('level');
        $this->id_login = $this->session->userdata('id_login');

        $this->load->model('m_artikel');
        $this->load->model('kategori/m_kategori', 'kategori');
        
    }

    public function index() {
        $data['list'] = $this->m_artikel->get_all_v_artikel();
        $data['view_file'] = "list";
        $data['module'] = "artikel";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){
        if ($this->input->post()) {
                $config['upload_path']="./image/artikel";
                $config['allowed_types']="jpg|png";
                $config['max_size']="1024";
                $config['max_width']="2000";
                $config['max_height']="1000";
                $config['encrypt_name']= TRUE;
                $this->upload->initialize($config);
                if ($this->input->post('tgl_post') ==''){
                    $tanggal = date('Y-m-d H:i:s');
                }else{
                    $pecahtgl = explode('/', $this->input->post('tgl_post'));
                    $tanggal = $pecahtgl['2'].'-'.$pecahtgl['0'].'-'.$pecahtgl['1'].' '.date('H:i:s');
                }
                $idincrement = auto_inc('m_artikel','id_artikel');
                if ($this->upload->do_upload('img')) {
                    $data= array(
                        'id_artikel'=> $idincrement,
                        'judul'=>$this->input->post('judul'),
                        'img'=>$this->upload->file_name,
                        'isi'=>$this->input->post('isi'),
                        'create_date'=>$tanggal,
                        'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                        'status'=>$this->input->post('status'),
                        'penulis' => $this->session->userdata('id_login')
                        );
                    if (!$this->m_artikel->insert($data)){
                        echo "<script>alert('Artikel berhasil disimpan');
                window.location=('" . site_url('adminweb/artikel.asp') . "');</script>";
                    }else{
                        echo "<script>alert('Gagal simpan Artikel');
                window.location=('" . site_url('adminweb/artikel.asp') . "');</script>";
                    }

                }else {
                    $data= array(
                        'id_artikel'=> $idincrement,
                        'judul'=>$this->input->post('judul'),
                        // 'img'=>$this->upload->file_name,
                        'isi'=>$this->input->post('isi'),
                        'create_date'=>$tanggal,
                        'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                        'status'=>$this->input->post('status'),
                        'penulis' => $this->session->userdata('id_login')
                        );
                    if (!$this->m_artikel->insert($data)){
                        echo "<script>alert('Artikel berhasil disimpan');
                window.location=('" . site_url('adminweb/artikel.asp') . "');</script>";
                    }else{
                        echo "<script>alert('Gagal simpan Artikel');
                window.location=('" . site_url('adminweb/artikel.asp') . "');</script>";
                    }
                }
                
            }else{
                $data['view_file']="add_berita";

                $data['module']="artikel";
                echo Modules::run('template/render_master',$data);
            }
        
    }
    public function edit($id){
        if ($cek = $this->m_artikel->get_by(array('id_artikel'=>$id))){
            $nama_file = "file_". time();
            $config['upload_path']="./image/artikel";
            $config['allowed_types']="jpg|png";
            $config['max_size']="1024";
            $config['max_width']="2000";
            $config['max_height']="1000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);
            if ($this->input->post()) {

                if ($this->input->post('tgl_post') ==''){
                        $tanggal = date('Y-m-d H:i:s');
                    }else{
                        $pecahtgl = explode('/', $this->input->post('tgl_post'));
                        $tanggal = $pecahtgl['2'].'-'.$pecahtgl['0'].'-'.$pecahtgl['1'].' '.date('H:i:s');
                    }
                    if ($this->upload->do_upload('img')) {
                        $pathgambar = realpath(APPPATH . '../image/artikel');
                        if ($cek->img != '') {
                            unlink($pathgambar . '/' . $cek->img);
                        }
                           $img = $this->upload->data();
                            $data= array(
                                'judul'=>$this->input->post('judul'),
                                'isi'=>$this->input->post('isi'),
                                'img'=>$this->upload->file_name,
                                'create_date' => $tanggal,
                                'update_date'=>date('Y-m-d H:i:s'),
                                'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                //'album_foto'=>$this->input->post('album_foto'),
                                'status'=>$this->input->post('status'),
                                );
                            $this->m_artikel->update($id,$data);
                            redirect('adminweb/artikel.asp','refresh');
                        }else {
                         $data= array(
                                'judul'=>$this->input->post('judul'),
                                'isi'=>$this->input->post('isi'),
                                'create_date' => $tanggal,
                                'update_date'=>date('Y-m-d H:i:s'),
                                'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                //'album_foto'=>$this->input->post('album_foto'),
                                'status'=>$this->input->post('status'),
                                );
                            $this->m_artikel->update($id,$data);
                            redirect('adminweb/artikel.asp','refresh');
                    }
                   
            }else{
                $data['view_file']="edit_berita";
                $edit = $this->m_artikel->get_by(array('id_artikel'=>$id));
                $pecah = explode(' ', $edit->create_date);
                $tanggal = explode('-', $pecah['0']);
                $data['tgl'] = $tanggal['1'].'/'.$tanggal['2'].'/'.$tanggal['0'];
                $data['edit']= $edit;
                
                $data['module']="artikel";
                echo Modules::run('template/render_master',$data);
            }
        }else{
            redirect('adminweb/artikel.asp' ,'refresh');
        }
        
    }
    public function hapus($id){
        if ($cek = $this->m_artikel->get_by(array('id_artikel'=>$id))){
            if ($this->session->userdata('level') == '1'){
                $pathgambar = realpath(APPPATH . '../image/artikel');
                if ($cek->img != '') {
                    unlink($pathgambar . '/' . $cek->img);
                }
                $this->m_artikel->delete($id);
                redirect('adminweb/artikel.asp');
            }
            if ($this->session->userdata('id_login') == $cek->penulis){
                 $pathgambar = realpath(APPPATH . '../image/artikel');
                if ($cek->img != '') {
                    unlink($pathgambar . '/' . $cek->img);
                }
                $this->m_artikel->delete($id);
                redirect('adminweb/artikel.asp');
            }
                echo "<script>alert('Maaf anda tidak memiliki hak untuk menghapus berita ini !!!' );
            window.location=('" . site_url('adminweb/artikel.asp') . "');</script>";
            }else{
                redirect('adminweb/artikel.asp');
            }
        
    }
}

