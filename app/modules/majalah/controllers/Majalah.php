<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Majalah extends MX_Controller {

    private $module;
    private $redirect;

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

        $this->load->model('m_majalah');
        $this->load->model('m_fm');
        $this->load->model('m_kategori1');
        $this->module = "majalah";
        $this->redirect = "adminweb/majalah.asp";

    }

    public function index() {
        $data['list']   = $this->m_majalah->get_all();
        $data['module'] = $this->module;
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

    

    public function tambah()
    {
        $data['module'] = $this->module;
        $data['view_file']   = 'tambah';
        echo Modules::run('template/render_master' , $data);
    }


    public function edit($id)
    {
        $cek =  $this->m_majalah->get_by(array('id' =>$id));
        if ($cek) {
            $data['e'] = $cek;
            $data['module'] = $this->module;
            $data['view_file']   = 'edit';
            echo Modules::run('template/render_master' , $data);
        }else{
            redirect($this->redirect,'refresh');
        }
    }


    

    public function simpan()
    {
        if ($this->input->post()) {
            // die(print_r('simpan'));
            // $tipenyafile = pathinfo($_FILES["nama"]['name'] ,PATHINFO_EXTENSION );
            // $namanyafile = pathinfo($_FILES["nama"]['name']  ,PATHINFO_BASENAME);
            
            // die(print_r($tipenyafile));


            // $folder = str_replace( array(':' , ' '), '', $this->input->post('dir'));
            // if ($tipenyafile == 'jpg') {
            //     $path = './files/file/foto';    
            
            //     if (!is_dir($path)) { //create the folder if it's not already exists
            //         mkdir($path, 0777, TRUE);
            //     }
            // }

            $path = './files/gorontalo/file/'.date('Y') .'/' .date('m') ;    
        
            if (!is_dir($path)) { //create the folder if it's not already exists
                mkdir($path, 0777, TRUE);

            //     }
            // }}
            }
            fopen($path."/" ."index.html","w");

            
           
            // chmod('./files/file/', 0777);
            
            // $namanyafile = 'tes';

            // pdf/doc/docx/ppt/pptx/zip/rar/jpg
            // $idincrement = auto_inc('m_fm','id');
            // $idincrement = auto_inc('m_majalah','id');
            // $nmfile = str_replace(array(':' , ' '), '-',$idincrement.$namanyafile);
            // $config['upload_path']= $path;
            // $config['allowed_types']="pdf";
            // $config['max_size']="10240000";
            // $config['overwrite']     = FALSE;
            // $config['max_width']="0";
            // $config['max_height']="0";
            // $config['encrypt_name']= TRUE;
            // $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            // $this->upload->initialize($config);


            // $this->upload->do_upload('nama');
            // echo '<pre>';
            

            
            // echo '</pre>';


            if ($_FILES) {
                $pdf    = $_FILES["namapdf"] ;
                $cover  = $_FILES["namaco"] ;

                if (pathinfo($pdf['name'] ,PATHINFO_EXTENSION ) == 'pdf') {
                    $expdf = TRUE;
                    // move_uploaded_file($pdf['tmp_name'], $path.'/' . $pdf['name'] );
                }else{
                    $expdf = FALSE;
                }

                if (pathinfo($cover['name'] ,PATHINFO_EXTENSION ) == 'png' || pathinfo($cover['name'] ,PATHINFO_EXTENSION ) == 'jpg') {
                    $excover = TRUE;
                    // move_uploaded_file($cover['tmp_name'], $path.'/' . $cover['name'] );
                }else{
                    $excover = FALSE;
                }


                if ($expdf && $excover) {
                    $idincrement = auto_inc('m_majalah', 'id');
                    move_uploaded_file($pdf['tmp_name'], $path.'/' . $idincrement .$pdf['name'] );

                    move_uploaded_file($cover['tmp_name'], $path.'/' . $idincrement .$cover['name']  );
                    $idincrement = auto_inc('m_majalah', 'id');
                    $data= array(
                        'id'=> $idincrement,
                        'judul' => $this->input->post('judul'),
                        
                        'tampil' => $this->input->post('tampil'),
                        'setuju' => $this->input->post('setuju'),
                        'unit' => '',
                        'domain' => NAMA_DOMAIN,
                        'ts' => date_timestamp_get(date_create()),
                        'iduser' => $this->session->userdata('id_login'),
                        );
                    $data['pdf'] = 'files/file/'.date('Y') . '/' .$idincrement .$pdf['name'] ; 
                    $data['cover'] = 'files/file/'.date('Y') . '/' .$idincrement .$cover['name'] ; 
                     $this->m_majalah->insert($data);
                echo "<script>alert('Berhasil Upload File');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }else{
                     echo "<script>alert('Gagal upload');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }


                // echo "<pre>";
                // print_r($pdf);
                // print_r(pathinfo($pdf['name'] ,PATHINFO_EXTENSION ));
                
                // print_r($cover);
                // print_r(pathinfo($cover['name'] ,PATHINFO_EXTENSION ));
                // echo "</pre>";

            }





         
            // if ($_FILES) {
            //     // $file = $this->upload->data();
                   
            //     $idincrement = auto_inc('m_majalah', 'id');
            //     $data= array(
            //         'id'=> $idincrement,
            //         'judul' => $this->input->post('judul'),
                    
            //         'tampil' => $this->input->post('tampil'),
            //         'setuju' => $this->input->post('setuju'),
            //         'unit' => '',
            //         'domain' => NAMA_DOMAIN,
            //         'ts' => '0'
            //         );
               
                
            //     foreach ($_FILES as $file['nama']) {
            //         // die(print_r($file));
            //      // s
            //         for($i=0;$i<count($file['nama']['name']);$i++){
            //             move_uploaded_file($file['nama']['tmp_name'][$i], $path.'/' . $file['nama']['name'][$i] );
            //         }
                    
            //        $data['pdf'] = 'files/file/'.date('Y') . '/' .$file['nama']['name'][0] ; 
            //        $data['cover'] = 'files/file/'.date('Y') . '/' .$file['nama']['name'][1] ; 
            //     }
            //     $this->m_majalah->insert($data);

            //         echo "<script>alert('Berhasil Upload File');
            // window.location=('" . site_url($this->redirect) . "');</script>";
            //     }else{
            //          echo "<script>alert('Gagal upload');
            // window.location=('" . site_url($this->redirect) . "');</script>";
            //     }
        }
    }


    


    public function simpan_perubahan(){
        if ($this->input->post()) {

            $cek =  $this->m_majalah->get_by(array('id' =>$this->input->post('id')));

            $data= array(
                'judul' => $this->input->post('judul'),
                
                'tampil' => $this->input->post('tampil'),
                'setuju' => $this->input->post('setuju'),
                'unit' => '',
                'domain' => NAMA_DOMAIN,
                'ts' => '0'
                );
            $this->m_majalah->update($cek->id  , $data);
            redirect($this->redirect,'refresh');


        }else{
            redirect($this->redirect,'refresh');
        }
    }


    public function tampil_a($id){
        $cek = $this->m_majalah->get_by(array('id' => $id));
        if ($cek->tampil == '1') {
            $data = array(
                 'tampil' => '0' 
                );
            $this->m_majalah->update($id , $data);
            $tampil = '0';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->tampil == '0') {
            $data = array(
                 'tampil' => '1' 
                );
            $this->m_majalah->update($id , $data);
            $tampil = '1';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }


    public function setuju_a($id){
        $cek = $this->m_majalah->get_by(array('id' => $id));
        if ($cek->setuju == '1') {
            $data = array(
                 'setuju' => '0' 
                );
            $this->m_majalah->update($id , $data);
            $setuju = '0';
            if ($setuju == '1') {
                
                 echo   '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="setujuA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->setuju == '0') {
            $data = array(
                 'setuju' => '1' 
                );
            $this->m_majalah->update($id , $data);
            $setuju = '1';
            if ($setuju == '1') {
                
                 echo   '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="setujuA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }

    


    public function  hapus($id)
    {
        $cek = $this->m_majalah->get_by(array('id' => $id));
        if ($cek) {
            
            // $tipenyafile = pathinfo($cek->nama ,PATHINFO_EXTENSION );
            // die(print_r($tipenyafile));  


            // if ($tipenyafile == 'jpg') {
            //     $patfile = realpath(APPPATH . '../files/file/foto/' . $cek->dir);    
            
            // }elseif ($tipenyafile == 'pdf' || $tipenyafile == 'doc' || $tipenyafile == 'docx' || $tipenyafile == 'ppt' || $tipenyafile == 'pptx' || $tipenyafile == 'rar' || $tipenyafile == 'zip' ) {
            //     $patfile = realpath(APPPATH . '../files/file/file/' . $cek->dir);
            // }else {
            //     redirect('adminweb/file.asp','refresh');
            // }
            unlink(realpath(APPPATH .'../' . $cek->cover));
            unlink(realpath(APPPATH .'../' . $cek->pdf));
            $this->m_majalah->delete($id);
            redirect($this->redirect,'refresh');
        }else{
            redirect($this->redirect,'refresh');
        }
    }

}