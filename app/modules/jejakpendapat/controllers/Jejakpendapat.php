<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jejakpendapat extends MX_Controller {



    public function __construct() {
        parent::__construct();

        $this->load->model('m_jejakpendapat');
    }

    public function index() {
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
            $all =$this->m_jejakpendapat->get_all();
            $data ['count']=$this->m_jejakpendapat->get_stat($all);
            $data['data1'] = $this->m_jejakpendapat->count1();
            $data['data2'] = $this->m_jejakpendapat->count2();
            $data['data3'] = $this->m_jejakpendapat->count3();
            $data['data4'] = $this->m_jejakpendapat->count4();
            $data['data5'] = $this->m_jejakpendapat->count5();
            $data['data6'] = $this->m_jejakpendapat->count6();
            $data['module'] = "jejakpendapat";
            $data['view_file'] = "list";
            echo Modules::run('template/render_master',$data);
    }
    public function jejak_pendapat()
    {

        $idincrement = auto_inc('m_jejakpendapat','id_jejak_pendapat');
        if($this->input->post()){
            $data=array(
                'id_jejak_pendapat'=>$idincrement,
                'pendapat'=>$this->input->post('pendapat'),
                'ip'=>$this->input->ip_address(),
                );
            if (!$this->m_jejakpendapat->insert($data)){
                    echo "<script>alert('Pendapat Anda berhasil Terkirim');

            window.location=('" . site_url('hasil-poling-jejak-pendapat.html') . "');
            </script>";
                }else{
                    echo "<script>alert('Maaf anda Telah mengirim Pendapat Sebelumnya');

            window.location=('" . site_url('hasil-poling-jejak-pendapat.html') . "');
            </script>";
                }


        }
    }




}
