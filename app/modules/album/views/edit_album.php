<div class="container-fluid">
  <section class="content-header">
    <h4>Edit Album</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">
			<?php echo form_open('',array('class'=>'form-horizontal')); ?>
				<div class="row form-group">
						<label class="control-label col-sm-2">Album</label>
						<div class="col-sm-10">
							<?php echo form_input('album',$edit->album,'class="form-control" placeholder="Waktu"'); ?>
						</div>
				</div>
				<div class="row form-group">
						
						<div class="col-sm-10">
						<a href="<?= site_url('adminweb/album.asp') ?>" class="btn btn-default">Kembali</a>
							<?php echo form_submit('submit','Edit Album','class="btn btn-primary"'); ?>
						</div>
				</div>
				
			<?php echo  form_close(); ?>
		</section>
	</div>
</div>


