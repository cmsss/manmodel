<div class="content">
    <p>Dokumen Perencanaan Kota </p>
</div>
<div class="isi">
    <?php 
    foreach ($perencanaan_kota as $kota) {
        ?>
        <div class="media">
            <h4 class="media-heading">
                <div class="pull-left">
                    <b><?php echo $kota->perencanaa_kota; ?></b><br/>
                    <small><?php echo $kota->deskripsi; ?></small>
                </div>
                <div class="pull-right">
                    <a href="<?php echo site_url('uploads/'.$kota->file) ?>" title="<?php echo $kota->file; ?>"><small><span class="glyphicon glyphicon-download"></span> Download File</small></a>
                </div>
            </h4> 
        </div>
    <hr/>
    <?php } ?>
    <?php
    ?>
</div>