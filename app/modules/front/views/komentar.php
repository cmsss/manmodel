
<h2><span class="glyphicon glyphicon-comment"></span> Komentar</h2>
    <hr/>
    <?php echo form_open(site_url('komentar.asp'), 'class="form-horizontal"'); ?>
    <fieldset>
    <div class="form-group">
        <div class="control-label col-lg-3">Nama Lengkap</div>
        <div class="col-lg-9">
            <?php echo form_input('nl', '', 'class="form-control" placeholder="Nama Lengkap"'); ?>
            <?php echo form_hidden('id_berita', $detail->id_berita); ?>
            <?php echo form_hidden('flag', site_url(uri_string())); ?>
            <?php echo form_hidden('flag', $detail->flag); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="control-label col-lg-3">Email</div>
        <div class="col-lg-9">
            <input type="email" class="form-control" placeholder="Email" name="email">
        </div>
    </div>
    <div class="form-group">
        <div class="control-label col-lg-3">Alamat Website</div>
        <div class="col-lg-9">
            <?php echo form_input('website', '', 'class="form-control" placeholder="Alamat Website (optional)"'); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="control-label col-lg-3">Komentar</div>
        <div class="col-lg-9">
            <?php echo form_textarea('komentar', '', 'class="form-control" placeholder="Tinggalkan komentar anda"'); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="control-label col-lg-3">&nbsp;</div>
        <div class="col-lg-9">
            <?php echo form_submit('submit', 'Kirim Komentar', 'class="btn btn-raised btn-info"'); ?>
        </div>
    </div>
    </fieldset>
    <?php echo form_close(); ?>