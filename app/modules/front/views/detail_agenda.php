
<div class="panel panel-isi">
    <div class="panel-heading">Detail Agenda</div>
    <div class="panel-body">
    <div class="media">
        <h4 class="media-heading">
            <b><?php echo $detail->tema; ?></b><br/>
        </h4> 
        <div class="media-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td width="83">Acara</td>
                            <td style="text-align: center;" width="28">:</td>
                            <td width="527"><?php echo $detail->acara; ?></td>
                        </tr>
                        <tr>
                            <?php $tanggal = $detail->tgl; ?>
                            <td width="83">Tanggal</td>
                            <td style="text-align: center;" width="28">:</td>
                            <td width="527"><?php echo nama_hari($tanggal).' '.tgl_indo($tanggal) ; ?></td>
                        </tr>
                        <tr>
                            <td width="83">Waktu</td>
                            <td style="text-align: center;" width="28">:</td>
                            <td width="527"><?php echo $detail->waktu; ?></td>
                        </tr>
                        <tr>
                            <td width="83">Tempat</td>
                            <td style="text-align: center;" width="28">:</td>
                            <td width="527"><?php echo $detail->tempat; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
