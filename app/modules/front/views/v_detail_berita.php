<div class="panel panel-default">
    <div class="panel-body">
    <div class="media">
        <h4 class="media-heading" style="font-size: 28px;">
            <b><?php echo $detail->judul; ?></b><br/>
            <small style="font-size: 12px;">Dibuat Oleh : <?php echo $penulis; ?>, Terbit : <?php echo $tgl ?> Dilihat : <?php echo $detail->dibaca; ?> Kali</small>
        </h4> 
        <?php if ($detail->img != '') { ?>
            <a href="#"><img alt="<?= $detail->judul; ?>" src="<?php if ($detail->img != '') {
            echo base_url('image/berita/' . $detail->img);
        } else {
            
        } ?>" class="img-responsive" style="width: 100%;"></a>
            <?php
            } else {
                echo '';
            }
            ?>
        <div class="media-body" style="color:#0e0e0e; font-size: 14px; line-height: 25px; font-weight: 400;-webkit-font-smoothing: antialiased;">
        <?php echo $detail->isi; ?>
            <br/>
            <?php echo sharethis('facebook,twitter', curPageURL()) ?>
        </div>
    </div>
    <div id="komentar">
    <h3 id="komentar_res"><i class="fa fa-comment"></i> <?php echo jumlah_komentar($detail->id_berita) ?> Komentar</h3>
    <ul class="list_komentar">
        <?php
        $komentar_view = view_komentar($detail->id_berita);
        if (is_array($komentar_view)){
            foreach ($komentar_view as $kv){ 
                $tgl = explode(' ', $kv->create_date);
                ?>
          <li class="komen even">
                <div class="komen-avatar"> 
                    <img alt="" src="<?php echo base_url(); ?>image/berita/gvatar.png" srcset="<?php echo base_url(); ?>image/berita/gvatar.png" class="avatar avatar-40 photo" height="40" width="40">
                </div>
                <div class="komen-meta commentmetadata">
                    <cite class="fn"><?php echo $kv->nama; ?></cite> 
                    <br> 
                    <a href="#"><?php echo nama_hari($tgl['0']).' '.  tgl_indo($tgl['0']) ?></a>
                </div>
                <div class="komen-body">
                    <p><?php echo $kv->komentar; ?></p>
                </div>
        </li>      
            <?php }
        }
        ?>
    </ul>
</div>

    <?php echo $this->load->view('komentar'); ?>
</div>
</div>
