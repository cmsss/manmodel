<div style="margin-top:10px;">
<div id="top-news" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <?php
    if (is_array($slider)) {

        foreach ($slider as $s) {

            if ($s->id_berita === $active_id) { ?>

                <div class="item active">
            <?php } else { ?>

                <div class="item">
            <?php }
            ?>
    <img src="<?php echo base_url(); ?>image/berita/<?php echo $s->img; ?>" alt="<?php echo $s->judul; ?>" style="width:100%; height:300px;">
    <div class="carousel-caption">
        <a href="<?php echo site_url('artikel/' . $s->flag) ?>" style="text-decoration: none; color: #fff;"><?php echo $s->judul; ?></a>

    </div>
    </div>
    <?php
            }
        }
        ?>
  </div>
<div class="headline">
    H e a d l i n e &nbsp; N e w s
</div>
</div>
</div>
        <div class="panel panel-default">
            <div class="panel-heading">BERITA</div>
            <div class="panel-body">
                <div class="isi">
                <?php 
                  if (!empty($berita)){
                    foreach ($berita as $b) {
                      $tgl = explode(' ', $b->create_date);
                      $isi = substr($b->isi, 0,250);
                     ?>
                        <div class="media">
                          <h4 class="media-heading">
                           <b><a style="color: black; text-decoration: none;" href="<?php echo site_url('artikel/' . $b->flag); ?>"><?php echo $b->judul; ?></a></b><br/>
                          </h4>
                          <a href="#" class="pull-left">
                         <?php if ($b->img !='') {?>
                          <img src="<?php echo base_url(); ?>image/berita/<?= $b->img ?>" class="mendia-object img-responsive img-thumb" style="width:100px; height: 100px;"/>
                        <?php } ?>
                          
                      </a>
                      <div class="media-body">
                      <small>Dibuat Oleh : <?php echo $b->full_name; ?>, Terbit : <?= nama_hari($tgl['0']).' '.tgl_indo($tgl['0']) ?>, Dilihat : <?php echo $b->dibaca; ?> Kali</small>
                             <?= $isi; ?>
                          <a href="<?php echo site_url('artikel/' . $b->flag); ?>" style="color:#000;">[...]</a>
                      </div>
                      </div>
                    <?php }
                  }
                ?>
            <!--      <nav>
            <?php //echo $this->pagination->create_links(); ?>
                </nav> -->
                </div>
            </div>
            
        </div>

<div class="col-lg-6">

    <div class="panel panel-default">

        <div class="panel-heading"><b>BERITA INTERNAL</b> </div>

        <div class="panel-body panel-widget">

            <ul class="sb-two">
                <?php
                $id_kat = get_kat('Berita Internal');

                $berita_first = get_front($id_kat);
                $id_art = '';

                if (is_array($berita_first)) {

                    foreach ($berita_first as $af) {

                        $id_art = $af->id_berita;

                        $isi = substr($af->isi, 0, 100);
                        $pecah = explode(' ', $af->create_date);
                        ?>

                        <li class="first">

                            <a href="<?php echo site_url('artikel/' . $af->flag); ?>" title="<?php echo $af->judul; ?>">

                                <?php if ($af->img === '') { ?>

                                    <img src="<?php echo base_url(); ?>tmp/frontend/img/300x250.jpg" alt="..." class="col-thumb" height="80" width="80">   

                                <?php } else {
                                    ?>

                                    <img src="<?php echo base_url(); ?>image/berita/<?php echo $af->img; ?>" class="col-thumb" height="80" width="80">

                                <?php } ?>
                            </a>
                            <h2 class="col-title">
                                <a href="<?php echo site_url('artikel/' . $af->flag); ?>" title="<?php echo $af->judul; ?>"><?php echo $af->judul; ?> </a>

                            </h2>

                            <div class="col-meta"><?php echo nama_hari($pecah['0']).' '.tgl_indo($pecah['0']); ?> | <a href="<?php echo site_url('artikel/' . $af->flag); ?>"><?php echo jumlah_komentar($af->id_berita) . ' Comments' ?></a></div>

                            <div style="padding: 5px; text-align: justify;"><?php echo $isi; ?>… <a class="col-readfull" href="<?php echo site_url('artikel/' . $af->flag); ?>">Baca Selengkapnya <span class="meta-nav">»</span></a></div>

                        </li>

                        <?php
                    }
                } else {



                    echo 'Belum ada berita dikategori ini';
                }
                if ($id_art != '' or $id_art != NULL) {
                    $berita_second = get_front($id_kat, $id_art);

                    if (is_array($berita_second)) {

                        foreach ($berita_second as $bin) {

                            $judul = substr($bin->judul, 0, 50);
                            ?>

                            <li class="col-list"><a href="<?php echo site_url('artikel/' . $bin->flag); ?>" title="<?php echo $bin->judul; ?>"><?php echo $judul; ?>...</a></li>

                            <?php
                        }
                    }
                } else {
                    echo 'Belum ada berita di kategori ini';
                }
                ?>
            </ul>

        </div>

    </div>
</div>


    <div class="col-lg-6">

    <div class="panel panel-default">

        <div class="panel-heading"><b>KOTA GORONTALO</b> </div>

        <div class="panel-body panel-widget">

            <ul class="sb-two">
                <?php
                $id_kat = get_kat('Kota Gorontalo');

                $berita_first = get_front($id_kat);
                $id_art = '';

                if (is_array($berita_first)) {

                    foreach ($berita_first as $af) {

                        $id_art = $af->id_berita;

                        $isi = substr($af->isi, 0, 100);
                        $pecah1 = explode(' ', $af->create_date);
                        ?>

                        <li class="first">

                            <a href="<?php echo site_url('artikel/' . $af->flag); ?>" title="<?php echo $af->judul; ?>">

                                <?php if ($af->img === '') { ?>

                                    <img src="<?php echo base_url(); ?>tmp/frontend/img/300x250.jpg" alt="..." class="col-thumb" height="80" width="80">   

                                <?php } else {
                                    ?>

                                    <img src="<?php echo base_url(); ?>image/berita/<?php echo $af->img; ?>" class="col-thumb" height="80" width="80">

                                <?php } ?>
                            </a>
                            <h2 class="col-title">
                                <a href="<?php echo site_url('artikel/' . $af->flag); ?>" title="<?php echo $af->judul; ?>"><?php echo $af->judul; ?> </a>

                            </h2>

                            <div class="col-meta"><?php echo nama_hari($pecah1['0']).' '.tgl_indo($pecah1['0']); ?> | <a href="<?php echo site_url('artikel/' . $af->flag); ?>"><?php echo jumlah_komentar($af->id_berita) . ' Comments' ?></a></div>

                            <div style="padding: 5px; text-align: justify;"><?php echo $isi; ?>… <a class="col-readfull" href="<?php echo site_url('artikel/' . $af->flag); ?>">Baca Selengkapnya <span class="meta-nav">»</span></a></div>

                        </li>

                        <?php
                    }
                } else {



                    echo 'Belum ada berita dikategori ini';
                }
                if ($id_art != '' or $id_art != NULL) {
                    $berita_second = get_front($id_kat, $id_art);

                    if (is_array($berita_second)) {

                        foreach ($berita_second as $bin) {

                            $judul = substr($bin->judul, 0, 50);
                            ?>

                            <li class="col-list"><a href="<?php echo site_url('berita/' . $bin->flag); ?>" title="<?php echo $bin->judul; ?>"><?php echo $judul; ?>...</a></li>

                            <?php
                        }
                    }
                } else {
                    echo 'Belum ada berita di kategori ini';
                }
                ?>
            </ul>

        </div>

    </div>
    </div>
