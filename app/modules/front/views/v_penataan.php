<div class="content">
    <p>Dokumen Perencanaan Kota </p>
</div>
<div class="isi">
    <?php 
    foreach ($penataan as $row) {
        ?>
        <div class="media">
            <h4 class="media-heading">
                <div class="pull-left">
                    <b><?php echo $row->penataan_ruang; ?></b><br/>
                    <small><?php echo $row->deskripsi; ?></small>
                </div>
                <div class="pull-right">
                    <a href="<?php echo site_url('uploads/'.$row->file) ?>" title="<?php echo $row->file; ?>"><small><span class="glyphicon glyphicon-download"></span> Download File</small></a>
                </div>
            </h4> 
        </div>
    <hr/>
    <?php } ?>
    <?php
    ?>
</div>