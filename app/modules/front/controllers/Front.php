<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front extends MX_Controller {

    private $rule = array(
        array(
            'field' => 'nl',
            'label' => 'Nama Lengkap',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean|valid_email'
        ),
        array(
            'field' => 'website',
            'label' => 'Website',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'komentar',
            'label' => 'Komentar',
            'rules' => 'trim|xss_clean|required'
        )
    );

    public function __construct() {
        parent::__construct();
        $this->load->model('kategori/m_kategori');
        $this->load->model('aplikasi/m_aplikasi');
        $this->load->model('jejakpendapat/m_jejakpendapat');
        $this->load->model('linkterkait/m_link');
        $this->load->model('iklan/m_iklan');
        $this->load->model('komentar/m_komentar');
        $this->load->model('galery/m_galery');
        $this->load->model('profil/m_profil');
        $this->load->model('profilpejabat/m_profilpejabat');
        $this->load->model('berita/m_berita');
        $this->load->model('runningtext/m_runtext');
        $this->load->model('agenda/m_agenda');
        $this->load->model('profile/m_profile');
        $this->load->model('bidang/m_bidang');
        
    }

    public function index() {   
        // $config['base_url'] = base_url('');
        $config['base_url'] = site_url('front/index');
        $config['total_rows'] = $this->m_berita->total_artikel();
        $config['per_page'] = 5;
        $config["uri_segment"] = 2;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data['hal']=$this->pagination->create_links();
        $berita = $this->m_berita->get_limit($config['per_page'], $page);
        $data['active_id'] = $this->m_berita->get_max_id();
        $data['slider'] = $this->m_berita->get_slider();
        $data['berita'] = $berita;
        $data['module'] = 'front';
        $data['view_file'] = 'v_front';
        echo Modules::run('template/render_main', $data);
    }

    public function search(){
        if ($this->input->post()){
            $cari = $this->input->post('cari');
            $data['result'] = query_sql("select * from v_berita where judul like '%$cari%'");
            $data['module'] = 'front';
            $data['view_file'] = 'v_search';
            echo Modules::run('template/render_main', $data);
        }else{
            redirect(site_url(),'refresh');
        }
    }

    public function detail_berita($flag){
        if ($cek = $this->m_berita->get_by(array('flag'=>$flag))){
            $dibaca = $cek->dibaca + 1;
            $this->m_berita->update($cek->id_berita, array('dibaca'=>$dibaca));
            $penulis = $this->m_user->get_by(array('id_user'=>$cek->penulis));
            $data['penulis'] = $penulis->display_name;
            $tanggal = explode(' ',$cek->create_date);
            $data['tgl'] = tgl_indo($tanggal['0']);
            $data['detail'] = $cek;
            $data['module'] = 'front';
            $data['view_file'] = 'v_detail_berita';
            echo Modules::run('template/render_main', $data);
        }
    }

    public function agenda($flag=NULL) {
            $config['base_url'] = base_url('agenda');
            $config['total_rows'] = count($this->m_agenda->get_all());
            $config['per_page'] = 10;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_agenda->get_apa($config['per_page'], $page);
            //$data['jml_agenda'] = count($art);
            $data['berita'] = $art;
            $data['module'] = 'front';
            $data['view_file'] = 'v_agenda';
            echo Modules::run('template/render_main', $data);
    }

    public function detail_agenda($flag){
          if ($flag != NULL){
            $cek = $this->m_agenda->get_by(array('flag'=>$flag));
            if (!empty($cek)){
                $data['detail'] = $cek;
                $data['module'] = 'front';
                $data['view_file'] = 'detail_agenda';
                echo Modules::run('template/render_main', $data);
            }else{
                redirect(site_url(),'refresh');
            }

        }
    }

    public function produk_hukum($kat = NULL){
        if ($kat != NULL){
            // $kategori = explode('.',strtoupper($kat));
            if ($cek = $this->m_produkhukum->get_many_by(array('kategori'=>$kat))){
                $data['produk_hukum'] = $cek; 
                $data['module'] = 'front';
                $data['view_file'] = 'v_produk';
                echo Modules::run('template/render_main', $data);
            }else{
                redirect(site_url(),'refresh');
            }
        }else{
            redirect(site_url(),'refresh');
        }
    }

    public function dokumen_bappeda($kat = NULL){
        if ($kat != NULL){
            $kategori = str_replace('-',' ',$kat);
            if ($cek = $this->m_dokumenbappeda->get_many_by(array('kategori'=>$kategori))) {
                $data['dokumen_bappeda'] = $cek;
                $data['module'] = 'front';
                $data['view_file'] = 'v_dokumen';
                echo Modules::run('template/render_main',$data);
            }else{
                redirect(site_url(),'refresh');
            }
        }else{
            redirect(site_url(),'refresh');
        }
    }

    public function penataan_ruang($kat = NULL){
        if ($kat != NULL){
            $kategori = str_replace('-',' ',$kat);
            if ($cek = $this->m_penataanruang->get_many_by(array('kategori'=>$kategori))){
                $data['penataan'] = $cek;
                $data['module'] = 'front';
                $data['view_file'] = 'v_penataan';
                echo Modules::run('template/render_main', $data);
            }else{
                redirect(site_url(),'refresh');
            }
        }else{
            redirect(site_url(),'refresh');
        }
    }

    public function dokumen_perencanaan($flag){
    
        if ($cek = $this->m_perencanaakota->get_many_by(array('kategori'=>$flag))) {
            $data['perencanaan_kota'] = $cek;
            $data['module'] = 'front';
            $data['view_file'] = 'v_perencanaan';
            echo Modules::run('template/render_main',$data);
            
        }
    }

    public function dokumen_perencanaan1($kat = NULL){
        if ($kat != NULL) {
            if ($cek = $this->m_perencanaakota->get_many_by(array('kategori'=>$kat))) {
                $data['perencanaan_kota'] = $cek;
                $data['module'] = 'front';
                $data['view_file'] = 'v_perencanaan';
                echo Modules::run('template/render_main',$data);
            }
        }
    }

    public function get_dokumen($kat , $sub = NULL){
        if ($sub != NULL) {
            $cek = query_sql("select * from v_dokumen where kategori= '$kat' and sub_kategori = '$sub'");
            if (!empty($cek)){
                $data['dokumen'] = $cek;
            }
        }else{
            $kat = str_replace('-', ' ', $kat);
            $cek1 = query_sql("select * from v_dokumen where kategori= '$kat' and sub_kategori = '$kat'");
            if (!empty($cek1)){
                $data['dokumen'] = $cek1;
            }
            
        }
        $data['module'] = "front";
        $data['view_file'] = 'v_dokumen';
        echo Modules::run('template/render_main',$data);
    }


    public function berita($kat , $flag = NULL){
    
        if ($cekdb = $this->m_kategori->get_by(array('flag'=>$kat))){
            
                $config['base_url'] = base_url('kategori/'.$kat.'/');
                $config['total_rows'] = count($this->m_berita->get_many_by(array('kategori'=>$cekdb->id_kategori)));
                $config['per_page'] = 11;
                $config["uri_segment"] = 3;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['first_link'] = false;
                $config['last_link'] = false;
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['prev_link'] = '&laquo';
                $config['prev_tag_open'] = '<li class="prev">';
                $config['prev_tag_close'] = '</li>';
                $config['next_link'] = '&raquo';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="active"><a href="#">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                
                $cek = $this->m_berita->get_apa(array('kategori'=>$cekdb->id_kategori), $config['per_page'], $page); 

                    $data['berita'] = $cek;
                    $data['module'] = 'front';
                    $data['view_file'] = 'v_berita';
                    echo Modules::run('template/render_main',$data);
                
            
        }
        
    }

    public function komentar(){
        $idincrement = auto_inc('m_komentar','id_komentar');
        if ($this->input->post()) {
            $data = array(
              'id_komentar'=> $idincrement,
              'id_berita' => $this->input->post('id_berita', TRUE),
              'nama' => $this->input->post('nl', TRUE),
              'email' => $this->input->post('email', TRUE),
              'website' => $this->input->post('website', TRUE),
              'komentar' => $this->input->post('komentar', TRUE),
              'status'=>'0',
              'create_date' => date('Y-m-d H:i:s') 
            );
            if (!$this->m_komentar->insert($data)) {
              redirect(site_url('artikel/' . $this->input->post('flag')));
            }else{
              echo "Gagal Mengirim Komentar";
            }
        }else {
          echo "Gagal Komentar";
        }
    }

    public function not_found(){
        redirect(site_url(),'refresh');
    }

    public function profil($profil)
    {
        $kat = str_replace('-',' dan ',$profil);
        $cek = $this->m_profil->get_by(array('kategori'=>$kat));
        if(!empty($cek)){
            $data['list'] =$cek;
            $data['module']="front";
            $data['view_file']="v_struktur";
            echo Modules::run('template/render_main',$data);
        }else{
            redirect(site_url(),'refresh');
        }
    }
    public function profilpejabat()
    {
      $data['list']=$this->m_profilpejabat->get_all();
      $data['list_bidang'] = $this->m_bidang->get_all('asc');
      $data['module']='front';
      $data['view_file']='v_profilpejabat';
      echo Modules::run('template/render_main',$data);
    }
    public function profilpejabat_detail($bidang)
    {
        $cek = $this->m_profilpejabat->get_many_by(array('devisi'=>$bidang),'asc');
        if (!empty($cek)) {
            $data['profil'] = $cek;
            $data['module'] = 'front';
            $data['view_file'] = 'v_profilpejabat_detail';
            echo Modules::run('template/render_main',$data);
        }else{
            redirect(site_url(),'refresh');
        }
    }

    public function galery($kat)
    {
        $cek = $this->m_galery->get_many_by(array('kategori'=>$kat));
        if (!empty($cek)) {
            $data['list']=$cek;
            $data['album']=list_album();
            $data['module']="front";
            $data['view_file']='v_galery';
            echo Modules::run('template/render_main',$data);
        }else{
            redirect(site_url(),'refresh');
        }
    }
    public function data_jejakpedapat()
    {
      $all =$this->m_jejakpendapat->get_all();
      $data ['count']=$this->m_jejakpendapat->get_stat($all);
      $data['data1'] = $this->m_jejakpendapat->count1();
      $data['data2'] = $this->m_jejakpendapat->count2();
      $data['data3'] = $this->m_jejakpendapat->count3();
      $data['data4'] = $this->m_jejakpendapat->count4();
      $data['data5'] = $this->m_jejakpendapat->count5();
      $data['data6'] = $this->m_jejakpendapat->count6();
      $data['module']="front";
      $data['view_file']="v_data_pendapat";
      echo Modules::run('template/render_main',$data);
    }

}

