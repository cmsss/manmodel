<?php 

	if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	class Kemenag extends MX_Controller
	{

		private $module;


		public function __construct()
		{
			parent::__construct();
			// $this->load->model('berita/m_berita');
			// $this->load->model('profil/m_profil');
			// $this->load->model('artikel/m_artikel');
			// $this->load->model('profilpejabat/m_profilpejabat');
			// $this->load->model('bidang/m_bidang');
			$this->module = 'kemenag';
			$this->load->model('m_berita1');
			$this->load->model('m_opini');
			$this->load->model('m_artikel1');
			$this->load->model('m_foto');
			$this->load->model('m_video');
			$this->load->model('m_audio');
			$this->load->model('m_renungan');
			$this->load->model('m_majalah');
			$this->load->model('m_banner');
                  $this->load->model('m_kategori1');

		}

		public function  data_berita($flag = NULL)
		{
			$config['base_url'] = site_url('berita-gorontalo/');
            $config['total_rows'] = count($this->m_berita1->get_all_v_k());
            $config['per_page'] = 5;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_berita1->get_page($config['per_page'], $page);
            $data['page'] = $this->pagination->create_links();
            //$data['jml_agenda'] = count($art);
            $data['list'] = $art;
			// $data['list']		= 	$this->m_berita1->get_many_by_v( array('a.setuju' => '1'), 10);
			$data['view']		=	'v_data_berita';
			$data['module']		= 	'kemenag';
			echo Modules::run('template/frontend' , $data);
		}


		public function data_majalah($flag = NULL)
		{
			$config['base_url'] = site_url('majalah-gorontalo/');
            $config['total_rows'] = count($this->m_majalah->get_all_v_k());
            $config['per_page'] = 12;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_majalah->get_page($config['per_page'], $page);
            $data['page'] = $this->pagination->create_links();
            //$data['jml_agenda'] = count($art);
            $data['list'] = $art;
			// $data['list']		= 	$this->m_majalah1->get_many_by_v( array('a.setuju' => '1'), 10);
			$data['view']		=	'v_data_majalah';
			$data['module']		= 	'kemenag';
			echo Modules::run('template/frontend' , $data);
		}


		public function data_banner($flag = NULL)
		{
			$config['base_url'] = site_url('banner-gorontalo/');
            $config['total_rows'] = count($this->m_banner->get_all_v_k());
            $config['per_page'] = 8;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_banner->get_page($config['per_page'], $page);
            $data['page'] = $this->pagination->create_links();
            //$data['jml_agenda'] = count($art);
            $data['list'] = $art;
			// $data['list']		= 	$this->m_banner1->get_many_by_v( array('a.setuju' => '1'), 10);
			$data['view']		=	'v_data_banner';
			$data['module']		= 	'kemenag';
			echo Modules::run('template/frontend' , $data);
		}


		public function  data_renungan($flag = NULL)
		{
			$config['base_url'] = site_url('renungna-gorontalo/');
            $config['total_rows'] = count($this->m_renungan->get_all_v_k());
            $config['per_page'] = 10;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_renungan->get_page($config['per_page'], $page);
            $data['page'] = $this->pagination->create_links();
            //$data['jml_agenda'] = count($art);
            $data['list'] = $art;
			// $data['list']		= 	$this->m_renungan1->get_many_by_v( array('a.setuju' => '1'), 10);
			$data['view']		=	'v_data_renungan';
			$data['module']		= 	'kemenag';
			echo Modules::run('template/frontend' , $data);
		}


		public function data_opini($flag = NULL)
		{
			$config['base_url'] = site_url('opini-gorontalo/');
            $config['total_rows'] = count($this->m_opini->get_all_v_k());
            $config['per_page'] = 5;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_opini->get_page($config['per_page'], $page);
            $data['page'] = $this->pagination->create_links();
            //$data['jml_agenda'] = count($art);
            $data['list'] = $art;
			// $data['list']		= 	$this->m_berita1->get_many_by_v( array('a.setuju' => '1'), 10);
			$data['view']		=	'v_data_opini';
			$data['module']		= 	'kemenag';
			echo Modules::run('template/frontend' , $data);
		}


		public function data_artikel($flag = NULL){
			$config['base_url'] = site_url('artikel-gorontalo/');
                  $config['total_rows'] = count($this->m_artikel1->get_all_v_k());
                  $config['per_page'] = 10;
                  $config["uri_segment"] = 2;
                  $config['full_tag_open'] = '<ul class="pagination">';
                  $config['full_tag_close'] = '</ul>';
                  $config['first_link'] = false;
                  $config['last_link'] = false;
                  $config['first_tag_open'] = '<li>';
                  $config['first_tag_close'] = '</li>';
                  $config['prev_link'] = '&laquo';
                  $config['prev_tag_open'] = '<li class="prev">';
                  $config['prev_tag_close'] = '</li>';
                  $config['next_link'] = '&raquo';
                  $config['next_tag_open'] = '<li>';
                  $config['next_tag_close'] = '</li>';
                  $config['last_tag_open'] = '<li>';
                  $config['last_tag_close'] = '</li>';
                  $config['cur_tag_open'] = '<li class="active"><a href="#">';
                  $config['cur_tag_close'] = '</a></li>';
                  $config['num_tag_open'] = '<li>';
                  $config['num_tag_close'] = '</li>';
                  $this->pagination->initialize($config);
                  $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
                  $art = $this->m_artikel1->get_page($config['per_page'], $page);
                  $data['page'] = $this->pagination->create_links();
                  //$data['jml_agenda'] = count($art);
                  $data['list_artikels'] = $art;
			// $data['list']		= 	$this->m_berita1->get_many_by_v( array('a.setuju' => '1'), 10);
			$data['view']		=	'v_data_artikel';
			$data['module']		= 	'kemenag';
			echo Modules::run('template/frontend' , $data);
		}


		public function data_foto($flag = NULL)
		{
			$config['base_url'] = site_url('foto-gorontalo/');
            $config['total_rows'] = count($this->m_foto->get_all_v_k());
            $config['per_page'] = 9;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_foto->get_page($config['per_page'], $page);
            $data['page'] = $this->pagination->create_links();
            //$data['jml_agenda'] = count($art);
            $data['list'] = $art;
			// $data['list'] = $this->m_foto->get_all_v_k();
			$data['view'] = 'v_data_foto';
			$data['module'] = 'kemenag';
			echo Modules::run('template/frontend' , $data);
		}


		public function data_video($flag = NULL)
		{
			$config['base_url'] = site_url('video-gorontalo/');
            $config['total_rows'] = count($this->m_video->get_all_v_k());
            $config['per_page'] = 6;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_video->get_page($config['per_page'], $page);
            $data['page'] = $this->pagination->create_links();
            //$data['jml_agenda'] = count($art);
            $data['list'] = $art;
			$data['view'] = 'v_data_video';
			$data['module'] = 'kemenag';
			echo Modules::run('template/frontend' , $data);
		}


		public function data_audio($flag = NULL){
			$config['base_url'] = site_url('audio-gorontalo/');
            $config['total_rows'] = count($this->m_audio->get_all_v_k());
            $config['per_page'] = 9;
            $config["uri_segment"] = 2;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            $art = $this->m_audio->get_page($config['per_page'], $page);
            $data['page'] = $this->pagination->create_links();
            //$data['jml_agenda'] = count($art);
            $data['list'] = $art;
			$data['view'] = 'v_data_audio';
			$data['module'] = 'kemenag';
			echo Modules::run('template/frontend' , $data);
		}

            public function detail_video($id)
            {
                  $cek = $this->m_video->get_by(array('id' => $id));
                  if ($cek) {
                       
                        $data['list'] =  $this->m_video->get_all_v_l($id , 3);
                        $data['l_video'] = $cek;
                        $data['view'] = 'v_detail_video';
                        $data['module'] = $this->module;
                        echo Modules::run('template/frontend' , $data);
                  }else{
                        redirect('','refresh');
                  }
            }


		public function detail_berita($id , $flag)
		{
			$cek = $this->m_berita1->get_by_v(array('a.id' => $id , 'a.setuju' => '1'));
			if ($cek) {
			   $judul_sterlir = str_replace(array('.' ,',' ,'(' , ')' ,'{' ,'}','?' ,'!' , '=' ,'/','\'','"'), '', $cek->judul);
			    $judul_sterlir2 = str_replace(' ', '-', $judul_sterlir);
			    $judul_fix = strtolower($judul_sterlir2);

			    // if ($flag == $judul_fix) {
      			    	$baca = array(
      			    		'counters' => $cek->counters+1
      			    		);
      			    	$this->m_berita1->update($cek->id , $baca);


      			    	$data['l_berita'] = $cek;
                              $data['list_berita_laninya'] = $this->m_berita1->get_all_v_k_berita_lainya($id , 5);
					$data['view'] = 'v_detail_berita';
					$data['module'] = $this->module;
					echo Modules::run('template/frontend' , $data);
			    // }else{
			    // 	redirect('','refresh');
			    // }

				
			}else{
				redirect('','refresh');
			}	
			
		}


		public function detail_opini($id , $flag)
		{
			$cek = $this->m_opini->get_by_v(array('a.id' => $id , 'a.setuju' => '1'));
			if ($cek) {

				$judulo = $cek->judul;
                $judul_sterliro = str_replace(array('.' ,',' ,'(' , ')' ,'{' ,'}','?' ,'!' , '=' ,'/','\'','"'), '', $judulo);
                $judul_sterliro2 = str_replace(' ', '-', $judul_sterliro);
                $judul_fixo = strtolower($judul_sterliro2);

                // if ($judul_fixo == $flag) {
                	$baca = array(
			    		'counters' => $cek->counters+1
			    		);
			    	$this->m_opini->update($cek->id , $baca);
			    	$data['l_opini'] = $cek;
					$data['view'] = 'v_detail_opini';
					$data['module'] = $this->module;
					echo Modules::run('template/frontend' , $data);
                // }else{
                // 	redirect('','refresh');
                // }
			}else{
				redirect('','refresh');
			}
		}


		public function detail_artikel($id , $flag){
			$cek = $this->m_artikel1->get_by_v(array('a.id' => $id , 'a.setuju' => '1' ));

			if ($cek) {
				$baca = array(
			    		'counters' => $cek->counters+1
			    		);
		    	$this->m_artikel1->update($cek->id , $baca);
		    	$data['l_artikel'] = $cek;
				$data['view'] = 'v_detail_artikel';
				$data['module'] = $this->module;
				echo Modules::run('template/frontend' , $data);
			}
		}


            public function rss(){
                  $berita = $this->m_berita1->get_many_by_v(array('a.setuju' => '1') , 4);
                  $data['berita1'] = $berita;
                  $this->load->view('v_rss' , $data);

            }


            public function kategori_berita(){
                  $d = $this->input->get('data');
                  $q = $this->db->query("SELECT * FROM berita WHERE kategori LIKE '".$d."%' AND hapus = '0' AND tampil = '1' AND setuju = '1' ORDER BY tgl_input DESC LIMIT 0 , 5");
                  $data['berita_kategori'] = $q->result();
                  echo $this->load->view('v_berita_kategori' , $data);
            }


            public function cari_data(){
                  $cari       =     $this->input->post('data');
                  $kategori   =     $this->input->post('kategori');
                  $by         =     $this->input->post('by');
                  $tgl_a      =     $this->input->post('tgl_a');
                  $tgl_b      =     $this->input->post('tgl_b');





                  $data['hasil'] = $cari;

                  if ($kategori == '' && $by == '' && $tgl_a == '') {
                        $data['data_yang_dicari'] = $this->db->query("SELECT * FROM berita  WHERE judul LIKE '%$cari%' AND hapus = '0' ORDER BY ts DESC LIMIT 0 , 10")->result();
						$data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM berita  WHERE judul LIKE '%$cari%' AND hapus = '0' ORDER BY ts DESC ")->row();
                        $data['view'] = 'v_cari_data';
                        
                  }


                  else{

                        if ($tgl_a == '') {
                              $tg_a = '';
                              $data['tgl_a'] = '';
                        }else{
                              $tg_a = $time = strtotime($tgl_a);
                              $data['tgl_a'] = $tgl_a;
                        }

                        if ($tgl_b == '') {
                              $tg_b = '';
                              $data['tgl_b'] = '';
                        }else{
                              $tg_b = $timeb = strtotime($tgl_b.' 12:59:59');
                              $data['tgl_b'] = $tgl_b;
                        }

                        $data['kategori'] = $kategori;
                        $data['by'] = $by;
                        switch ($by) {
                              case 'artikel':

                                    if ($tgl_a != '' AND $tgl_b != '') {
                                          $data['data_yang_dicari'] = $this->db->query("SELECT * FROM artikel WHERE judul LIKE '%$cari%' AND kategori LIKE '$kategori%' AND hapus = '0' AND (ts BETWEEN $tg_a AND $tg_b)  ORDER BY ts DESC LIMIT 0 , 10")->result();
										  $data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM artikel WHERE judul LIKE '%$cari%' AND kategori LIKE '$kategori%' AND hapus = '0' AND (ts BETWEEN $tg_a AND $tg_b)  ORDER BY ts DESC")->row();
                                          $data['view'] = 'v_cari_data_artikel';
                                    }else{
                                          $data['data_yang_dicari'] = $this->db->query("SELECT * FROM artikel WHERE judul LIKE '%$cari%' AND kategori LIKE '$kategori%' AND hapus = '0'  ORDER BY ts DESC LIMIT 0 , 10")->result();
										  $data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM artikel WHERE judul LIKE '%$cari%' AND kategori LIKE '$kategori%' AND hapus = '0'  ORDER BY ts DESC ")->row();
                                          $data['view'] = 'v_cari_data_artikel';
                                    }
                                    
                                    break;
                              case 'foto':
                                    if ($tgl_a != '' AND $tgl_b != '') {
                                          
                                          $data['data_yang_dicari'] = $this->db->query("SELECT * FROM foto WHERE keterangan LIKE '%$cari%' AND kategori LIKE '$kategori%' AND (ts BETWEEN $tg_a AND $tg_b) ORDER BY ts DESC LIMIT 0 , 12")->result();
										  $data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM foto WHERE keterangan LIKE '%$cari%' AND kategori LIKE '$kategori%' AND (ts BETWEEN $tg_a AND $tg_b) ORDER BY ts DESC ")->row();
                                          $data['view'] = 'v_cari_data_foto';
                                    }else{

                                          $data['data_yang_dicari'] = $this->db->query("SELECT * FROM foto WHERE keterangan LIKE '%$cari%' AND kategori LIKE '$kategori%' ORDER BY ts DESC LIMIT 0 , 12")->result();
                                          $data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM foto WHERE keterangan LIKE '%$cari%' AND kategori LIKE '$kategori%' ORDER BY ts DESC")->row();
										  $data['view'] = 'v_cari_data_foto';
                                    }

                                    break;
                              case 'opini':
                                    if ($tgl_a != '' AND $tgl_b != '') {
                                          $data['data_yang_dicari'] = $this->db->query("SELECT * FROM opini WHERE judul LIKE '%$cari%' AND kategori LIKE '$kategori%' AND hapus = '0' AND (ts BETWEEN $tg_a AND $tg_b) ORDER BY ts DESC LIMIT 0 , 10")->result();
                                          $data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM opini WHERE judul LIKE '%$cari%' AND kategori LIKE '$kategori%' AND hapus = '0' AND (ts BETWEEN $tg_a AND $tg_b) ORDER BY ts DESC")->row();
										  $data['view'] = 'v_cari_data_opini';
                                    }else{

                                          $data['data_yang_dicari'] = $this->db->query("SELECT * FROM opini WHERE judul LIKE '%$cari%' AND kategori LIKE '$kategori%' AND hapus = '0' ORDER BY ts DESC LIMIT 0 , 10")->result();
                                          $data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM opini WHERE judul LIKE '%$cari%' AND kategori LIKE '$kategori%' AND hapus = '0' ORDER BY ts DESC ")->row();
                                          $data['view'] = 'v_cari_data_opini';
                                    }
                                    break;
                              
                              default:
                                    if ($tgl_a != '' AND $tgl_b != '') {
                                           $data['data_yang_dicari'] = $this->db->query("SELECT * FROM berita WHERE judul LIKE '%$cari%' AND  kategori LIKE '$kategori%' AND hapus = '0' AND (ts BETWEEN $tg_a AND $tg_b) ORDER BY ts DESC LIMIT 0 , 10")->result();
                                           $data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM berita WHERE judul LIKE '%$cari%' AND  kategori LIKE '$kategori%' AND hapus = '0' AND (ts BETWEEN $tg_a AND $tg_b) ORDER BY ts DESC ")->row();
                                          $data['view'] = 'v_cari_data_berita';
                                    }else{
                                           $data['data_yang_dicari'] = $this->db->query("SELECT * FROM berita WHERE judul LIKE '%$cari%' AND  kategori LIKE '$kategori%' AND hapus = '0'  ORDER BY ts DESC LIMIT 0 , 10")->result();
                                            $data['data_yang_dicari_count'] = $this->db->query("SELECT COUNT(id) as id FROM berita WHERE judul LIKE '%$cari%' AND  kategori LIKE '$kategori%' AND hapus = '0' ORDER BY ts DESC ")->row();
                                          $data['view'] = 'v_cari_data_berita';
                                    }
                                   
                                    break;
                        }
                  }

                  
                  $data['module'] = $this->module;
                  $data['l_kategori'] = drop_list('m_kategori1' , 'id' , 'nama' , 'Kategori' );
                  $semua = array(
                              'berita' => 'Berita' ,
                              'artikel' => 'Artikel',
                              'foto' => 'Foto',
                              'opini' => 'Opini',
                              );
                  $data['semua'] =  $semua;
                  echo Modules::run('template/frontend' , $data);
                  
            }


		// public function detail_artikel($flag)
		// {
		// 	$cek = $this->m_artikel->get_by(array('flag' => $flag , 'status' => '1'));
		// 	if ($cek) {
		// 		$dibaca = $cek->dibaca + 1;
		// 		$this->m_artikel->update($cek->id_artikel, array('dibaca'=>$dibaca));
		// 		$data['e']		= $cek;
		// 		$data['view']	= 'v_detail_artikel';
		// 		$data['module']	= 'Kemenag';
		// 		echo Modules::run('template/frontend' , $data);
		// 	}else {
		// 		redirect('' , 'refresh');
		// 	}
		// }


		// public function frontend($flag)
		// {
		// 	$cek = $this->m_berita->get_by(array('flag' => $flag  , 'status' => '1'));

		// 	if ($cek) {
		// 		$dibaca = $cek->dibaca + 1;
		// 		$this->m_berita->update($cek->id_berita, array('dibaca'=>$dibaca));
		// 		$data_berita = $this->m_berita->get_by_v_berita(array('flag' => $flag));
		// 		$data['e'] 			= 	$data_berita;	 
		// 		$data['view'] 		= 	'v_detai_berita';
		// 		$data['module'] 	=	'kemenag';
		// 		$data['berita_random']		= 	$this->m_berita->get_random_v_berita('5');
		// 		echo Modules::run('template/frontend' , $data); 
		// 	}else {
		// 		redirect('','refresh');
		// 	}
		// }

		// public function data_berita_kemenag()
		// {	
		// 	$berita = $this->m_berita->get_v_berita('30' ,
		// 		array(
		// 			'status' => '1' ,
		// 			));
		// 	$data['berita_cc'] = $berita;
		// 	$data['view']		=	'v_data_berita';
		// 	$data['module']		= 	'kemenag';
		// 	echo Modules::run('template/frontend' , $data);
		// }


		// public function profil_visi_misi()
		// {		

		// 	$profil = 'visi-misi';
		// 	$kat = str_replace('-',' dan ',$profil);
		// 	$cek = $this->m_profil->get_by(array('kategori'=>$kat));
		// 	if(!empty($cek)){
		// 		$data['list'] =$cek;
		// 		$data['module']="kemenag";
		// 		$data['view']="v_visi_misi";
		// 		echo Modules::run('template/frontend', $data);
		// 	}else{
		// 		redirect(site_url(),'refresh');
		// 	}
		// }

		// public function profil_struktur()
		// {
		// 	$profil = 'struktur';
		// 	$kat = str_replace('-',' dan ',$profil);
		// 	$cek = $this->m_profil->get_by(array('kategori'=>$kat));
		// 	if(!empty($cek)){
		// 		$data['list'] =$cek;
		// 		$data['module']="kemenag";
		// 		$data['view']="v_struktur";
		// 		echo Modules::run('template/frontend', $data);
		// 	}else{
		// 		redirect(site_url(),'refresh');
		// 	}
		// }

		// public function profilpejabat()
		// {
		// 	$data['list']=$this->m_profilpejabat->get_all();
		// 	$data['list_bidang'] = $this->m_bidang->get_all('asc');
		// 	$data['module']='kemenag';
		// 	$data['view']='v_profilpejabat';
		// 	echo Modules::run('template/frontend',$data);
		// }

		// public function profilpejabat_detail($bidang)
		// {
		// 	$cek = $this->m_profilpejabat->get_many_by(array('devisi'=>$bidang),'asc');
		// 	if ($cek) {
		// 		$data['profil'] = $cek;
		// 		$data['module'] = 'kemenag';
		// 		$data['view'] = 'v_profilpejabat_detail';
		// 		echo Modules::run('template/frontend',$data);
		// 	}else{
		// 		echo "<script>

		// 		alert('Maaf Pejabat bagian Tersebut Belum Ada');
		// 		window.location=('".site_url('profil-pejabat')."');

		// 		</script>";
		// 	}
		// }


		public function kalkulator_zakat()
		{
			$data['view']	= 'v_kalkulator_zakat';
			$data['module'] 	= 'kemenag';
			echo Modules::run('template/frontend' ,$data);
		}


            public function sitemap(){
                  $data['view']     = 'v_sitemap';
                  $data['module']   = 'kemenag';
                  echo Modules::run('template/frontend'  , $data);
            }



	}


