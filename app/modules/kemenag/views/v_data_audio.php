<div class="marginTop">
    <div class="panel-heading">
        <h3 class="panel-title">Galeri Audio</h3>
    </div>
    <div class="panel-body">
        <div class="cbp-panel">
            <div class="row">
                <div class="cbp cbp-caption-active cbp-caption-overlayBottomReveal cbp-ready">
                    <?php

                        if (!empty($list)) {
                            foreach ($list as $l_audio) {
                                // $tgl = explode(' ', $l_audio->tanggal);
                                ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xl-12 " >
                            		<div class="cbp-item-wrapper">
                                        <a class="cbp-caption cbp-lightbox" data-title="<?= $l_audio->keterangan ?>"
                                           href="<?= base_url('files/gorontalo/file/audio/'.$l_audio->id .'.mp3') ?>">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="<?= base_url('uploads/kemenag/kemenag.jpg') ?>" alt="...">
                                            </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Dengar</div>
                                                        <div class="cbp-l-caption-desc"><?= substr($l_audio->keterangan, 0 , 20) ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="projects-title"><?= substr($l_audio->keterangan ,0 ,40) ?></div>
                                        <div class="cbp-l-grid-projects-desc"><?php //echo nama_hari($tgl['0']).' '.  tgl_indo($tgl['0']) ?></div>
                                    </div>
                                    
                                </div>

                                <?php
                            }
                        }

                    ?>
                    
                </div>
	                

            </div>
            

            <?= $page ?>
                    
                

        </div>
    </div>
</div>
