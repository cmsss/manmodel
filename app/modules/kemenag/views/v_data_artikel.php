<div class="marginTop">
    <div class="panel-heading">
        <h3 class="panel-title">Artikel</h3>
    </div>
    <div class="panel-body">
    	<?php if (!empty($list_artikels)) {
    		foreach ($list_artikels as $l_artikel) {
    			?>
    			<?php 
                    // $tgl = explode(' ', $l_artikel->tgl_input);
                    $judul = $l_artikel->judul;
                    
                    $judul_fix = flag($judul);
                 ?>
    				<div class="col-md-12">
			            <div class="marginBottomMedia">
			                <div class="media">
			                    <a class="pull-left" href="<?= site_url('artikel/' .$l_artikel->id.'/'.$judul_fix) ?>">
			                    <!-- 100x100 -->
			                        <img class="media-object thumbnail img-responsive marginRightMedia" style="width: 200px;" src="<?= base_url('files/file/fotoartikel/' .$l_artikel->foto) ?>" alt="..." onerror="this.onerror=null;this.src='<?= base_url('uploads/kemenag/kemenag.jpg') ?>';">
			                    </a>
			                    <div class="media-body">
			                        <a href="<?= site_url('artikel/' .$l_artikel->id.'/'.$judul_fix) ?>">
                                        <h4 class="media-heading"><?= $l_artikel->judul ?></h4>
                                    </a>

			                        <p><?php // echo substr($l_artikel->isi, 0 , 150) ?> 
			                        <ul class="blog-meta">
			                            <!-- <li>By: <a href="http://djavaui.com/" target="_blank">Djava UI</a></li> -->
			                            <!-- <li><?php// echo nama_hari($tgl['0']).' '.  tgl_indo($tgl['0']) ?></li> -->
			                            <li><a href=""><?= $l_artikel->counters ?>x dibaca</a></li>
			                        </ul>
			                        </p>

			                    </div>
			                </div>
			                
		                      
			            </div>
			        </div>

    			<?php
    		}
    	} ?>
    	<?php echo $page; ?>
        
        
        <!-- <div class="col-md-12">
            <div class="blog-item rounded shadow">
                <div class="media">
                    <a class="pull-left" href="detail.html">
                        <img class="media-object thumbnail" src="http://img.djavaui.com/?create=100x100,6880B0?f=ffffff" alt="...">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">About The Author</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        <ul class="blog-meta">
                            <li>By: <a href="http://djavaui.com/" target="_blank">Djava UI</a></li>
                            <li>Jun 08, 2014</li>
                            <li><a href="">2 Comments</a></li>
                        </ul>
                        </p>

                    </div>
                </div>
                <div class="ribbon-wrapper">
                    <div class="ribbon ribbon-success">New tes</div>
                </div>
                    
            </div>
        </div> -->
    </div>
</div>