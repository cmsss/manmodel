<div class="panel panel-theme shadow  wow zoomIn">
    <div class="panel-heading">
        <h3 class="panel-title">MAJALAH</h3>
    </div>
    <div class="panel-body ">
    <div class="row">
        
    <!-- </div> -->
        <?php 

            if (!empty($list)) {
                foreach ($list as $l_majalah) {
                    ?>

                         <?php 
                            // $tgl = explode(' ', $l_majalah->tgl_input);

                            $judulo = $l_majalah->judul;
                            
                            $judul_fixo = flag($judulo);

                         ?>

                        <div class="col-sm-4">
                            <div  class="cbp cbp-caption-active cbp-caption-revealBottom cbp-ready">
                                <div class="cbp-item art">
                                    <a target="_blank" href="<?= base_url($l_majalah->pdf) ?>" class="cbp-caption">
                                        <div class="cbp-caption-defaultWrap">
                                            <!-- 340x210 -->
                                            <div class=" fileinput-preview fileinput-exists" style=" max-height: 250px; line-height: 10px;">
                                                <img src="<?= base_url($l_majalah->cover) ?>" class="img-responsive"  alt="..." onerror="this.onerror=null;this.src='<?= base_url('uploads/kemenag/kemenag.jpg') ?>';">
                                            </div>
                                        </div>
                                        <div class="cbp-caption-activeWrap">
                                            <div class="cbp-l-caption-alignCenter">
                                                <div class="cbp-l-caption-body">
                                                    <div class="cbp-l-caption-text">Lihat Majalah</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="<?= site_url('opini/'.$l_majalah->id. '/' .$judul_fixo) ?>"  class="blog-title"><?= $l_majalah->judul ?></a>
                                    
                                </div>
                            </div>
                        </div>


                    <?php
                }
            }

         ?>
        
        <!-- <div class="col-sm-6">
            <div  class="cbp cbp-caption-active cbp-caption-revealBottom cbp-ready">
            <div class="cbp-item art">
                <a href="https://wrapbootstrap.com/user/djavaui" target="_blank" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="http://img.djavaui.com/?create=340x210,37BC9B?f=ffffff" alt="...">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-text">VIEW POST</div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="https://wrapbootstrap.com/user/djavaui" target="_blank" class="cbp-l-grid-blog-title">Berlin wall breakthrough</a>
                <div class="cbp-l-grid-blog-date">14 june 2015</div>
                <div class="cbp-l-grid-blog-split">|</div>
                <a href="#" class="cbp-l-grid-blog-comments">7 comments</a>
                <div class="cbp-l-grid-blog-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</div>
            </div>
        </div>
        </div> -->
        <div class="clearfix">
    
        </div>
        
     </div>
     <div class="pull-right">
       <?php echo $page; ?>
    </div>   
    </div>

</div> 