<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>asset/plugin/maskmoney/jquery.maskMoney.js" type="text/javascript"></script>
<div class="desc_calczakat">
	<h4><strong>Zakat Emas dan Perak</strong></h4>
	<blockquote> zakat yang dikenakan atas emas atau perak yang telah mencapai <strong>nisab dan haul</strong>. Zakat emas wajib dikenakan atas kepemilikan emas yang telah mencapai nisab <strong>85 gram emas</strong>. Zakat perak wajib dikenakan atas kepemilikan perak yang telah mencapai nisab <strong>595 gram perak</strong>. Kadar zakat atas emas ataupun perak sebesar <strong>2,5%</strong>.</blockquote><br>
</div>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">

		<tbody>
			<tr>
				<th colspan="2" class="header"><strong>Zakat Emas dan Perak</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Emas</th>
				<th class="value_zakat"><input type="text" id="emas" onkeyup="nilaiEmas(this);" value="" class="input_angka " style="width:100px"  > gram</th>
			</tr>
			<tr>
				<th class="label_zakat">Perak</th>
				<th class="value_zakat"><input type="text" id="perak" onkeyup="nilaiPerak(this);" value="" class="input_angka " style="width:100px"   > gram</th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat">Jumlah zakat emas</th>
				<th class="value_zakat"><input type="text" id="zakat_emas" class="input_angka" value="0" style="width:100px"  disabled="disabled"> gram</th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat">Jumlah zakat perak</th>
				<th class="value_zakat"><input type="text" id="zakat_perak" class="input_angka" value="0" style="width:100px" disabled="disabled"> gram</th>
			</tr>
			<tr>
				<th colspan="2" class="header"><strong>Zakat Emas dan Perak (jika dibayar dengan uang)</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Harga 1 gram emas</th>
				<th class="value_zakat">Rp <input type="text" id="harga_emas" class="input_angka " onkeyup="hargaEmasa(this)"   value=""></th>
			</tr>
			<tr>
				<th class="label_zakat">Harga 1 gram perak</th>
				<th class="value_zakat">Rp <input type="text" id="harga_perak" class="input_angka " onkeyup="hargaPerak(this)"   value=""></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat">Jumlah zakat emas yang dikeluarkan</th>
				<th class="value_zakat">Rp <input type="text" id="zakat_emas_uang" class="input_angka harga" value="0" disabled="disabled"></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat">Jumlah zakat perak yang dikeluarkan</th>
				<th class="value_zakat">Rp <input type="text" id="zakat_perak_uang" class="input_angka harga" value="0" disabled="disabled"></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat">Total zakat emas dan perak yang dikeluarkan</th>
				<th class="value_zakat">Rp <input type="text" id="zakat_total_uang" value="0" class="input_angka harga" disabled="disabled"></th>
			</tr>
			<tr>
				<th colspan="2" class="header" id="keterangan">&nbsp;</th>
			</tr>
		</tbody>
	</table>
</div>

<script>
	function nilaiEmas(data) {
		
		var data = parseFloat(data.value);
		var  harga_emas = parseFloat($("#harga_emas").val());
		if (data >= 85) {

			var bayar = ((data * 2.5)/100);
			$("#zakat_emas").val(bayar);
			if ($("#harga_emas").val() == '') {
				var  harga_emas = 0;
			}

			var harga =  bayar * harga_emas;
			$("#zakat_emas_uang").val(harga);

			// $("#zakat_total_uang").val(parseFloat($("#zakat_emas_uang").val(harga)) + parseFloat($("#zakat_perak_uang").val()));
			// console.log(parseFloat($("#zakat_emas_uang").val(harga)) + parseFloat($("#zakat_perak_uang").val()));

			
			$("#zakat_total_uang").val(jumlahkan(harga,$("#zakat_perak_uang").val())).maskMoney();
		}else{
			console.log(0);
			$("#zakat_emas").val(0);
			$("#zakat_emas_uang").val(0);
			$("#zakat_total_uang").val(jumlahkan(0 , $("#zakat_perak_uang").val()));
		}
	}

	function jumlahkan(a,b){
		return parseFloat(a) + parseFloat(b);
	}


	function nilaiPerak(data){
		
		var data = parseFloat(data.value);
		var  harga_perak = parseFloat($("#harga_perak").val());
		if (data >= 595) {

			var bayar = ((data * 2.5)/100);
			// console.log(bayar);
			$("#zakat_perak").val(bayar);
			if ($("#harga_perak").val() == '') {
				var  harga_perak = 0;
			}



			var harga = bayar * harga_perak;
			$("#zakat_perak_uang").val(harga);

			// $("#zakat_total_uang").val(parseFloat($("#zakat_perak_uang").val(harga)) + parseFloat($("#zakat_emas_uang").val()));
			$("#zakat_total_uang").val(jumlahkan(harga,$("#zakat_emas_uang").val())).maskMoney();

		}else{
			$("#zakat_perak").val(0);
			$("#zakat_perak_uang").val(0);
			$("#zakat_total_uang").val(jumlahkan(0 , $("#zakat_emas_uang").val()));
		}
	}

	function hargaEmasa(data){
		var data = parseFloat(data.value);
		var zEmas = parseFloat($("#zakat_emas").val());
		var gEmas = parseFloat($("#perak").val());
		if ( gEmas >= 85 ) {
			if ($('#harga_emas').val() == '') {
				var data = 0;
			}
			$("#zakat_emas_uang").val(zEmas * data);
			$("#zakat_total_uang").val(jumlahkan((zEmas * data) , $("#zakat_perak_uang").val()));
		}else{
			$("#zakat_emas_uang").val(0);
			$("#zakat_total_uang").val(jumlahkan(0 , $("#zakat_perak_uang").val())).trigger('mask.maskMoney');
		}
	}

	function hargaPerak(data){
		var data = parseFloat(data.value);
		var zPerak = parseFloat($("#zakat_perak").val());
		var gPerak = parseFloat($("#perak").val());
		if ( gPerak >= 595 ) {
			if ($('#harga_perak').val() == '') {
				var data = 0;
			}
			$("#zakat_perak_uang").val(zPerak * data);
			$("#zakat_total_uang").val(jumlahkan((zPerak * data) , $("#zakat_emas_uang").val())).trigger('mask.maskMoney');
		}else{
			$("#zakat_perak_uang").val(0);
			$("#zakat_total_uang").val(jumlahkan(0 , $("#zakat_emas_uang").val()));
		}
	}
</script>

<script>
	$(function() {
		$(".harga").maskMoney({ allowNegative: true, thousands:',', affixesStay: false});
	})
</script>