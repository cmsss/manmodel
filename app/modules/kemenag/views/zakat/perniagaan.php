<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>asset/plugin/maskmoney/jquery.maskMoney.js" type="text/javascript"></script>
<div class="desc_calczakat">
	<h4><strong>Zakat Perniagaan</strong></h4>
	<blockquote> zakat yang dikenakan atas usaha perniagaan yang telah mencapai <strong>nisab dan haul</strong>. Nisab zakat perniagaan senilai dengan <strong>85 gram emas</strong>. Kadar zakat perniagaan sebesar 2,5%.</blockquote><br>
</div>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<tbody>
			<tr>
				<th colspan="2" class="header"><strong>Nishab</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Harga 1 gram emas</th>
				<th class="value_zakat">Rp <input type="text" id="harga_emas_uang1" class="input_angka" onkeyup="hargaEmas1(this);" value="0"></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Nishab</strong></th>
				<th class="value_zakat">Rp <input type="text" id="nisab_emas_uang1" class="input_angka" disabled="disabled" value="0"> <input type="hidden" id="nisab_emas_uang_float" value="0"></th>
			</tr>

			<tr>
				<th colspan="2" class="header"><strong>Harta</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Uang (Cash, Tabungan, dkk)</th>
				<th class="value_zakat">Rp <input type="text" id="uang_tabungan1" onkeyup="uangTabungan1(this);" class="input_angka"  ></th>
			</tr>
			<tr>
				<th class="label_zakat">Stok Barang Dagangan</th>
				<th class="value_zakat">Rp <input type="text" id="saham1" onkeyup="Saham1(this);" class="input_angka"  ></th>
			</tr>
			<tr>
				<th class="label_zakat">Piutang</th>
				<th class="value_zakat">Rp <input type="text" id="piutang1" onkeyup="Piutang1(this);" class="input_angka"  ></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Total Harta Kena Zakat</strong></th>
				<th class="value_zakat">Rp <input type="text" id="total_harta1" class="input_angka" value="0" disabled="disabled"> <input type="hidden" id="total_harta_float"></th>
			</tr>

			<tr>
				<th colspan="2" class="header"><strong>Kewajiban</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Hutang</th>
				<th class="value_zakat">Rp <input type="text" id="hutang1" class="input_angka"  onkeyup="Hutang1(this);"></th>
			</tr>
			<tr>
				<th class="label_zakat">Biaya Lain</th>
				<th class="value_zakat">Rp <input type="text" id="biaya_lain" class="input_angka"  onkeyup="biayaLain(this);"></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Total Kewajiban</strong></th>
				<th class="value_zakat">Rp <input type="text" id="total_kewajiban1" class="input_angka" value="0" disabled="disabled"> <input type="hidden" id="total_kewajiban_float"></th>
			</tr>

			<tr>
				<th colspan="2" class="header"><strong>Zakat</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Selisih harta dan kewajiban</th>
				<th class="value_zakat">Rp <input type="text" id="selisih_harta1" class="input_angka" value="0" disabled="disabled"></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Zakat yang dikeluarkan</strong></th>
				<th class="value_zakat">Rp <input type="text" id="zakat_harta1" class="input_angka" value="0" disabled="disabled"></th>
			</tr>

			<tr>
				<th colspan="2" class="header" id="ket_mal">&nbsp;</th>
			</tr>
		</tbody>
	</table>
</div>

<script>
	function hargaEmas1(data){
		var data = data.value;
		if (data ==  '') {
			$("#nisab_emas_uang1").val(0 * 85 );
		}else{
			$("#nisab_emas_uang1").val(parseFloat(data) * 85 );
		}

	}

	function tambah(a , b , c , d ){
		var a = parseFloat(a);
		var b = parseFloat(b);
		var c = parseFloat(c);

		if (a >= 1) {
			var aa = a;
		}else{
			var aa = 0;
		}

		if (b >= 1) {
			var bb = b;
		}else{
			var bb = 0;
		}

		if (c >= 1) {
			var cc = c;
		}else{
			var cc = 0;
		}

		return aa + bb + cc;
	}

	function kurang(a, b){
		var a = parseFloat(a);
		var b = parseFloat(b);

		if (a >= 1) {
			var aa = a;
		}else{
			var aa = 0;
		}

		if (b >= 1) {
			var bb = b;
		}else{
			var bb = 0;
		}

		return aa - bb;
	}

	function uangTabungan1(data){
		var data 	= data.value;
		var saham 	= parseFloat($("#saham1").val());
		var piutang = parseFloat($("#piutang1").val());
		var total_harta = tambah(data , saham ,piutang);
		
		$("#total_harta1").val(total_harta);

		$("#selisih_harta1").val(kurang(total_harta , $("#total_kewajiban1").val()));
		if ($("#selisih_harta1").val() > 0) {
			$("#zakat_harta1").val(($("#selisih_harta1").val() * 2.5) / 100);
		}else{
			$("#zakat_harta1").val(0);
		}
		
		

	}

	function Saham1(data){
		var data 			= data.value;
		var uang_tabungan 	= parseFloat($("#uang_tabungan1").val());
		var piutang 		= parseFloat($("#piutang1").val());
		var total_harta 	= tambah(data , uang_tabungan ,piutang);

		$("#total_harta1").val(total_harta);
		$("#selisih_harta1").val(kurang(total_harta , $("#total_kewajiban1").val()));
		if ($("#selisih_harta1").val() > 0) {
			$("#zakat_harta1").val(($("#selisih_harta1").val() * 2.5) / 100);
		}else{
			$("#zakat_harta1").val(0);
		}
		
		
	}

	function Piutang1(data){
		var data 			= data.value;
		var uang_tabungan 	= parseFloat($("#uang_tabungan1").val());
		var saham 		= parseFloat($("#saham1").val());
		var total_harta 	= tambah(data , uang_tabungan ,saham);
		
		$("#total_harta1").val(total_harta);
		$("#selisih_harta1").val(kurang(total_harta , $("#total_kewajiban1").val()));
		if ($("#selisih_harta1").val() > 0) {
			$("#zakat_harta1").val(($("#selisih_harta1").val() * 2.5) / 100);
		}else{
			$("#zakat_harta1").val(0);
		}
		
		
	}

	function tambahHutang(a,b){
		var a = parseFloat(a);
		var b = parseFloat(b);

		if (a >=1 ) {
			var aa = a;
		}else{
			var aa = 0;
		}

		if (b >=1 ) {
			var bb = b;
		}else{
			var bb = 0;
		}

		return aa + bb;
	}

	function Hutang1(data){
		var data = parseFloat(data.value);
		if (data >= 1) {
			var total_kewajiban = $("#total_kewajiban1").val(tambahHutang(data , $("#biaya_lain").val())) ;
			$("#selisih_harta1").val(parseFloat($("#total_harta1").val()) - tambahHutang(data , $("#biaya_lain").val()));
			if (parseFloat($("#selisih_harta1").val()) > 0) {
				$("#zakat_harta1").val((parseFloat($("#selisih_harta1").val()) * 2.5) / 100);
			}else{
				$("#zakat_harta1").val(0);
			}
			

		}else{
			var totol_kewajiban = $("#total_kewajiban1").val(tambahHutang( 0 ,  $("#biaya_lain").val())) ;
			$("#selisih_harta1").val(parseFloat($("#total_harta1").val()) - tambahHutang( 0 ,  $("#biaya_lain").val()));
			$("#zakat_harta1").val(($("#selisih_harta1").val() * 2.5)/100);
		}

		// console.log(totol_kewajiban);
		// $("#selisih_harta1").val(parseFloat($("#total_harta1").val()) - totol_kewajiban);
		
	}

	function biayaLain(data){
		var data = parseFloat(data.value);
		if (data >= 1) {
			var total_kewajiban = $("#total_kewajiban1").val(tambahHutang(data , $("#hutang1").val())) ;
			$("#selisih_harta1").val(parseFloat($("#total_harta1").val()) - tambahHutang(data , $("#hutang1").val()));
			if (parseFloat($("#selisih_harta1").val()) > 0) {
				$("#zakat_harta1").val((parseFloat($("#selisih_harta1").val()) * 2.5) / 100);
			}else{
				$("#zakat_harta1").val(0);
			}
			

		}else{
			var totol_kewajiban = $("#total_kewajiban1").val(tambahHutang( 0 ,  $("#hutang1").val())) ;
			$("#selisih_harta1").val(parseFloat($("#total_harta1").val()) - tambahHutang( 0 ,  $("#hutang1").val()));
			$("#zakat_harta1").val(($("#selisih_harta1").val() * 2.5)/100);
		}
	}


</script>

<script>
	$(function() {
		$(".harga").maskMoney({ allowNegative: true, thousands:',', affixesStay: false});
	})
</script>