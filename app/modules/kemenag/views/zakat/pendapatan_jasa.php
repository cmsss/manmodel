<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>asset/plugin/maskmoney/jquery.maskMoney.js" type="text/javascript"></script>
<div class="desc_calczakat">
	<h4><strong>zakat yang dikeluarkan dari penghasilan yang diperoleh dari hasil profesi pada saat menerima pembayaran. Nisab zakat pendapatan senilai <strong>653 kg gabah atau 524 kg beras</strong>. Kadar zakat pendapatan dan jasa senilai <strong>2,5%</strong>.</blockquote><br>
	</div>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<tbody><tr>
				<th colspan="2" class="header"><strong>Nishab</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Harga 1 kg beras</th>
				<th class="value_zakat">Rp<input type="text" id="harga_beras" class="input_angka hargaa" onkeyup="hargaBeraas(this);" value=""></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Nishab</strong></th>
				<th class="value_zakat">Rp <input type="text" id="nisab_beras" class="input_angka harga" disabled="disabled" value=""> <input type="hidden" id="nisab_beras_float" value=""></th>
			</tr>

			<tr>
				<th colspan="2" class="header"><strong>Pendapatan dan Jasa</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Pendapatan</th>
				<th class="value_zakat">Rp <input type="text" id="pendapatan" onkeyup="Pendapatan(this);" class="input_angka " ></th>
			</tr>
			<tr>
				<th class="label_zakat">Jasa</th>
				<th class="value_zakat">Rp <input type="text" id="jasa" onkeyup="Jasa(this);" class="input_angka" ></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Total Pendapatan dan Jasa</strong></th>
				<th class="value_zakat">Rp <input type="text" id="total_pendapatan_jasa" class="input_angka " disabled="disabled"> <input type="hidden" id="total_pendapatan_jasa_float" value="12"></th>
			</tr>

			<tr>
				<th colspan="2" class="header"><strong>Zakat</strong></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Zakat yang dikeluarkan</strong></th>
				<th class="value_zakat">Rp <input type="text" id="zakat_pendapatan_jasa" class="input_angka hargaa" disabled="disabled"></th>
			</tr>

			<tr>
				<th colspan="2" class="header" id="ket_pendapatan_jasa">Pendapatan dan Jasa BELUM mencapai nishab. Tidak dikenai kewajiban zakat.</th>
			</tr>
		</tbody>
	</table>
</div>

<script>
	function hargaBeraas(data){
		// alert('tes');
		var data = parseFloat(data.value);
		if (data >= 1) {
			$("#nisab_beras").maskMoney().val(data * 524).trigger('mask.maskMoney');
		}else{
			$("#nisab_beras").maskMoney().val(0);
		}
	}
	function jumlahPendapatan(a , b){
		var a = parseFloat(a);
		var b = parseFloat(b);

		if (a >= 1) {
			aa = a;
		}else{
			aa = 0;
		}

		if (b >= 1) {
			bb = b;
		}else{
			bb = 0;
		}

		return aa + bb;
	}

	function Pendapatan(data){
		var data = data.value;
		var jasa = $("#jasa").val();
		// console.log(jasa);
		$("#total_pendapatan_jasa").val(jumlahPendapatan(data , jasa));
		$("#zakat_pendapatan_jasa").val((jumlahPendapatan(data , jasa) * 2.5) / 100).trigger('mask.maskMoney');

	}

	function Jasa(data){
		var data = data.value;
		var pendapatan = $("#pendapatan").val();
		// console.log(jasa);
		$("#total_pendapatan_jasa").val(jumlahPendapatan(data , pendapatan));
		$("#zakat_pendapatan_jasa").val((jumlahPendapatan(data , pendapatan) * 2.5) / 100).trigger('mask.maskMoney');
	}


</script>

<script>
	$(function() {
		$(".hargaa").maskMoney({ allowNegative: true, thousands:'.', affixesStay: false});
	})
</script>