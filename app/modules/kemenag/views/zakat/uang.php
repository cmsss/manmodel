<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>asset/plugin/maskmoney/jquery.maskMoney.js" type="text/javascript"></script>
<div class="desc_calczakat">
	<h4><strong>Zakat Uang dan Surat berharga lainnya</strong></h4>
	<blockquote> zakat yang dikenakan atas uang, harta yang disetarakan dengan uang, dan surat berharga lainnya yang telah mencapai <strong>nisab dan haul</strong> . Zakat uang surat berharga wajib dikenakan atas kepemilikan emas yang telah mencapai nisab <strong>85 gram emas</strong>. Kadar zakat atas uang dan surat berharga sebesar <strong>2,5%</strong>.</blockquote><br>
</div>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<tbody>
			<tr>
				<th colspan="2" class="header"><strong>Nishab</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Harga 1 gram emas</th>
				<th class="value_zakat">Rp <input type="text" id="harga_emas_uang" class="input_angka" onkeyup="hargaEmas(this);" value="0"></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Nishab</strong></th>
				<th class="value_zakat">Rp <input type="text" id="nisab_emas_uang" class="input_angka" disabled="disabled" value="0"> <input type="hidden" id="nisab_emas_uang_float" value="0"></th>
			</tr>

			<tr>
				<th colspan="2" class="header"><strong>Harta</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Uang tunai dan tabungan</th>
				<th class="value_zakat">Rp <input type="text" id="uang_tabungan" onkeyup="uangTabungan(this);" class="input_angka"  ></th>
			</tr>
			<tr>
				<th class="label_zakat">Saham dan surat berharga lainnya</th>
				<th class="value_zakat">Rp <input type="text" id="saham" onkeyup="Saham(this);" class="input_angka"  ></th>
			</tr>
			<tr>
				<th class="label_zakat">Piutang</th>
				<th class="value_zakat">Rp <input type="text" id="piutang" onkeyup="Piutang(this);" class="input_angka"  ></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Total Harta</strong></th>
				<th class="value_zakat">Rp <input type="text" id="total_harta" class="input_angka" value="0" disabled="disabled"> <input type="hidden" id="total_harta_float"></th>
			</tr>

			<tr>
				<th colspan="2" class="header"><strong>Kewajiban</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Hutang</th>
				<th class="value_zakat">Rp <input type="text" id="hutang" class="input_angka"  onkeyup="Hutang(this);"></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Total Kewajiban</strong></th>
				<th class="value_zakat">Rp <input type="text" id="total_kewajiban" class="input_angka" value="0" disabled="disabled"> <input type="hidden" id="total_kewajiban_float"></th>
			</tr>

			<tr>
				<th colspan="2" class="header"><strong>Zakat</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Selisih harta dan kewajiban</th>
				<th class="value_zakat">Rp <input type="text" id="selisih_harta" class="input_angka" value="0" disabled="disabled"></th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Zakat yang dikeluarkan</strong></th>
				<th class="value_zakat">Rp <input type="text" id="zakat_harta" class="input_angka" value="0" disabled="disabled"></th>
			</tr>

			<tr>
				<th colspan="2" class="header" id="ket_mal">&nbsp;</th>
			</tr>
		</tbody>
	</table>
</div>

<script>
	function hargaEmas(data){
		var data = data.value;
		if (data ==  '') {
			$("#nisab_emas_uang").val(0 * 85 );
		}else{
			$("#nisab_emas_uang").val(parseFloat(data) * 85 );
		}

	}

	function tambah(a , b , c , d ){
		var a = parseFloat(a);
		var b = parseFloat(b);
		var c = parseFloat(c);

		if (a >= 1) {
			var aa = a;
		}else{
			var aa = 0;
		}

		if (b >= 1) {
			var bb = b;
		}else{
			var bb = 0;
		}

		if (c >= 1) {
			var cc = c;
		}else{
			var cc = 0;
		}

		return aa + bb + cc;
	}

	function kurang(a, b){
		var a = parseFloat(a);
		var b = parseFloat(b);

		if (a >= 1) {
			var aa = a;
		}else{
			var aa = 0;
		}

		if (b >= 1) {
			var bb = b;
		}else{
			var bb = 0;
		}

		return aa - bb;
	}

	function uangTabungan(data){
		var data 	= data.value;
		var saham 	= parseFloat($("#saham").val());
		var piutang = parseFloat($("#piutang").val());
		var total_harta = tambah(data , saham ,piutang);
		
		$("#total_harta").val(total_harta);

		$("#selisih_harta").val(kurang(total_harta , $("#total_kewajiban").val()));
		$("#zakat_harta").val(($("#selisih_harta").val() * 2.5) / 100);
		

	}

	function Saham(data){
		var data 			= data.value;
		var uang_tabungan 	= parseFloat($("#uang_tabungan").val());
		var piutang 		= parseFloat($("#piutang").val());
		var total_harta 	= tambah(data , uang_tabungan ,piutang);

		$("#total_harta").val(total_harta);
		$("#selisih_harta").val(kurang(total_harta , $("#total_kewajiban").val()));
		$("#zakat_harta").val(($("#selisih_harta").val() * 2.5) / 100);
		
	}

	function Piutang(data){
		var data 			= data.value;
		var uang_tabungan 	= parseFloat($("#uang_tabungan").val());
		var saham 		= parseFloat($("#saham").val());
		var total_harta 	= tambah(data , uang_tabungan ,saham);
		
		$("#total_harta").val(total_harta);
		$("#selisih_harta").val(kurang(total_harta , $("#total_kewajiban").val()));
		$("#zakat_harta").val(($("#selisih_harta").val() * 2.5) / 100);
		
	}

	function Hutang(data){
			var data = parseFloat(data.value);
		if (data >= 1) {
			var total_kewajiban = $("#total_kewajiban").val(data);
			$("#selisih_harta").val(parseFloat($("#total_harta").val()) - data);
			if (parseFloat($("#selisih_harta").val()) > 0) {
				$("#zakat_harta").val((parseFloat($("#selisih_harta").val()) * 2.5) / 100);
			}else{
				$("#zakat_harta").val(0);
			}
			

		}else{
			var totol_kewajiban = $("#total_kewajiban").val(0);
			$("#selisih_harta").val(parseFloat($("#total_harta").val()) - 0);
			$("#zakat_harta").val(($("#selisih_harta").val() * 2.5)/100);
		}

		// console.log(totol_kewajiban);
		// $("#selisih_harta").val(parseFloat($("#total_harta").val()) - totol_kewajiban);
		
	}


</script>

<script>
	$(function() {
		$(".harga").maskMoney({ allowNegative: true, thousands:',', affixesStay: false});
	})
</script>