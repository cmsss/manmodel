<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>asset/plugin/maskmoney/jquery.maskMoney.js" type="text/javascript"></script>
<div class="desc_calczakat">
	<h4><strong>Zakat Rikaz(Temuan)</strong></h4>
	<blockquote> Zakat Rikaz tidak disyaratkan adanya Nisab. Kadar zakat rikaz sebesar <strong>1/5 atau 20%.</strong></blockquote><br>
</div>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<tbody>
		<tr>
			<th colspan="2" class="header"><strong>Zakat Rikaz</strong></th>
		</tr>
		<tr>
			<th class="label_zakat">Rikaz(Temuan)</th>
			<th class="value_zakat"><input onkeyup="Rikaz(this);" type="text" id="panen" class="input_angka" style="width:100px" ></th>
		</tr>

		<tr>
			<th colspan="2" class="header"><strong>Zakat</strong></th>
		</tr>
		<tr class="total_zakat">
			<th class="label_zakat"><strong>Zakat yang dikeluarkan</strong></th>
			<th class="value_zakat"><input type="text" id="hasil_rikaz" class="input_angka" value="0" style="width:100px" disabled="disabled"></th>
		</tr>

		<tr>
			<th colspan="2" class="header" id="keterangan">&nbsp;</th>
		</tr>
	</tbody>
</table>
</div>



<script>

	function Rikaz(data){
		var data = parseFloat(data.value);
		if (data >= 1) {
			$("#hasil_rikaz").val( (data * 20 )/ 100);

		}else{
			$("#hasil_rikaz").val( 0);
		}
	}

	$(function() {
		$(".harga").maskMoney({ allowNegative: true, thousands:',', affixesStay: false});
	})
</script>