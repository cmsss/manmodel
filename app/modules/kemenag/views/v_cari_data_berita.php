<div class="marginTop">
    <div class="panel-heading">
        <h3 class="panel-title">ditemukan <?= $data_yang_dicari_count->id ?> Artikel </h3>
    </div>
    <div class="panel-body">
        
            <?= form_open('', 'class=" form-horizontal"'); ?>
                <div class="form-group">
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <?php //form_input('tgl_a',$tgl_a, 'class="form-control datepicker" '); ?>
                            <input type="hidden" name="tgl_a" value="<?php echo $tgl_a ?>">
                        </div>
                        <div class="col-sm-1">
                            
                        </div>
                        <div class="col-sm-5">
                            <?php // form_input('tgl_b',$tgl_b, 'class="form-control datepicker"'); ?>
                            <input type="hidden" name="tgl_b" value="<?php echo $tgl_b ?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <?php // form_dropdown('kategori', $l_kategori,$kategori, 'class="form-control "'); ?>
                        <input type="hidden" name="kategori" value="">
                    </div>
                    <div class="col-sm-2">
                        <?php //  form_dropdown('by', $semua,$by, 'class="form-control "'); ?>
                        <input type="hidden" name="by" value="berita">
                    </div>
                    <div class="col-sm-3">
                        
                        <div class="form-group has-feedback">
                            <input name="data" type="text" class="form-control typeahead rounded" value="<?= $hasil ?>" placeholder="Kata Kunci">
                            <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded" style="top: 4px;
                                right: 5px;
                                width: 37px;
                                height: 26px;
                                line-height: 12px;
                                position: absolute;
                                pointer-events: auto"></button>
                        </div>
                    </div>
                </div>
            <?= form_close(); ?>
        	<?php if (!empty($data_yang_dicari)) {
                foreach ($data_yang_dicari as $l_berita) {

                    ?>

                    <?php 
                        $tgl = explode(' ', $l_berita->tgl_input);
                        $judul = $l_berita->judul;
                       
                        $judul_fix = flag($judul);

                         // waktu ts
                        $date = new DateTime();
                        $tgl_n = $date->setTimestamp($l_berita->ts);
                        $tgl_pecah1 = explode(' ', $date->format('Y-m-d H:i:s'));

                        // waktu ts nama_hari($tgl_pecah1[0]).' '.  tgl_indo($tgl_pecah1[0]) .' '.$tgl_pecah1[1]

                     ?>

                        <div class="col-md-12">
                            <div class="marginBottomMedia">
                                <div class="media">
                                    <a class="pull-left" href="<?= site_url('berita/' .$l_berita->id.'/'.$judul_fix) ?>">
                                    <!-- 100x100 -->
                                        <img class="media-object thumbnail img-responsive" style="width: 200px;" src="<?= base_url('files/gorontalo/file/fotoberita/' .$l_berita->id.'.jpg') ?>" onerror="this.onerror=null;this.src='<?= base_url('uploads/kemenag/200x200.png') ?>';"  alt="...">
                                    </a>
                                    <div class="media-body">
                                        <a  href="<?= site_url('berita/' .$l_berita->id.'/'.$judul_fix) ?>">
                                            <h4 class="media-heading"><?= $l_berita->judul ?></h4>
                                        </a>
                                        <p><?= substr($l_berita->isi, 0 , 150) ?> ... 
                                        <ul class="blog-meta">
                                            <!-- <li>By: <a href="http://djavaui.com/" target="_blank">Djava UI</a></li> -->
                                            <li><?php echo nama_hari($tgl_pecah1[0]).' '.  tgl_indo($tgl_pecah1[0]) .' '.$tgl_pecah1[1]?></li>
                                            <li><a href=""><?= $l_berita->counters ?>x dibaca</a></li>
                                        </ul>
                                        </p>

                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>

                    <?php
                }
            } ?>
        
        
        
    </div>
</div>
