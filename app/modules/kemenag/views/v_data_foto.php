<div class="marginTop">
    <div class="panel-heading">
        <h3 class="panel-title">Galeri Foto</h3>
    </div>
    <div class="panel-body">
        <div class="cbp-panel">
            <div class="row">
                <div class="gallery-list d-flex justify-content-between flex-wrap">
                    <?php if (!empty($list_foto)): ?>
                        <?php foreach ($list_foto as $l_foto): ?>
                            <div class="col-sm-4">
                                <a href="<?= base_url('files/gorontalo/file/foto/'.$l_foto->id.'.jpg') ?>" class="gallery-img" title="<?php echo strip_tags($l_foto->keterangan) ?>"><img src="<?= base_url('files/gorontalo/file/foto/'.$l_foto->id.'.jpg') ?>" alt="<?php echo strip_tags($l_foto->keterangan) ?>"></a>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
            <?= $page ?>
        </div>
    </div>
</div>