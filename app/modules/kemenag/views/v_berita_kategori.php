<?php 

    if (!empty($berita_kategori)) {
        foreach ($berita_kategori as $kepo ) {

            $judul = $kepo->judul;
            $judul_sterlir = str_replace(array('.' ,',' ,'(' , ')' ,'{' ,'}','?' ,'!' , '=','/'), '', $judul);
            $judul_sterlir2 = str_replace(' ', '-', $judul_sterlir);
            $judul_fix = strtolower($judul_sterlir2);
            ?>
                <a href="<?= site_url('berita/'.$kepo->id.'/'.$judul_fix) ?>" class="list-group-item">
                    <?= $kepo->judul ?>
                </a>

            <?php
        }
    }

 ?>