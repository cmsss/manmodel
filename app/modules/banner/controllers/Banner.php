<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banner extends MX_Controller {

    private $module;
    private $redirect;

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

        $this->load->model('m_banner');
        $this->load->model('m_fm');
        $this->load->model('m_kategori1');
        $this->module = "banner";
        $this->redirect = "adminweb/banner.asp";

    }

    public function index() {
        $data['list']   = $this->m_banner->get_all();
        $data['module'] = $this->module;
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

    

    public function tambah()
    {
        $data['module'] = $this->module;
        $data['view_file']   = 'tambah';
        echo Modules::run('template/render_master' , $data);
    }


    public function edit($id)
    {
        $cek =  $this->m_banner->get_by(array('id' =>$id));
        if ($cek) {
            $data['e'] = $cek;
            $data['module'] = $this->module;
            $data['view_file']   = 'edit';
            echo Modules::run('template/render_master' , $data);
        }else{
            redirect($this->redirect,'refresh');
        }
    }


    

    public function simpan()
    {
        if ($this->input->post()) {
            // die(print_r('simpan'));
            $tipenyafile = pathinfo($_FILES["nama"]['name'] ,PATHINFO_EXTENSION );
            $namanyafile = pathinfo($_FILES["nama"]['name']  ,PATHINFO_BASENAME);
            
            // die(print_r($tipenyafile));


            // $folder = str_replace( array(':' , ' '), '', $this->input->post('dir'));
            // if ($tipenyafile == 'jpg') {
            //     $path = './files/file/foto';    
            
            //     if (!is_dir($path)) { //create the folder if it's not already exists
            //         mkdir($path, 0777, TRUE);
            //     }
            // }

            
           
            // chmod('./files/file/', 0777);
            
          

            // pdf/doc/docx/ppt/pptx/zip/rar/jpg
            // $idincrement = auto_inc('m_fm','id');
            $idincrement = auto_inc('m_banner','id');
            $nmfile = str_replace(array(':' , ' '), '-',$idincrement.$namanyafile);
            $config['upload_path']= './files/gorontalo/file/banner/';
            $config['allowed_types']="jpg|png|gif|jpeg";
            $config['max_size']="10240000";
            // $config['max_width']="0";
            // $config['max_height']="0";
            // $config['encrypt_name']= TRUE;
            $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            $this->upload->initialize($config);


         
            if ($this->upload->do_upload('nama')) {
                $file = $this->upload->data();
                 // die(print_r($file));
                // die(print_r($file));
                $idincrement = auto_inc('m_banner', 'id');
                $data= array(
                    'id'=> $idincrement,
                    'filegambar'=>$file['file_name'],
                    'ukuran' => $file['file_size'],
                    // 'mime' => $file['file_type'],
                    'besar' => $file['image_width'].'x'.$file['image_height'],
                    'tampil' => $this->input->post('tampil'),
                    'grup' => $this->input->post('grup'),
                    'domain' => NAMA_DOMAIN,
                    
                    'nomor' => $this->input->post('nomor')
                    );
                    if ($this->input->post('url')) {
                        $data['url'] = $this->input->post('url');
                    }else{
                        $data['url'] = '';
                    }
                $this->m_banner->insert($data);

                    echo "<script>alert('Berhasil Upload File');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }else{
                     echo "<script>alert('Gagal upload');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }
        }
    }


    public function simpan_perubahan(){
        if ($this->input->post()) {

            $cek =  $this->m_banner->get_by(array('id' =>$this->input->post('id')));



            $tipenyafile = pathinfo($_FILES["nama"]['name'] ,PATHINFO_EXTENSION );
            $namanyafile = pathinfo($_FILES["nama"]['name']  ,PATHINFO_BASENAME);
            
            // die(print_r($tipenyafile));


            // $folder = str_replace( array(':' , ' '), '', $this->input->post('dir'));
            // if ($tipenyafile == 'jpg') {
            //     $path = './files/file/foto';    
            
            //     if (!is_dir($path)) { //create the folder if it's not already exists
            //         mkdir($path, 0777, TRUE);
            //     }
            // }

            
           
            // chmod('./files/file/', 0777);
            
          

            // pdf/doc/docx/ppt/pptx/zip/rar/jpg
            // $idincrement = auto_inc('m_fm','id');
            $idincrement = auto_inc('m_banner','id');
            $nmfile = str_replace(array(':' , ' '), '-',$idincrement.$namanyafile);
            $config['upload_path']= './files/gorontalo/file/banner';
            $config['allowed_types']="jpg|png|gif|jpeg";
            $config['max_size']="10240000";
            // $config['max_width']="0";
            // $config['max_height']="0";
            // $config['encrypt_name']= TRUE;
            $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            $this->upload->initialize($config);

            if ($this->upload->do_upload('nama') != '') {

                $patfile = realpath(APPPATH . '../files/gorontalo/file/banner/');
                if ($cek->filegambar != '') {
                    unlink($patfile . '/' . $cek->filegambar);
                }

                $file = $this->upload->data();
            
                // die(print_r($file));
                // $idincrement = auto_inc('m_fm', 'id');
                $data= array(
                    'filegambar'=>$file['file_name'],
                    'ukuran' => $file['file_size'],
                    // 'mime' => $file['file_type'],
                    'besar' => $file['image_width'].'x'.$file['image_height'],
                    'tampil' => $this->input->post('tampil'),
                    'grup' => $this->input->post('grup'),
                    'domain' => NAMA_DOMAIN,
                    'nomor' => $this->input->post('nomor')
                    );
                if ($this->input->post('url')) {
                    $data['url'] = $this->input->post('url');
                }else{
                    $data['url'] = '';
                }
                $this->m_banner->update($this->input->post('id') , $data);
                    echo "<script>alert('Berhasil Upload File');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }else{

                         $data= array(
                            // 'mime' => $file['file_type'],
                            'tampil' => $this->input->post('tampil'),
                            'nomor' => $this->input->post('nomor'),
                            'grup' => $this->input->post('grup'),
                            );
                         if ($this->input->post('url')) {
                            $data['url'] = $this->input->post('url');
                        }else{
                            $data['url'] = '';
                        }
                        $this->m_banner->update($this->input->post('id') , $data);


                     echo "<script>alert('Berhasil Menyimpan Tanpa Mengubah File');
            window.location=('" . site_url($this->redirect) . "');</script>";
                }




        }
    }


    public function tampil_a($id){
        $cek = $this->m_banner->get_by(array('id' => $id));
        if ($cek->tampil == '1') {
            $data = array(
                 'tampil' => '0' 
                );
            $this->m_banner->update($id , $data);
            $tampil = '0';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->tampil == '0') {
            $data = array(
                 'tampil' => '1' 
                );
            $this->m_banner->update($id , $data);
            $tampil = '1';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }


    


    public function  hapus($id)
    {
        $cek = $this->m_banner->get_by(array('id' => $id));
        if ($cek) {
            
            // $tipenyafile = pathinfo($cek->nama ,PATHINFO_EXTENSION );
            // die(print_r($tipenyafile));  


            // if ($tipenyafile == 'jpg') {
            //     $patfile = realpath(APPPATH . '../files/file/foto/' . $cek->dir);    
            
            // }elseif ($tipenyafile == 'pdf' || $tipenyafile == 'doc' || $tipenyafile == 'docx' || $tipenyafile == 'ppt' || $tipenyafile == 'pptx' || $tipenyafile == 'rar' || $tipenyafile == 'zip' ) {
            //     $patfile = realpath(APPPATH . '../files/file/file/' . $cek->dir);
            // }else {
            //     redirect('adminweb/file.asp','refresh');
            // }
            $patfile = realpath(APPPATH . '../files/gorontalo/file/banner');

            if ($cek->filegambar != '') {
                unlink($patfile . '/' . $cek->filegambar);
            }
            $this->m_banner->delete($id);
            redirect($this->redirect,'refresh');
        }else{
            redirect($this->redirect,'refresh');
        }
    }

}