<div class="container-fluid">
	<section class="content-header">
		<h4>Edit Banner</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('adminweb/banner/simpan-perubahan',array('class'=>'form-horizontal')); ?>
			

			<div class="row form-group">
				<label class="control-label col-sm-2"></label>
				<div class="col-sm-10">
					<img style="height: 200px;" class="img-responsive" src="<?= base_url('files/gorontalo/file/banner/'.$e->filegambar) ?>" alt="">
					<?= form_hidden('id', $e->id); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">File Banner</label>
				<div class="col-sm-10">
					<span class="pull-right">Format : JPG|PNG|GIF</span>
					<input type="file" name="nama" value=""  class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Grup</label>
				<div class="col-sm-10">
					<?php echo form_dropdown('grup',array('' => 'Pilih Grup' , '2' => '2' , '10' => '10'),$e->grup,'class="form-control" required onchange="grupBanner(this)"'); ?>
				</div>
			</div>
			<div id="grupid">
				<?php if ($e->url != ''): ?>
					<div class="row form-group">
						<label class="control-label col-sm-2">URL</label>
						<div class="col-sm-10">
							<?php echo form_input('url',$e->url,'class="form-control"  placeholder="url"'); ?>
						</div>
					</div>
				<?php endif ?>
			</div>
			
			<div class="row form-group">
				<label class="control-label col-sm-2">Nomor</label>
				<div class="col-sm-10">
					<input type="number" name="nomor" value="<?= $e->nomor ?>" class="form-control" required="required">
				</div>
			</div>
			
			
			<div class="row form-group">
				<label class="control-label col-sm-2">Tampil</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="tampil" value="1" style="margin-right: 10px;" <?= $t1 = ($e->tampil == '1' ? 'checked ' : ''); ?> >
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="tampil" value="0" style="margin-right: 10px; padding-left: 10px;" <?= $t0 = ($e->tampil == '0' ? 'checked ' : ''); ?>>
						<label class="">Tidak</label>
					</div>
				</div>
			</div>

				<div class="row form-group">

					<div class="col-sm-12">
						<a href="<?= site_url('adminweb/banner.asp') ?>" class="btn btn-default pull-right">Kembali</a>
						<label class="col-sm-2"></label>
						<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
					</div>
				</div>

				<?php echo  form_close(); ?>
			</section>
		</div>
	</div>


<script>
	
	function grupBanner(data) {
		var data = data.value;
		if (data == '10') {
			$("#grupid").html(
				'<div class="row form-group">' +
					'<label class="control-label col-sm-2">URL</label>' +
					'<div class="col-sm-10">'+
						'<input type="text" name="url" class="form-control" required="required" placeholder="url">'+
					'</div>'+
				'</div>'
				);	

		}else{
			$("#grupid").html('');
		}
	}
</script>


