  <section class="content-header">
    <h1>Banner</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Banner</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('adminweb/banner/post.asp'); ?>" class="btn btn-info"><i class="fa fa-plus"></i> Banner</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>
			<div class="table-responsive">
				<table class="table table-hover table-condensed" id="list_kategori">
					<thead>
						<tr>
							<th>No</th>
							<th>Gambar</th>
							<th>URL</th>
							<th>Tampil</th>
							<th>Nomor</th>
							<th>Grup</th>
							<th>aksi</th>
						</tr>
					</thead>
					<tbody>
					
						<?php 
							$no=1;
							foreach ($list as $row) {?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td>
									<img src="<?= base_url('files/gorontalo/file/banner/'.$row->filegambar) ?>" class="img-responsive" style="width: 80px" alt="">

							</td>
							<td><?php echo $row->url; ?></td>
							<td>
								<div id="tampil<?= $row->id ?>">
								<?php
									if ($row->tampil == '1') {
										?>
											<a href="#" onclick="tampilA('<?= $row->id ?>');"><small class="label  bg-green">Ya</small></a>
										<?php
									}else {
										?>
											<a href="#" onclick="tampilA('<?= $row->id ?>');"><small class="label  bg-red">Tidak</small></a>
										<?php
									}

								?>
								</div>
								
								
							</td>
							<td><?php echo $row->nomor; ?></td>
							<td><?php echo $row->grup; ?></td>

							
							<td>
								<a href="<?php echo site_url('adminweb/banner/edit/'.$row->id); ?>"  class="btn btn-warning"><i class="fa fa-pencil"></i></a>
								<a href="<?php echo site_url('adminweb/banner/delete/'.$row->id); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-danger"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<?php } ?>
						
					</tbody>
				</table>
			</div>
			
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>


<script>
	function tampilA(data) {
		// alert(data);
		var nilai = data;
		$.ajax({
			url: '<?php echo site_url();?>adminweb/banner/tampila/' + nilai,
			// type: 'POST',
			dataType: 'html',
			beforeSend : function(){

			},
			success : function(data){
				$("#tampil" + nilai).html(data);
			}
		});
		
		
	}
	
</script>