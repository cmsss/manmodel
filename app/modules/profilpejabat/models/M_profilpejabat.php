<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_profilpejabat extends MY_Model{
    public function __construct() {
        parent::__construct();
        parent::set_table('t_profil_pejabat', 'id_profil_pejabat');
    }
    public function get_slider(){
        $this->db->limit(5);
        // $this->db->where(array('headline' => '1'));
        // $this->db->where(array('status' => '1'));
        return parent::get_all();
    }
    public function get_max_id() {
        $this->db->limit('1');
        $this->db->where("img <> ''");
        $this->db->where(array('slide'=>'1'));
        $data = parent::get_all();
        if (is_array($data)) {
            foreach ($data as $d) {
                $id = $d->id_profil_pejabat;
            }
        }
        return $id;
    }

    public function get_profil(){
        $this->db->where(array('slide'=>'1'));
        return parent::get_all();
    }



}
