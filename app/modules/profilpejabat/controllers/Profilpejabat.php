<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profilpejabat extends MX_Controller {



    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_profilpejabat');
        $this->load->model('bidang/m_bidang');
    }

    public function index() {
    	$data['list'] = $this->m_profilpejabat->get_all();
        $data['module'] = "profilpejabat";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

      public function add(){
        if ($this->input->post()) {
            $config['upload_path']="./uploads";
            $config['allowed_types']="png|jpg";
            $config['max_size']="1024";
            $config['max_width']="2000";
            $config['max_height']="1000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);
            $idincrement = auto_inc('m_profilpejabat','id_profil_pejabat');
            if ($this->upload->do_upload('img')) {
                $data= array(
                    'id_profil_pejabat'=> $idincrement,
                    'nama'=>$this->input->post('nama'),
                    'img'=>$this->upload->file_name,
                    'jabatan'=>$this->input->post('jabatan'),
                    'devisi'=>$this->input->post('devisi'),
                    'no_hp'=>$this->input->post('no_hp'),
                    'tupoksi'=>$this->input->post('tupoksi'),
                    'email' => $this->input->post('email'),
                    'nip' => $this->input->post('nip'),
                    'skp' => $this->input->post('skp'),
                    'slide' => $this->input->post('slide')
                    );
                if (!$this->m_profilpejabat->insert($data)){
                    echo "<script>alert('Profil pejabat berhasil disimpan');
            window.location=('" . site_url('adminweb/profilpejabat.asp') . "');</script>";
                }else{
                    echo "<script>alert('Gagal simpan Profil Pejabat');
            window.location=('" . site_url('adminweb/profilpejabat.asp') . "');</script>";
                }

            }else {
              $data= array(
                  'id_profil_pejabat'=> $idincrement,
                  'nama'=>$this->input->post('nama'),
                  'jabatan'=>$this->input->post('jabatan'),
                  'devisi'=>$this->input->post('devisi'),
                  'no_hp'=>$this->input->post('no_hp'),
                  'tupoksi'=>$this->input->post('tupoksi'),
                  'email' => $this->input->post('email'),
                  'nip' => $this->input->post('nip'),
                  'slide' => $this->input->post('slide'),
                  'skp' => $this->input->post('skp')
                  );
                  if (!$this->m_profilpejabat->insert($data)){
                      echo "<script>alert('Profil Pejabat berhasil disimpan tanpa foto');
              window.location=('" . site_url('adminweb/profilpejabat.asp') . "');</script>";
                  }else{
                      echo "<script>alert('Gagal simpan Profil Pejabat');
              window.location=('" . site_url('adminweb/profilpejabat.asp') . "');</script>";
                  }
            }
        }
        $data['module']="profilpejabat";
        $data['drop_bidang'] = drop_list('m_bidang', 'flag','nama', 'Pilih Bidang','get_all','asc' );
        $data['view_file']="add_profilpejabat";
        echo Modules::run('template/render_master',$data);
    }
    public function edit($id){
      if ($cek = $this->m_profilpejabat->get_by(array('id_profil_pejabat'=>$id))){
          $config['upload_path']="./uploads/";
          $config['allowed_types']="jpg|png";
          $config['max_size']="1024";
          $config['max_width']="2000";
          $config['max_height']="1000";
          $config['encrypt_name']= TRUE;
          $this->upload->initialize($config);
          if ($this->input->post()) {
              if ($this->upload->do_upload('img')) {
                  $pathgambar = realpath(APPPATH . '../uploads/');
                  if ($cek->img != '') {
                      unlink($pathgambar . '/' . $cek->img);
                  }
                     $img = $this->upload->data();
                     $data= array(
                         'nama'=>$this->input->post('nama'),
                         'img'=>$this->upload->file_name,
                         'jabatan'=>$this->input->post('jabatan'),
                         'devisi'=>$this->input->post('devisi'),
                         'no_hp'=>$this->input->post('no_hp'),
                         'tupoksi'=>$this->input->post('tupoksi'),
                         'email' => $this->input->post('email'),
                         'nip' => $this->input->post('nip'),
                         'skp' => $this->input->post('skp'),
                         'slide' => $this->input->post('slide')
                         );
                     if ($this->m_profilpejabat->update($id,$data)){
                         echo "<script>alert('Profil pejabat berhasil disimpan');
                 window.location=('" . site_url('adminweb/profilpejabat.asp') . "');</script>";
                     }else{
                         echo "<script>alert('Gagal simpan Profil Pejabat');
                 window.location=('" . site_url('adminweb/profilpejabat.asp') . "');</script>";
                     }
                  }else {
                    $data= array(
                        'nama'=>$this->input->post('nama'),
                        'jabatan'=>$this->input->post('jabatan'),
                        'devisi'=>$this->input->post('devisi'),
                        'no_hp'=>$this->input->post('no_hp'),
                        'tupoksi'=>$this->input->post('tupoksi'),
                        'email' => $this->input->post('email'),
                        'nip' => $this->input->post('nip'),
                        'skp' => $this->input->post('skp'),
                        'slide' => $this->input->post('slide')
                        );
                    if ($this->m_profilpejabat->update($id,$data)){
                        echo "<script>alert('Profil pejabat berhasil disimpan');
                window.location=('" . site_url('adminweb/profilpejabat.asp') . "');</script>";
                    }else{
                        echo "<script>alert('Gagal simpan Profil Pejabat');
                window.location=('" . site_url('adminweb/profilpejabat.asp') . "');</script>";
                    }
              }
          }
        }
        $data['edit']=$this->m_profilpejabat->get_by(array('id_profil_pejabat'=>$id));
        $data['module']="profilpejabat";
        $data['drop_bidang'] = drop_list('m_bidang', 'flag','nama', 'Pilih Bidang','get_all','asc' );
        $data['view_file']="edit_profilpejabat";
        echo Modules::run('template/render_master',$data);
    }
    public function hapus($id){
      if ($cek = $this->m_profilpejabat->get_by(array('id_profil_pejabat'=>$id))){
          $pathgambar = realpath(APPPATH . '../uploads/');
          if ($cek->img != '') {
              unlink($pathgambar . '/' . $cek->img);
          }
          $this->m_profilpejabat->delete($id);
      }
      redirect('adminweb/profilpejabat.asp');
    }

}

