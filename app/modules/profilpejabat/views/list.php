  <section class="content-header">
    <h1></i>Profil Pejabat</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Profil Pejabat</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('adminweb/profilpejabat/post.asp'); ?>" class="btn btn-info">Tambah</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>

			<table class="table table-hover table-condensed" id="list_kategori">
				<thead>
					<tr>
						<td>No</td>
						<td>Nama</td>
						<td>NIP</td>
						<td>Jabatan</td>
						<td>Devisi</td>
						<td>Email</td>
						<td>Aksi</td>
					</tr>
				</thead>
				<tbody>

					<?php
						$no=1;
						foreach ($list as $row) {?>
					<tr>
						<td><?php echo $no++ ?></td>
						<td><?php echo $row->nama; ?></td>
						<td><?= $row->nip; ?></td>
						<td><?php echo $row->jabatan; ?></td>
						<td><?php echo ucwords(str_replace('-',' ',$row->devisi)); ?></td>
						<td><?php echo $row->email; ?></td>
						<td>
							<a href="<?php echo site_url('adminweb/profilpejabat/update/'.$row->id_profil_pejabat); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
							<a href="<?php echo site_url('adminweb/profilpejabat/delete/'.$row->id_profil_pejabat); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					<?php } ?>

				</tbody>
			</table>
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
