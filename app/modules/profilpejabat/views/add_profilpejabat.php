<?php $devisi=array(
	''=>'Pilih Bidang',
	'kepala'=>'Kepala',
	'sekretariat'=>'Sekretariat',
	'bidang-perencanaan-ekonomi'=>'Bidang Perencnaan Ekonomi',
	'bidang-perencanaan-sosial-budaya'=>'Bidang Perencnaan Sosial Budaya',
	'bidang-perencanaan-tata-ruang'=>'Bidang Perencnaan Tata Ruang',
	'bidang-pendataan-dan-evaluasi'=>'Bidang Pendataan dan Evaluasi',
) ?>
<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah Profil Pejabat</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">

		<?php echo form_open_multipart('',array('class'=>'form-horizontal')); ?>
			<div class="row form-group">
					<label class="control-label col-sm-2">Nama</label>
					<div class="col-sm-10">
						<?php echo form_input('nama',set_value('nama'),'class="form-control" placeholder="Nama"required'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">NIP</label>
					<div class="col-sm-10">
						<?php echo form_input('nip',set_value('nip'),'class="form-control" placeholder="Nomor Induk Pegawai"required'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">Bidang</label>
					<div class="col-sm-10">
					<?php echo form_dropdown('devisi',$drop_bidang,'','class="form-control" required'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">Jabatan</label>
					<div class="col-sm-10">
						<?php echo form_input('jabatan',set_value('jabatan'),'class="form-control" placeholder="Jabatan"required'); ?>
					</div>
			</div>
			
			<div class="row form-group">
					<label class="control-label col-sm-2">Email</label>
					<div class="col-sm-10">
						<?php echo form_input('email',set_value('email'),'class="form-control" placeholder="Email" required'); ?>
					</div>
			</div>
      <div class="row form-group">
					<label class="control-label col-sm-2">Nomor HP</label>
					<div class="col-sm-10">
						<?php echo form_input('no_hp',set_value('no_hp'),'class="form-control" placeholder="Nomor HP" required'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">SKP</label>
					<div class="col-sm-10">
						<?php echo form_input('skp',set_value('skp'),'class="form-control" placeholder="SKP"required'); ?>
					</div>
			</div>
      <div class="row form-group">
					<label class="control-label col-sm-2">Tupoksi</label>
					<div class="col-sm-10">
            <textarea name="tupoksi" rows="8" class="ckeditor" placeholder="Tupoksi"></textarea>

					</div>
			</div>
      		<div class="row form-group">
					<label class="control-label col-sm-2">Photo</label>
					<div class="col-sm-10">
						<input type="file" name="img"  class="form-control">
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">Tampilkan di Slide</label>
					<div class="col-sm-10">
						<input type="radio" value="1" name="slide"> Ya
						<input type="radio" value="0" name="slide"> Tidak
					</div>
			</div>
			<div class="row form-group">

					<div class="col-sm-10">
					<a href="<?= site_url('adminweb/profilpejabat.asp'); ?>" class="btn btn-default">Kembali</a>
						<?php echo form_submit('submit','Tambah Profil Pejabat','class="btn btn-primary"'); ?>
					</div>
			</div>
		<?php echo  form_close(); ?>
		</section>
	</div>
</div>


