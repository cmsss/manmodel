<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agenda extends MX_Controller {

    

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        if (!cek_role($this->session->userdata('level'),'Agenda'))
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_agenda');
    }

    public function data(){
        $header = array('Desa', 'Kecamatan','Kabupaten','Provinsi','#');
        $tmpl = array('table_open' => '<table class="table table-hover table-condensed" id="list_desa">');
        $this->table->set_template($tmpl);
        $this->table->set_heading($header);
        $data['table'] = $this->table->generate();
        $data['htitle'] = 'List Agenda';
        $data['view_file'] = 'list2';
        $data['module'] = 'agenda';
        echo Modules::run('template/render_master' , $data);
    }

    public function index() {
    	$data['list'] = $this->m_agenda->get_all();
        $data['module'] = "agenda";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

    public function add1(){
    	$idincrement = auto_inc('m_agenda','id_agenda');
    	$tema = 'Pameran Gorantalo Utara';
    	echo url_title($tema,'-',TRUE).'.html';
    	echo $idincrement;
    }
    public function add(){
    	$idincrement = auto_inc('m_agenda','id_agenda');
        

    	if ($this->input->post()) {
            $pecahtgl = explode('/', $this->input->post('tgl'));
            $tanggal = $pecahtgl['2'].'-'.$pecahtgl['0'].'-'.$pecahtgl['1'];
    		$data= array(
    			'id_agenda'=> $idincrement,
    			'tema'=>$this->input->post('tema'),
    			'acara'=>$this->input->post('acara'),
    			'tempat'=>$this->input->post('tempat'),
    			'waktu'=>$this->input->post('waktu'),
                'tgl'=>$tanggal,
                'status'=>$this->input->post('status'),
    			'flag'=>url_title($this->input->post('tema'),'dash',TRUE).'.html',
    			);
    		$this->m_agenda->insert($data);
    		redirect('adminweb/agenda.asp','refresh');
			
			
    	}
    	$data['module']="agenda";
    	$data['view_file']="add_agenda";
    	echo Modules::run('template/render_master',$data);
    }
    public function edit($id){

    	if ($this->input->post()) {
            $pecahtgl = explode('/', $this->input->post('tgl'));
            $tanggal = $pecahtgl['2'].'-'.$pecahtgl['0'].'-'.$pecahtgl['1'];
    		$data= array(
    			
    			'tema'=>$this->input->post('tema'),
    			'acara'=>$this->input->post('acara'),
    			'tempat'=>$this->input->post('tempat'),
    			'waktu'=>$this->input->post('waktu'),
                'tgl'=>$tanggal,
                'status'=>$this->input->post('status'),
    			'flag'=>url_title($this->input->post('tema'),'dash',TRUE).'.html',
    			);
    		$this->m_agenda->update($id,$data);
    		redirect('adminweb/agenda.asp','refresh');
			
			
    	}
    	$data['edit']=$this->m_agenda->get_by(array('id_agenda'=>$id));
        $edit = $this->m_agenda->get_by(array('id_agenda'=>$id));
        $tanggal = explode('-', $edit->tgl);
        $data['tgl'] = $tanggal['1'].'/'.$tanggal['2'].'/'.$tanggal['0'];
    	$data['module']="agenda";
    	$data['view_file']="edit_agenda";
    	echo Modules::run('template/render_master',$data);
    }
    public function hapus($id){
    	$this->m_agenda->delete($id);
    	redirect('adminweb/agenda.asp','refresh');
    }

}

