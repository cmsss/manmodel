<div class="container-fluid">
  <section class="content-header">
    <h4>Edit Agenda</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">
			<?php echo form_open('',array('class'=>'form-horizontal')); ?>
				<div class="row form-group">
						<label class="control-label col-sm-2">Tema</label>
						<div class="col-sm-10">
							<?php echo form_input('tema',$edit->tema,'class="form-control" placeholder="Tema"'); ?>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Acara</label>
						<div class="col-sm-10">
							<textarea class="ckeditor" name="acara" rows="30" cols="80" style="width: 100%;" required="required"><?php echo $edit->acara; ?></textarea>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Tempat</label>
						<div class="col-sm-10">
							<?php echo form_input('tempat',$edit->tempat,'class="form-control" placeholder="Tempat"'); ?>
						</div>
				</div>
				<div class="row form-group">
					<label class="control-label col-sm-2">Tanggal</label>
					<div class="col-sm-10">
						<?php echo form_input('tgl',$tgl,'class="form-control datepicker" placeholder="Tanggal"required'); ?>
					</div>
			</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Waktu</label>
						<div class="col-sm-10">
							<?php echo form_input('waktu',$edit->waktu,'class="form-control" placeholder="Waktu"'); ?>
						</div>
				</div>
				<div class="row form-group">
					<label class="control-label col-sm-2">Status</label>
					<div class="col-sm-10">
	                    <?php if ($edit->status == '0') { ?>
	                        <input type="radio" name="status" value="0" checked="true"> Belum Selesai
	                        <input type="radio" name="status" value="1"> Selesai
	                    <?php } elseif ($edit->status == '1') { ?>
	                        <input type="radio" name="status" value="0"> Belum Selesai
	                        <input type="radio" name="status" value="1" checked="true"> Selesai
	                    <?php } else { ?>
	                        <input type="radio" name="status" value="0"> Belum Selesai
	                        <input type="radio" name="status" value="1"> Selesai
	                    <?php } ?>
					</div>
				</div>
				<div class="row form-group">
						<div class="col-sm-10">
						<a href="<?= site_url('adminweb/agenda.asp') ?>" class="btn btn-default">Kembali</a>
							<?php echo form_submit('submit','Edit Agenda','class="btn btn-primary"'); ?>
						</div>
				</div>
			<?php echo  form_close(); ?>
		</section>
	</div>
</div>

