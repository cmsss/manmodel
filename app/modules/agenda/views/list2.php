<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $htitle;?></h3>
        <div class="box-tools pull-right">
            <a href="<?php echo site_url('apps/master-data/penduduk/tambah.asp'); ?>" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Tambah Data</a>
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <?php echo $table; ?>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<!-- Modal -->
