<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_agenda extends MY_Model{
    public function __construct() {
        parent::__construct();
        parent::set_table('t_agenda', 'id_agenda');
    }

    public function get_apa($limit, $start) {
        $this->db->limit($limit, $start);
        return parent::get_all();
    }

    public function get_agenda(){
    	$this->db->where(array('status' => '0'));
    	return parent::get_all();
    }
    
}

