<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends MX_Controller {

    private $rule_login = array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|trim|xss_clean'
        )
    );

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect(site_url('adminweb/site-login.asp'));
    }

    public function log_admin() {
        if ($this->autentifikasi->sudah_login()) {
            redirect(site_url('adminweb/dashboard.asp'));
        } else {
            if ($this->input->post()) {
                $user = $this->input->post('username', TRUE);
                $pass = $this->input->post('password', TRUE);
                $this->form_validation->set_rules($this->rule_login);
                if ($this->form_validation->run() == TRUE) {
                    if ($this->autentifikasi->login($user, $pass)) {
                        if ($this->session->userdata('user_login') == "admin") {
                            $_SESSION['ses_admin'] = "admin";
                            $_SESSION['ses_kcfinder'] = array();
                            $_SESSION['ses_kcfinder']['disabled'] = false;
                            $_SESSION['ses_kcfinder']['uploadURL'] = "../content_upload";
                        }
                        redirect(site_url('adminweb/dashboard.asp'));
                    }
                }else{
                    echo "<script>alert('".  validation_errors()."');
			window.location=('" . site_url(uri_string()) . "');</script>";
                }
            }
            $data['module'] = 'login';
            $data['view_file'] = 'login_page';
            $this->load->view('v_login', $data);
        }
    }

    public function logout() {
        $this->session->set_userdata(array('user_login' => '', 'user_pass' => '', 'nama_lengkap' => '', 'level' => '', 'status' => ''));
        $this->session->sess_destroy();
        redirect(site_url());
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
