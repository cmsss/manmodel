<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_runtext extends MY_Model {

    public function __construct(){
        parent::__construct();
        parent::set_table('t_run_text','id_run_text');
    }

    public function  get_limit($limit)
    {
    	$d = $this->db->select('*')
    			->from('t_run_text')
    			->limit($limit)
    			->order_by('id_run_text' , 'desc')
    			->get();
    	return $d->result();
    }
}
?>
