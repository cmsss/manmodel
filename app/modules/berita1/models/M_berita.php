<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_berita extends MY_Model {
    
    public function __construct(){
        parent::__construct();
        parent::set_table('t_berita','id_berita');
    }
    public function get_judul($key) {
        $this->db->like('judul', $key, 'after');
        return parent::get_all();
    }

    public function total_artikel(){
    	return $this->db->count_all('t_berita');
    }

    public function get_slider(){
        $this->db->limit(5);
        $this->db->where(array('headline' => '1'));
        $this->db->where(array('status' => '1'));
        $this->db->order_by("DATE(create_date) DESC");
        return parent::get_all();
    }

    public function get_max_id() {
        $this->db->limit('1');
        $this->db->join('t_profile_user','t_profile_user.user_id = t_berita.penulis');
        $this->db->where("img <> ''");
        $this->db->where(array('headline'=>'1'));
        $this->db->where(array('status' => '1'));
        $this->db->order_by("DATE(create_date) DESC");
        $data = parent::get_all();
        if (is_array($data)) {
            foreach ($data as $d) {
                $id = $d->id_berita;
            }
        }
        return $id;
    }

    public function get_limit($limit, $start) {
        if (empty($start)){
            $startt = 5;
        }else{
            $startt = $start+5;
        }
        $this->db->limit($limit, $startt);
        $this->db->join('t_profile_user','t_profile_user.user_id = t_berita.penulis');
        $this->db->where(array('headline' => '1'));
        $this->db->order_by("DATE(create_date) DESC");
        $query = parent::get_all();

        if ($query) {
            return $query;
        }
        return false;
    }

    public function berita($num, $offset){
        $this->db->order_by('create_date','DESC');
        $data= $this->db->get('t_berita',$num,$offset);
        return $data->result();
    }

    public function count_berita($apa = FALSE) {

        return parent::get_all();
    }

    public function get_artikel($kat, $except = FALSE) {

        if ($except === FALSE) {

            $this->db->limit('1');
            $this->db->where(array('kategori' => $kat));
            $this->db->order_by("DATE(create_date) DESC");
        } else {

            $this->db->limit('4');
            $this->db->where("id_berita <> $except");
            $this->db->where(array('kategori' => $kat));
            $this->db->order_by("DATE(create_date) DESC");
        }

        $this->db->where(array('headline' => '1'));

        return parent::get_all();
    }
    public function get_apa($param, $limit, $start) {
        if (is_array($param)) {
            $this->db->where($param);
            $this->db->order_by("DATE(create_date) DESC");
            $this->db->join('t_profile_user','t_profile_user.user_id = t_berita.penulis');
            $this->db->limit($limit, $start);
            return parent::get_all();
        }
        return FALSE;
    }

    public function get_v_berita($limit = NULL  , $data = NULL )
    {
        if ($data === NULL) {
            $d = $this->db->query("SELECT * FROM v_berita ORDER BY create_date DESC LIMIT $limit");
        }else{
            $d = $this->db->select('*')
                    ->from('v_berita')
                    ->where($data)
                    ->limit($limit)
                    ->order_by('create_date', 'desc')
                    ->get();
        }

        return $d->result();
    }
    
    public  function get_by_v_berita($data)
    {
        $d = $this->db->select('*')
                ->from('v_berita')
                ->where($data)
                ->get();
        return $d->row();
    }

    public function get_random_v_berita($limit = NULL)
    {
        $d = $this->db->select('*')
                ->from('v_berita')
                ->where(array('status' => '1'))
                ->order_by('judul' , 'RANDOM')
                ->order_by('create_date' ,'desc')
                ->limit($limit)
                ->get();
        return $d->result();
    }

    public function get_populer_v_berita($limit = NULL)
    {
        $d = $this->db->select('*')
                ->from('v_berita')
                ->where('status' , '1')
                ->limit($limit)
                ->order_by('dibaca' , 'desc')
                ->order_by('create_date' , 'desc')
                ->get();
        return $d->result();
    }


    public function get_head_v_berita($limit =  NULL)
    {
        $d = $this->db->select('*')
            ->from('v_berita')
            ->where(array('headline' => '1' , 'status' => '1'))
            ->order_by('create_date' , 'desc')
            ->limit($limit)
            ->get();

        return $d->result();
    }

    public function  get_berita($limit = 1)
    {
        $d  = $this->db->select('*')
                ->from('v_berita')
                ->where('status' , '1')
                ->order_by('create_date' ,'desc')
                ->limit($limit)
                ->get();
        return $d->result();
    }
        
}
?>