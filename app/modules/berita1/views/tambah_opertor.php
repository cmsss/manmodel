<div class="container-fluid">
	<section class="content-header">
		<h4>Tambah Berita</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('adminweb/berita/simpann1',array('class'=>'form-horizontal')); ?>
			
			<div class="row">
				<div class="col-sm-8">
					<div class="row form-group">
						<label class="control-label col-sm-2">Judul</label>
						<div class="col-sm-10">
							<?php echo form_input('judul',set_value('judul'),'class="form-control" required placeholder="Judul"'); ?>
						</div>
					</div>
					<div class="row form-group">
						<label class="control-label col-sm-2"></label>
						<div class="col-sm-10">
							<textarea class="ckeditor" name="isi" rows="30" required cols="80" style="width: 100%;"></textarea>
						</div>
					</div>
					
					<div class="row form-group">
						<label class="control-label col-sm-2">Foto</label>
						<div class="col-sm-10">
						<span class="pull-right">Format : jpg</span>
							<input type="file" name="nama" value="" required="required" class="form-control">
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="row form-group">
						<label class="control-label col-sm-2">Kategori</label>
						<div class="col-sm-10">
							<?php echo form_dropdown('kategori', $kategori, '' , 'class="form-control" required'); ?>
						</div>
					</div>
					<div class="row form-group">
						<label class="control-label col-sm-2">Caption</label>
						<div class="col-sm-10">
							<?php echo form_input('caption',set_value('caption'),'class="form-control"  placeholder="caption"'); ?>
						</div>
					</div>
					
					

					<div class="row form-group">

						<div class="col-sm-12">
							<a href="<?= site_url('adminweb/berita.asp') ?>" class="btn btn-default pull-right">Kembali</a>
							<label class="col-sm-2"></label>
							<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
						</div>
					</div>
				</div>
			</div>
			
			

		<?php echo  form_close(); ?>
	</section>
</div>
</div>


