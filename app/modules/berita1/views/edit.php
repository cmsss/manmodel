<div class="container-fluid">
	<section class="content-header">
		<h4>Edit Berita</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('adminweb/berita/simpan-perubahan',array('class'=>'form-horizontal')); ?>
			
			<div class="row">
				<div class="col-sm-8">
					<div class="row form-group">
						<label class="control-label col-sm-2">Judul</label>
						<div class="col-sm-10">
							<?php echo form_input('judul',$e->judul,'class="form-control" required placeholder="Judul"'); ?>
						</div>
					</div>
					<div class="row form-group">
						<label class="control-label col-sm-2"></label>
						<div class="col-sm-10">
							<textarea class="ckeditor" name="isi" rows="30" required cols="80" style="width: 100%;"><?= $e->isi ?></textarea>
						</div>
					</div>
					<div class="row form-group">
						<label class="control-label col-sm-2"></label>
						<div class="col-sm-10">
							<img class="img-responsive" style="width: 200px; height: 100%" src="<?= base_url('files/gorontalo/file/fotoberita/'.$e->id . '.jpg') ?>"  alt="">
							<?= form_hidden('id', $e->id); ?>
							<?= form_hidden('ts', $e->ts); ?>
						</div>
					</div>

					<div class="row form-group">
						<label class="control-label col-sm-2">Foto</label>
						<div class="col-sm-10">
						<span class="pull-right">Format : jpg</span>
							<input type="file" name="nama" value=""  class="form-control">
						</div>
					</div>
					
				</div>
				<div class="col-sm-4">
					<div class="row form-group">
						<label class="control-label col-sm-2">Kategori</label>
						<div class="col-sm-10">
							<?php echo form_dropdown('kategori', $kategori, $e->kategori , 'class="form-control" required'); ?>
						</div>
					</div>
					<div class="row form-group">
						<label class="control-label col-sm-2">Caption</label>
						<div class="col-sm-10">
							<?php echo form_input('caption',$e->caption,'class="form-control"  placeholder="caption"'); ?>
						</div>
					</div>
					<div class="row form-group">
						<label class="control-label col-sm-2">Tampil</label>
						<div class="col-sm-10">
							<div class="col-sm-12">
								<input type="radio" name="tampil" value="1" style="margin-right: 10px;" <?= $t1 = ($e->tampil == '1' ? 'checked ' : ''); ?> >
								<label class="" style="margin-right: 20px;">Ya</label >
								
								<input type="radio" name="tampil" value="0" style="margin-right: 10px; padding-left: 10px;" <?= $t0 = ($e->tampil == '0' ? 'checked ' : ''); ?>>
								<label class="">Tidak</label>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<label class="control-label col-sm-2">Setuju</label>
						<div class="col-sm-10">
							<div class="col-sm-12">
								<input type="radio" name="setuju" value="1" checked style="margin-right: 10px;" <?= $s1 = ($e->setuju == '1' ? 'checked ' : ''); ?>>
								<label class="" style="margin-right: 20px;">Ya</label >
								
								<input type="radio" name="setuju" value="0" style="margin-right: 10px; padding-left: 10px;" <?= $s0 = ($e->setuju == '0' ? 'checked ' : ''); ?>>
								<label class="">Tidak</label>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<label class="control-label col-sm-2">Lengket</label>
						<div class="col-sm-10">
							<div class="col-sm-12">
								<input type="radio" name="lengket" value="1" checked style="margin-right: 10px;" <?= $l1 = ($e->lengket == '1' ? 'checked ' : ''); ?>>
								<label class="" style="margin-right: 20px;">Ya</label >
								
								<input type="radio" name="lengket" value="0" style="margin-right: 10px; padding-left: 10px;" <?= $l0 = ($e->lengket == '0' ? 'checked ' : ''); ?>>
								<label class="">Tidak</label>
							</div>
						</div>
					</div>
					

					<div class="row form-group">

						<div class="col-sm-12">
							<a href="<?= site_url('adminweb/berita.asp') ?>" class="btn btn-default pull-right">Kembali</a>
							<label class="col-sm-2"></label>
							<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
						</div>
					</div>
				</div>
			</div>
			
			
			

		<?php echo  form_close(); ?>
	</section>
</div>
</div>


