<div class="container-fluid">
  <section class="content-header">
    <h4>Edit Run text</h4>
  </section>
  <div class="box box-primary">
    <section class="content">
      <?php echo form_open('',array('class'=>'form-horizontal')); ?>
        <div class="col-md-8">
          <div class="row form-group">
              <label class="control-label col-sm-2">Run text</label>
              <div class="col-sm-10">
                <?php echo form_input('run_text',$edit->run_text,'class="form-control"'); ?>
              </div>
          </div>
          <div class="row form-group">
            <div class="col-sm-2"></div>
              <div class="col-sm-10">
                  <?php echo form_submit('submit','Update','class="btn btn-primary"'); ?>
                  <a href="<?php echo site_url('runningtext'); ?>" class="btn btn-info">Kembali</a>
              </div>
          </div>
        </div>
      <?php echo form_close(); ?>
    </section>
  </div>
</div>
