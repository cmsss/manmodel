<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Teksberjalan extends MX_Controller {
	private $module;
	private $redirect;

	public function __construct() {
		parent::__construct();
		if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

		$this->load->model('m_teksberjalan');	
		$this->module = 'teksberjalan';
		$this->redirect = 'adminweb/teksberjalan.asp';

	}

	public function index() {
		$idincrement = auto_inc('m_teksberjalan','id');
		if ($this->input->post()) {
			$data = array(
				'id'=> $idincrement,
				'isi'=> $this->input->post('isi'),
				'tampil' => '1',
				);
			$this->m_teksberjalan->insert($data);
			redirect($this->redirect);
		}
		$data['list'] = $this->m_teksberjalan->get_all();
		$data['module'] = $this->module;
		$data['view_file'] = "list";
		echo Modules::run('template/render_master',$data);
	}
	public function edit(){
		if ($this->input->post()) {
			$data = array(
				'isi'=> $this->input->post('isi'),
				);
			if ($this->m_teksberjalan->update($this->input->post('id'),$data)) {
				redirect($this->redirect);
			}
		}else{
			redirect($this->redirect);
		}
	}

	public function tampil_a($id){
        $cek = $this->m_teksberjalan->get_by(array('id' => $id));
        if ($cek->tampil == '1') {
            $data = array(
                 'tampil' => '0' 
                );
            $this->m_teksberjalan->update($id , $data);
            $tampil = '0';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#"  onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }elseif ($cek->tampil == '0') {
            $data = array(
                 'tampil' => '1' 
                );
            $this->m_teksberjalan->update($id , $data);
            $tampil = '1';
            if ($tampil == '1') {
                
                 echo   '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-green">Ya</small></a>';
                
            }else {
               
                    echo '<a href="#" onclick="tampilA('.$id.');"><small class="label  bg-red">Tidak</small></a>';
                
            }
        }
    }

	public function hapus($id){
		$cek = $this->m_teksberjalan->get_by(array('id' => $id));
		if ($cek) {
			$this->m_teksberjalan->delete($id);
			redirect($this->redirect);
		}else {
			redirect($this->redirect,'refresh');
		}
		
	}
	public function search(){
  // memproses hasil pengetikan keyword pada form
		$keyword = $this->input->post('term');
      $data['response'] = 'false'; //mengatur default response
      $query = $this->m_berita->get_judul($keyword); //memanggil fungsi pencarian pada model

      if (!empty($query)) {
          $data['response'] = 'true'; //mengatur response
          $data['message'] = array(); //membuat array
          foreach ($query as $row) {
              $data['message'][] = array('label' => $row->judul, 'value' => $row->judul); //mengisi array dengan record yang diperoleh
          }
      }
      //konstanta IS_AJAX
      if (IS_AJAX) {
          echo json_encode($data); //mencetak json jika merupakan permintaan ajax
      } else {
      	$data['message'][] = array('label' => $keyword, 'value' => $keyword);
      	echo json_encode($data);
      }
  }
}
