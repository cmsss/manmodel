<section class="content-header">
  <h1>
    Profile <?= $this->session->userdata('nama_lengkap'); ?>
  </h1>
</section>
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-body">
        <?= form_open_multipart('','class="form-horizontal"'); ?>
          <div class="form-group">
            <label class="col-lg-2 control-label">Nama Lengkap</label>
            <div class="col-lg-10">
              <?= form_input('nama_lengkap',$list->full_name,'class="form-control"'); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-2 control-label">Jenis Kelamin</label>
            <div class="col-lg-10">
              <?php 
                if ($list->gender == 'L'){
                  echo form_radio('gender','L',TRUE).' Laki - Laki ';
                  echo form_radio('gender','P',FALSE).' Perempuan';
                }else{
                  echo form_radio('gender','L',FALSE).' Laki - Laki';
                  echo form_radio('gender','P',TRUE).' Perempuan';
                }
              ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-2 control-label">Email</label>
            <div class="col-lg-10">
              <?= form_input('email',$list->email,'class="form-control"'); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-2 control-label">Agama</label>
            <div class="col-lg-10">
              <?= form_input('agama',$list->religion,'class="form-control"'); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-2 control-label">No HP</label>
            <div class="col-lg-10">
              <?= form_input('hp',$list->phone,'class="form-control"'); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-2 control-label">&nbsp;</label>
            <div class="col-lg-10">
              <?= form_submit('submit','Update','class="btn btn-primary"'); ?>
            </div>
          </div>
        <?= form_close(); ?>
    </div><!-- /.box-body -->
  </div><!-- /.box -->

</section>