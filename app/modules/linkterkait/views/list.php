<section class="content-header">
  <h1>Link Terkait</h1>
</section>
<section class="content">
  <?php echo form_open(); ?>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Link Text</h3>
            <div class="pull-right">
              <a href="<?php echo site_url('adminweb/linkterkait/post.asp'); ?>" class="btn btn-primary btn-sm">Tambah data</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover table-condensed" id="list_kategori">
            <thead>
              <tr>
                <td>No</td>
                <td>Judul</td>
                <td>Gambar</td>
                <td>Link Terkait</td>
                <td>Aksi</td>
              </tr>
            </thead>
            <tbody>
              <?php
              $no  = 1;
              foreach ($list as $row) {?>
              <tr>
                <td><?php echo $no++;   ?></td>
                <td><?php echo $row->nama; ?></td>
                <td><img src="<?php echo base_url('uploads/'.$row->img); ?>" height="70px" width="70px" ></td>
                <td><?php echo $row->link; ?></td>
                <td>
                  <a href="<?php echo site_url('adminweb/linkterkait/update/'.$row->id_link); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
                  <a href="<?php echo site_url('adminweb/linkterkait/delete/'.$row->id_link); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php } ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
