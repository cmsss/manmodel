<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dokumen extends MY_Model {
    
    public function __construct(){
        parent::__construct();
        parent::set_table('t_dokumen','id_dokumen');
    }

    public function  get_all_v_dokumens()
    {
    	$d = $this->db->select('*')
    			->from('v_dokumens')
    			->order_by('id_dokumen' , 'desc')
    			->get();
    	return $d->result();

    }

    public function get_by_v_dokumens($data)
    {
    	$d = $this->db->select('*')
    			->from('v_dokumens')
    			->where($data)
    			->get();
    	return $d->row();
    }
 	
 	public function insert_prosedur(){
		$data = $this->db->query("CALL insert_dokumen(? ? ? ? ? ?)");
		$result = $data->result();
 	}
}
?>