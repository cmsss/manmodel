<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dokumen
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                    <div class="pull-right">
                    	<a href="<?= site_url('adminweb/produk-kemenag/dokumen/add.asp') ?>" class="btn btn-primary" title="Tambah Data"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed" id="list_kategori">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Judul Dokumen</th>
                                    <th>Deskripsi</th>
                                    <th>Copy LINK</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($list as $row) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?= $row->main_kat; ?></td>
                                        <td><?= $row->sub_kat; ?></td>
                                        <td><?php echo $row->nama_dokumen; ?></td>
                                        <td><?php echo $row->ket_dokumen; ?></td>
                                        <td>
                                            <button class="copyyy btn btn-info" data-klik='<a href="<?= $row->link ?>"><?php echo $row->nama_dokumen; ?></a>'>Copy URl</button>
                                        </td>
                                        <td>
                                            <a href="<?= site_url('adminweb/produk-kemenag/dokumen/edit/'.$row->id_dokumen) ?>" title="edit" class="btn btn-sm btn-primary" data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></a>
                                            <a href="<?php echo site_url('adminweb/produk-kemenag/dokumen/hapus/' . $row->id_dokumen); ?>" title="hapus" class="btn btn-sm btn-warning" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i class="glyphicon glyphicon-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


    <script>

    jQuery(document).ready(function() {
        $('.copyyy').click(function() {
            var data = $(this).data('klik');
            var $temp = $("<input>");
            var tes = $("body").append($temp);
            var tes2 = $temp.val(data).select();
            document.execCommand("copy");
            $temp.remove();
            $.toaster({priority: 'info', title: 'Update', message: 'Berhail Copy URL'});

        });    
    });

 </script>