<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subkategori extends MX_Controller {

    
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_sub_kat');
        $this->load->model('m_main_kat');
        $this->load->model('m_kategory');
    }

    public function index() {
        $idincrement = auto_inc('m_sub_kat','id_sub_kat');
        if ($this->input->post()) {
            $data= array(
            'id_sub_kat'=> $idincrement,
            'sub_kat'=>$this->input->post('sub_kat'),
            'flag_sub' => url_title($this->input->post('sub_kat'),'-',true),
            'id_main_kat' => $this->input->post('main_kat'),
            );
            $this->m_sub_kat->insert($data);
            
            redirect('adminweb/produk-kemenag/subkategori.asp','refresh');
        }
        $data['main_kat'] = drop_list('m_main_kat','id_main_kat','main_kat','Pilih Kategori');
    	$data['list'] = query_sql("select * from v_sub_kategori");
        $data['module'] = "dokumen";
        $data['view_file'] = "kategori/list_sub";
        echo Modules::run('template/render_master',$data);
    }

    public function update(){
      if ($this->input->post()) {
        $id = $this->input->post('id_sub_kat');
        $data= array(
            'sub_kat'=>$this->input->post('sub_kat'),
            'flag_sub' => url_title($this->input->post('sub_kat'),'-',true),
            'id_main_kat' => $this->input->post('main_kat'),
            );
        $this->m_sub_kat->update($id ,$data);
        redirect('adminweb/produk-kemenag/subkategori.asp');
      }else{
        redirect('adminweb/produk-kemenag/subkategori.asp');
      }
    }
    public function delete($id){
        if ($this->m_sub_kat->get_by(array('id_sub_kat'=>$id))){
            if ($this->m_kategory->get_by(array('sub_kategori'=>$id))){
                $this->m_kategory->delete_by(array('sub_kategori' => $id));
            }
            $this->m_sub_kat->delete($id);
            redirect('adminweb/produk-kemenag/subkategori.asp');
        }else{
            echo 'tidak ditemukan';
        }
    }

    

}

