<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dokumen extends MX_Controller {

    
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_sub_kat');
        $this->load->model('m_dokumen');
        $this->load->model('m_mdokumen');
        $this->load->model('m_kategory');
    }

    public function index() {
        $data['list'] = $this->m_dokumen->get_all_v_dokumens();
        $data['module'] = "dokumen";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){
         $id_dokumen = auto_inc('m_dokumen','id_dokumen');
         // $id_kategori= explode(',',$this->input->post('sub_kat'));
         
        if ($this->input->post()){
            $data= array(
                'id_dokumen'=>$id_dokumen,
                'nama_dokumen'=>$this->input->post('nama_dokumen'),
                'ket_dokumen'=>$this->input->post('ket_dokumen'),
                'link'=>$this->input->post('link'),
                'id_sub_kat' => $this->input->post('id_sub_kat'),
                );
            $this->m_dokumen->insert($data);
            redirect('adminweb/produk-kemenag/dokumen.asp','refresh');

        }else {
            $data['main_kat'] = drop_list('m_main_kat','id_main_kat','main_kat','Pilih Kategori');
            $data['sub_kat'] = array(''=>'Pilih Sub Kategori');
            $data['module']="dokumen";
            $data['view_file']="add";
            echo Modules::run('template/render_master',$data);
        }
        
    }
    public function edit($id){
        if ($cek = $this->m_dokumen->get_by_v_dokumens(array('id_dokumen'=>$id))){
            // $id_kategori= explode(',',$this->input->post('sub_kat'));
            if ($this->input->post()){
                // echo "tes";
                $data= array(
                    'nama_dokumen'=>$this->input->post('nama_dokumen'),
                    'ket_dokumen'=>$this->input->post('ket_dokumen'),
                    'link'=>$this->input->post('link'),
                    'id_sub_kat' => $this->input->post('id_sub_kat'),
                    );
                // print_r($data);
                $this->m_dokumen->update($id,  $data);
                redirect('adminweb/produk-kemenag/dokumen.asp','refresh');

            }else {
                $edit = query_sql("select * from v_dokumen where id_dokumen = '$id'","row");
                $data['edit'] = $cek;
                $data['main_kat'] = drop_list('m_main_kat','id_main_kat','main_kat','Pilih Kategori');
                $data['sub_kat']  = drop_list('m_sub_kat' ,'id_sub_kat' , 'sub_kat' , '-Pilih Sub Kategory','get_many_by' , array('id_main_kat' => $cek->id_main_kat));
                
                // $data['sub_kat'] = $list_sub;
                $data['module']="dokumen";
                $data['view_file']="edit";
                echo Modules::run('template/render_master',$data);
            }
            
        }else{
            redirect('adminweb/produk-kemenag/dokumen.asp' , 'refresh');
        }
    }
    public function delete($id){
        if ($this->m_dokumen->get_by(array('id_dokumen'=>$id))){
            $this->m_dokumen->delete($id);
             redirect('adminweb/produk-kemenag/dokumen.asp','refresh');
        }
        redirect('adminweb/produk-kemenag/dokumen.asp','refresh');
    }



    public function get_sub_kategori()
    {
        $data = $this->input->get('data');

        $tes = array('id_main_kat' => $data);
        $drop = drop_list('m_sub_kat' ,'id_sub_kat' , 'sub_kat' , '-Pilih Sub Kategory','get_many_by' , $tes);

        echo form_dropdown('id_sub_kat',$drop,'','class="form-control"   required ');
    }

    public function grab_sub(){
        if ($this->input->post()){
            $id = $this->input->post('id_main');
            $result = query_sql("select * from v_kategori where id_main_kat = '$id'");
            if (!empty($result)) {
                // jika hasil query array maka looping hasil query
                foreach ($result as $row) {
                    echo '<option value="'. $row->id_sub_kat.','.$row->id_kategori. '">' . $row->sub_kat. '</option>';
                }
            } else {
                echo '<option value="">Tidak ada data</option>';
            }
        }
    }

}
