<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kategori extends MX_Controller {

    
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        // $this->load->model('m_main_kat');
        // $this->load->model('m_kategory');
        $this->load->model('m_kategori1');
    }

    public function index() {
        $idincrement = auto_inc('m_kategori1','id');
        if ($this->input->post()) {
            $data= array(
            'id_main_kat'=> $idincrement,
            'main_kat'=>$this->input->post('main_kat'),
            'flag_main' => url_title($this->input->post('main_kat'),'-',true)
            );
            $this->m_main_kat->insert($data);
            redirect('adminweb/produk-kemenag/kategori.asp','refresh');
        }
    	$data['list'] = $this->m_main_kat->get_all();
        $data['module'] = "dokumen";
        $data['view_file'] = "kategori/list";
        echo Modules::run('template/render_master',$data);
    }

    public function update(){
      if ($this->input->post()) {
        $data= array(
            'main_kat'=>$this->input->post('main_kat'),
            'flag_main' => url_title($this->input->post('main_kat'),'-',true)
            );
        if ($this->m_main_kat->update($this->input->post('id_main_kat'),$data)) {
            redirect('adminweb/produk-kemenag/kategori.asp');
        }
      }else{
        redirect('adminweb/produk-kemenag/kategori.asp');
      }
    }
    public function delete($id){
        if ($this->m_main_kat->get_by(array('id_main_kat'=>$id))){
            if ($this->m_kategory->get_by(array('kategori'=>$id))){
                $this->m_kategory->delete_by(array('kategori' => $id));
            }
            $this->m_main_kat->delete($id);
        }
        redirect('adminweb/produk-kemenag/kategori.asp');
    }

    

}

