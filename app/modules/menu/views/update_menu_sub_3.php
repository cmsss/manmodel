<?php 
$url = $lu->url;
$pecah = explode('punyaku/', $url);
if (!empty($pecah[1])) {
  $urlGet = base_url().$pecah[1];
}else{
  $urlGet = $url;
}

?>

<div class="modal-body">
  <?php echo form_open('' ,'id="dataDataSub3'.$lu->id.'"');?>
  <div class="form-group">
    <label for="recipient-name" class="control-label">Nama Menu</label>
    <?php echo form_input('nama',$lu->nama ,'class="form-control " required id="nama'.$lu->id.'"');?>
  </div>
  <div class="form-group">
    <label for="recipient-name" class="control-label">Url</label>
    <?php echo form_input('url',$urlGet ,'class="form-control " required id="url'.$lu->id.'"');?>
  </div>
</div>
<div class="modal-footer">
  <div id="simpanData<?php echo $lu->id; ?>">
    <button type="button"  onclick="simpanDataUpdateSub2('<?php echo $lu->id ?>');" class="btn btn-primary">Simpan</button>
  </div>
  <?php echo form_close();?>

</div>

<script>
      function simpanDataUpdateSub2(datai){
        var datai = datai;
        var data = $('#dataDataSub3'+datai).serialize();
        data += '&id=' +datai;
        // console.log(data);

        // alert(datai);
        if ($("#nama"+datai).val() == '') {
            $.toaster({priority: 'info', title: 'Perhatian!', message: 'Text Tidak Boleh Kosong'});
            $('#nama'+datai).focus();
        } 
        else if ($("#url"+datai).val() == '') {
            $.toaster({priority: 'info', title: 'Perhatian!', message: 'Url Tidak Boleh Kosong'});
            $('#url'+datai).focus();
        } 
         else {

            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('cpanel/menu/update-sub3') ?>",
                data: data,
                dataType: 'html',
                beforeSend: function() {
                    // setting a timeout
                    $("#simpanData"+datai).html('Loading ...');
                },
                success: function (status) {
                     
                    $.toaster({priority: 'success', title: 'Success', message: 'Berhasil update Data'});
                    $(".modal").modal('hide');
                    $('.modal-backdrop').hide();
                    // $("#lsitBaru").html(status);
                    $("#simpanData"+datai).html('<button type="button"  onclick="simpanDataUpdate('+datai+');" class="btn btn-primary">Simpan</button>');
                    $("#subMenu3").html(status);
                    // console.log(status);
                     // window.location = "<?php echo site_url('adminweb/menu.asp') ?>";

                },
                error: function () {
                    $.toaster({priority: 'error', title: 'Error', message: 'Gagal Updatte Data'});
                    $("#simpanData"+datai).html('<button type="button"  onclick="simpanDataUpdate('+datai+');" class="btn btn-primary">Simpan</button>');
                }

            });
        }
    }
</script>