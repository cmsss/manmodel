<div class="box box-warning">
	<div class="box-header">
		<i class="ion ion-clipboard"></i>
		<h3 class="box-title"><?php echo $judul2;?></h3>
		<!-- <button type="button" class="btn btn-sm btn-primary pull-right" onclick="viewTambah()" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-fw fa-plus"></i> <?php //echo $judul1; ?></button> -->
		

	</div><!-- /.box-header -->
	<div class="box-body">
		<div class="table-responsive">
			<table class="table table-striped table-hover table-condensed table-bordered">
				<thead>
					<tr>
						<th style="width: 5%;">No</th>
						<th>Nama</th>
						<!-- <th>Url</th> -->
						<!-- <th width="1%">Status</th> -->
						<th width="30%">#</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1;  if (!empty($list)): ?>
						<?php foreach ($list as $l): ?>
							<?php 
							  $url = $l->url;
							  $pecah = explode('punyaku/', $url);
							  if (!empty($pecah[1])) {
							      $urlGet = base_url().$pecah[1];
							  }else{
							      $urlGet = $url;
							  }

							?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $l->nama ?></td>
								<!-- <td><?php echo $urlGet ?></td> -->
								<!-- <td align="center">
									<?php 
										// if ($l->status == '1'){
										// 	$chek = "checked";
										// }else{
										// 	$chek = "";
										// }
									 ?>
									<div class="form-group">
					                      <input  id="chek<?php //echo $l->id?>" onchange="cekSatatus('<?php //echo $l->id?>')" type="checkbox"  <?php //echo $chek;?> />
					                    								                    
					                  </div>
								</td> -->
								<td align="center">
                                    <div class="btn-group">
                                        <!-- <button type="button" class="btn btn-xs btn-success" onclick="viewListSub3('<?php echo $l->id ?>')"><i class="fa fa-bars"></i></button> -->
                                        <!-- <button type="button" class="btn btn-xs btn-primary" onclick="viewTambahSub3('<?php echo $l->id ?>')" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i></button> -->
										<button type="button" class="btn btn-xs btn-warning" onclick="viewUpdateSub2('<?php echo $l->id ?>')" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-pencil"></i></button>
										<button type="button" class="btn btn-xs btn-danger" onclick="haspusData('<?php echo $l->id; ?>');" ><i class="fa fa-trash"></i></button>
                                    </div>
								</td>
							</tr>
						<?php endforeach ?>
					<?php endif ?>
				</tbody>
			</table>
		</div>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix no-border">

	</div>
</div><!-- /.box -->

<script>
	function viewListSub3(datai){
        $.ajax({
              type: 'GET',
              url: "<?php echo site_url('cpanel/menu/list-sub3') ?>",
              data: {data : datai},
              dataType: 'html',
              beforeSend: function() {
                  // setting a timeout
                  $("#subMenu3").html('Loading ...');
              },
              success: function (status) {
                  $("#subMenu3").html(status);

              },
              error: function () {
                  $.toaster({priority: 'error', title: 'Error', message: 'Gagal'});                  
              }

          });
    }


    function viewTambahSub3(datai){
        $.ajax({
            url: '<?php echo site_url('cpanel/menu/vtambah-sub-3')?>',
            type: 'GET',
            dataType: 'html',
            data: {data: datai},
            beforeSend : function (){
                var html = '<div class="overlay">'+
                    '<i class="fa fa-refresh fa-spin"></i>'+
                    '</div>';

                $("#viewForm").html(html);
                $("#formApa").text('Tambah Sub');
            },
            success  : function (get){
                $("#viewForm").html(get);
            },
            error : function(){
                console.log('error');
            }
        });     
        
    }

    function viewUpdateSub2(datai){
        $.ajax({
            url: '<?php echo site_url('cpanel/menu/vupdate-sub-2')?>',
            type: 'GET',
            dataType: 'html',
            data: {data: datai},
            beforeSend : function (){
                var html = '<div class="overlay">'+
                    '<i class="fa fa-refresh fa-spin"></i>'+
                    '</div>';

                $("#viewForm").html(html);
                $("#formApa").text('Update Sub');
            },
            success  : function (get){
                $("#viewForm").html(get);
            },
            error : function(){
                console.log('error');
            }
        });     
        
    }
</script>