<div class="modal-body">
  <?php echo form_open('' ,'id="dataDataSub3"');?>
  <div class="form-group">
    <label for="recipient-name" class="control-label">Nama SubMenu <?php echo $lu->nama?></label>
    <input type="hidden" name="parent" id="parent" value="<?php echo $lu->id ?>">
    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama SubMenu <?php echo $lu->nama?>">
  </div>
  <div class="form-group">
    <label for="recipient-name" class="control-label">URL</label>
    <input type="text" name="url" class="form-control" id="url" placeholder="Url">
  </div>

</div>
<div class="modal-footer">
  <div id="simpanData">
    <button type="button"  onclick="simpanDataSub3('<?php echo $lu->id?>');" class="btn btn-primary">Simpan</button>
  </div>
  <?php echo form_close();?>
</div>

<script>
    function simpanDataSub3(){
      var data = $('#dataDataSub3').serialize();
      if ($("#nama").val() == '') {
          $.toaster({priority: 'info', title: 'Perhatian!', message: 'Text Tidak Boleh Kosong'});
          $('#nama').focus();
      } 

      else if ($("#url").val() == '') {
          $.toaster({priority: 'info', title: 'Perhatian!', message: 'Url Tidak Boleh Kosong'});
          $('#url').focus();
      }
       else {

          $.ajax({
              type: 'POST',
              url: "<?php echo site_url('cpanel/menu/simpan-sub-3') ?>",
              data: data,
              dataType: 'html',
              beforeSend: function() {
                  // setting a timeout
                  $("#simpanData").html('Loading ...');
              },
              success: function (status) {
                 
                  $.toaster({priority: 'success', title: 'Success', message: 'Berhasil Simpan data'});
                  $(".modal").modal('hide');
                  $('.modal-backdrop').hide();
                  // $("#lsitBaru").html(status);
                  $("#medsos").val('');
                  $("#simpanData").html('<button type="button"  onclick="simpanData();" class="btn btn-primary">Simpan</button>');
                  // window.location = "<?php echo site_url('adminweb/menu.asp') ?>";
                  $("#subMenu3").html(status);

              },
              error: function () {
                  $.toaster({priority: 'error', title: 'Error', message: 'Gagal Tambah Data'});
                  $("#simpanData").html('<button type="button"  onclick="simpanData();" class="btn btn-primary">Simpan</button>');
              }

          });
      }
    }
</script>