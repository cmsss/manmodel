<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_menu extends MY_Model {
    
    public function __construct() {
        parent::__construct();
        parent::set_table('menu', 'id');
    }

    public function get_all_s($hapus = ''){
    	$data = $this->db->select('*')
    			->from('menu')
    			->where(array('hapus' => $hapus , 'parent' => '0' ))
    			->get();
    	return $data->result();
    }

    public function get_all_sub($data = array()){
        $data = $this->db->select('*')
                ->from('menu')
                ->where(array('hapus' => '' ))
                ->where($data)
                ->get();
        return $data->result();
    }

    public function get_aktif($start = 0 , $limit = 5 , $return = 'result')
    {
        $data = $this->db->select('*')
                ->from('menu')
                ->where(array('hapus' => '0'  , 'status' => '1' ))
                ->limit($limit , $start)
                ->get();
        return $data->$return();
    }


    function sub_menu($id_menu)
	{
	    $d = $this->db->query("SELECT a.* , b.menu as kop FROM t_menu a , t_menu b WHERE a.parent_id = '$id_menu' AND b.id_menu = '$id_menu' ORDER BY a.id_menu asc");
	    return $d->result();
	}


	function get_by_sub_menu($id_menu)
	{
		 $d = $this->db->query("SELECT a.* , b.menu as kop FROM t_menu a , t_menu b WHERE a.parent_id = '$id_menu' AND b.id_menu = '$id_menu' ORDER BY a.id_menu asc");
	    	return $d->row();
	}


   

    
}
?>