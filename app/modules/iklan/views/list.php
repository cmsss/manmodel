  <section class="content-header">
    <h1></i>Iklan</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Iklan</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('iklan/add'); ?>" class="btn btn-info">Tambah</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>

			<table class="table table-hover table-condensed" id="list_kategori">
				<thead>
					<tr>
						<td>No</td>
						<td>Iklan</td>
						<td>File</td>
						<td>aksi</td>
					</tr>
				</thead>
				<tbody>

					<?php
						$no=1;
						foreach ($list as $row) {?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $row->iklan; ?></td>
						<td><img src="<?php echo base_url('uploads/'.$row->file); ?>" width="70px" height="70px" ></td>
						<td>
							<a href="<?php echo site_url('iklan/edit/'.$row->id_iklan); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
							<a href="<?php echo site_url('iklan/hapus/'.$row->id_iklan); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					<?php } ?>

				</tbody>
			</table>
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
