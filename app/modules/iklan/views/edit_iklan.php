<div class="container-fluid">
  <section class="content-header">
    <h4>Edit Iklan</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">
			<?php echo form_open_multipart('',array('class'=>'form-horizontal')); ?>
				<div class="row form-group">
						<label class="control-label col-sm-2">Iklan</label>
						<div class="col-sm-10">
							<?php echo form_input('iklan',$edit->iklan,'class="form-control" placeholder="Iklan"'); ?>
						</div>
				</div>
				<!-- <div class="row form-group">
						<label class="control-label col-sm-2">Posisi</label>
						<div class="col-sm-10">
							<?php //echo form_dropdown('posisi',$posisi,$edit->posisi,'class="form-control" required'); ?>
						</div>
				</div> -->
				<div class="row form-group">
						<label class="control-label col-sm-2">File</label>
						<div class="col-sm-10">
							<input class="form-control" name="file" type="file"/>
						</div>
				</div>
				<?php echo $this->session->flashdata('pesan'); ?>
				<div class="row form-group">
						<label class="control-label col-sm-2"></label>
						<div class="col-sm-10">
						<a href="<?= site_url('adminweb/iklan.asp') ?>" class="btn btn-default">Kembali</a>
							<?php echo form_submit('submit','Edit Iklan','class="btn btn-primary"'); ?>
						</div>
				</div>
				
			<?php echo  form_close(); ?>
		</section>
	</div>
</div>

