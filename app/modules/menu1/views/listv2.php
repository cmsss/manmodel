<?php

function menu_kemenag(){
    $d = query_sql("SELECT * FROM t_menu WHERE parent_id = '0' ORDER BY id_menu asc" );
    return $d;
}


function sub_menu($id_menu)
{
    $d = query_sql("SELECT * FROM t_menu WHERE parent_id = '$id_menu' ORDER BY id_menu asc");
    return $d;
}



?>


<section class="content-header">
  <h1>
    Menu
    <small>List menu, menambah menu, mengedit menu</small>
  </h1>
</section>
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List Menu</h3>
      <div class="box-tools pull-right">
        <a href="javascript::0" data-toggle="modal" data-target="#addmenu" class="btn btn-primary"><i class="fa fa-plus"></i> Menu</a>
      </div>
    </div>
    <div class="box-body">
      <div class="table-responsive">
      <!-- id="list_kategori" -->
        <table class="table table-condensed table-bordered table-hover table-striped" >
          <thead>
            <tr>
              <td>No</td>
              <td>Menu</td>
              <td>Url</td>
              <td>Aksi</td>
            </tr>
          </thead>
          <tbody>
          
            <?php 
              $no=1;
              foreach ($list as $row) {?>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $row->menu; ?></td>
              <td><?= $row->url; ?></td>
              <td>
                <a href="javascript::0" data-toggle="modal" data-target="#editmenu<?= $row->id_menu ?>" class="btn btn-info">Edit</a>
                <a href="javascript::0" data-toggle="modal" data-target="#tamsub1<?= $row->id_menu ?>" class="btn btn-primary"><i class="fa fa-plus"></i> SUB</a>
                <a href="<?php echo site_url('adminweb/menu/hapus/'.$row->id_menu); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-danger">Hapus</a>
                <?php if (sub_menu($row->id_menu)): ?>
                  <a href="<?php echo site_url('adminweb/menu/sub-menu/'.$row->id_menu); ?>"  class="btn btn-warning">List</a>
                <?php endif ?>
                 
              </td>
             
            </tr>

            
            <!-- tambah Modal 2 -->
            <div class="modal fade" id="tamsub1<?= $row->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Tambah Menu Dari Parent <b><?= $row->menu ?></b></h4>
                  </div>
                  <?= form_open(site_url('adminweb/menu/tambah1edit.asp'),'class="form-horizontal"'); ?>
                  <div class="modal-body">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Menu</label>
                      <div class="col-md-9">
                        <?= form_input('menu','','class="form-control" placeholder="Nama Menu" required'); ?>
                        <?= form_hidden('id',$row->id_menu); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Url</label>
                      <div class="col-md-9">
                        <?= form_input('url','','class="form-control" placeholder="Url Menu" required'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Parent</label>
                      <div class="col-md-9">
                        <?= form_dropdown('parent_id', array('' => $row->menu),'', ' class="form-control" readonly disabled'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                  </div>
                  <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>
                  <?= form_close(); ?>
                </div>
              </div>
            </div>
            <!-- tambah Modal 2 -->
            

            <!-- edit Modal 1 -->
            <div class="modal fade" id="editmenu<?= $row->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
                  </div>
                  <?= form_open(site_url('adminweb/menu/edit.asp'),'class="form-horizontal"'); ?>
                  <div class="modal-body">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Menu</label>
                      <div class="col-md-9">
                        <?= form_input('menu',$row->menu,'class="form-control" placeholder="Nama Menu" required'); ?>
                        <?= form_hidden('id',$row->id_menu); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Url</label>
                      <div class="col-md-9">
                        <?= form_input('url',$row->url,'class="form-control" placeholder="Url Menu" required'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    
                  </div>
                  <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>
                  <?= form_close(); ?>
                </div>
              </div>
            </div>
            <!-- edit Modal 1 -->
            <?php } ?>
            
          </tbody>
        </table>        
      </div>
        
    </div><!-- /.box-body -->
  </div><!-- /.box -->

</section><!-- /.content -->


<!-- Modal -->
<div class="modal fade" id="addmenu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Menu</h4>
      </div>
      <?= form_open('','class="form-horizontal"'); ?>
      <div class="modal-body">
        <div class="form-group">
          <label class="col-md-3 control-label">Menu</label>
          <div class="col-md-9">
            <?= form_input('menu','','class="form-control" placeholder="Nama Menu" required'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Url</label>
          <div class="col-md-9">
            <?= form_input('url','','class="form-control" placeholder="Url Menu" required'); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>