<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends MX_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        if ($this->autentifikasi->role(array('1')))
            redirect('adminpage/site-login.asp','refresh');
    }

    public function index() {
    	if ($this->input->post()){
            $data = array(
                'id_menu' => auto_inc('m_menu','id_menu'),
                'menu' => $this->input->post('menu'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon')
            );
            $this->m_menu->insert($data);
            redirect('adminweb/menu.asp','refresh');
        }
    	$data['list'] = $this->m_menu->get_many_by(array('parent_id' =>  '0') , 'asc');
        $data['module'] = "menu";
        $data['view_file'] = "listv2";
        echo Modules::run('template/render_master',$data);
    }


    public function sub1($id_menu){
        $cek = $this->m_menu->get_by_sub_menu($id_menu);
        if ($cek) {
            $data['cek'] = $cek;
            $data['list'] = $this->m_menu->sub_menu($id_menu);

            $data['view_file'] = 'sub1';
            $data['module']  = 'menu';
            echo Modules::run('template/render_master' ,$data);

        }else{
            redirect('adminweb/menu.asp','refresh');
        }
    }


    public function tambah1edit()
    {
        if ($this->input->post()){
        $id = $this->input->post('id');
            if ($this->m_menu->get_by(array('id_menu'=>$id))){
                $data = array(
                    'id_menu' => auto_inc('m_menu','id_menu'),
                    'menu' => $this->input->post('menu'),
                    'url' => $this->input->post('url'),
                    'parent_id' => $id,
                );
                $this->m_menu->insert($data);
                redirect('adminweb/menu.asp','refresh');
            }
            redirect('adminweb/menu.asp','refresh');
        }
        redirect('adminweb/menu.asp','refresh');
    }

   
    public function edit(){
       if ($this->input->post()){
        $id = $this->input->post('id');
            if ($this->m_menu->get_by(array('id_menu'=>$id))){
                $data = array(
                    'menu' => $this->input->post('menu'),
                    'url' => $this->input->post('url'),
                );
                $this->m_menu->update($id,$data);
                redirect('adminweb/menu.asp','refresh');
            }
            redirect('adminweb/menu.asp','refresh');
        }
        redirect('adminweb/menu.asp','refresh');
    }
    public function delete($id){
        if ($this->m_menu->get_by(array('id_menu'=>$id))){
            $cek = $this->m_menu->get_by(array('parent_id' => $id));
            if ($cek) {
                // echo "Maaf Menu Ini Tidak Bisa Di hapus Karena Memiliki Sub menu";
                echo "<script>alert('Maaf Menu Ini Tidak Bisa Di hapus Karena Memiliki Sub menu');
                window.location=('".site_url('adminweb/menu.asp')."');</script>";
            }else {
                // echo "berhasil Hapus";
                $this->m_menu->delete($id);
                echo "<script>alert('Menu Berhasil Dihapus');
                window.location=('".site_url('adminweb/menu.asp')."');</script>";

            }
        }
        // redirect('adminweb/menu.asp','refresh');
    }

    

}
