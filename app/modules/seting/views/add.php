<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">

		<?php echo form_open_multipart('',array('class'=>'form-horizontal')); ?>
			<div class="row form-group">
				<label class="control-label col-sm-2">keterangan</label>
				<div class="col-sm-10">
					<?php echo form_input('ket',set_value('ket'),'class="form-control" placeholder="Keterangan"required'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Gamabar</label>
				<div class="col-sm-10">
					<span class="small pull-right"><i style="color: red;">Format PNG</i></span>
					<input type="file" name="img" required="required" class="form-control">
					<?php //echo form_input('img',set_value('img'),'class="form-control" placeholder="Gamabar"required'); ?>
				</div>
			</div>
			<div class="row form-group">
					
					<div class="col-sm-12">
						<a href="<?= site_url('adminweb/seting.asp') ?>" class="btn btn-default pull-right">Kembali</a>
						<label class="col-sm-2"></label>
						<?php echo form_submit('submit','Tambah','class="btn btn-primary "'); ?>
					</div>
			</div>
		<?php echo  form_close(); ?>
		</section>
	</div>
</div>


