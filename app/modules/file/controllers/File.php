<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class File extends MX_Controller {

    private $module;

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

        $this->load->model('m_fm');
        $this->module = "file";

    }

    public function index() {
        $data['list']   = $this->m_fm->get_all();
        $data['module'] = $this->module;
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

    public function tambah()
    {
        $data['module'] = $this->module;
        $data['view_file']   = 'tambah';
        echo Modules::run('template/render_master' , $data);
    }

    function config_file($data){

    }

    public function simpan()
    {
        if ($this->input->post()) {
            
            $tipenyafile = pathinfo($_FILES["nama"]['name'] ,PATHINFO_EXTENSION );
            $namanyafile = pathinfo($_FILES["nama"]['name']  ,PATHINFO_BASENAME);
            
            // die(print_r($tipenyafile));


             $folder = str_replace( array(':' , ' '), '', $this->input->post('dir'));
            // if ($tipenyafile == 'jpg') {
            //     $path = './files/file/foto/'.$folder;    
            
            //     if (!is_dir($path)) { //create the folder if it's not already exists
            //         mkdir($path, 0777, TRUE);
            //     }
            // }elseif ($tipenyafile == 'pdf' || $tipenyafile == 'doc' || $tipenyafile == 'docx' || $tipenyafile == 'ppt' || $tipenyafile == 'pptx' || $tipenyafile == 'rar' || $tipenyafile == 'zip' ) {
            $path = './files/gorontalo/file/file/'.$folder;    
        
            if (!is_dir($path)) { //create the folder if it's not already exists
                mkdir($path, 0777, TRUE);
            //     }
            // }}
            }

            
           
            // chmod('./files/gorontalo/file/', 0777);
            
          

            // pdf/doc/docx/ppt/pptx/zip/rar/jpg
            $nmfile = str_replace(array(':' , ' '), '-',$namanyafile);
            $config['upload_path']= $path;
            $config['allowed_types']="pdf|doc|docx|ppt|pptx|rar|jpg|zip";
            $config['max_size']="10240000";
            // $config['max_width']="0";
            // $config['max_height']="0";
            $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            $this->upload->initialize($config);


         
            $idincrement = auto_inc('m_fm','id');
            if ($this->upload->do_upload('nama')) {
                $file = $this->upload->data();
            
                // die(print_r($file));
                $idincrement = auto_inc('m_fm', 'id');
                $data= array(
                    'id'=> $idincrement,
                    'nama'=>$file['file_name'],
                    'asli' =>$file['file_name'],
                    'ukuran' => $file['file_size'],
                    'mime' => $file['file_type'],
                    'tanggal' => date('Y-m-d'),
                    'domain' => NAMA_DOMAIN,
                    'dir' => $this->input->post('dir'),
                    'iduser' => $this->session->userdata('id_login'),
                    );
                $this->m_fm->insert($data);
                    echo "<script>alert('Berhasil Upload File');
            window.location=('" . site_url('adminweb/file.asp') . "');</script>";
                }else{
                     echo "<script>alert('Gagal upload');
            window.location=('" . site_url('adminweb/file.asp') . "');</script>";
                }
        }
    }



    public function  hapus($id)
    {
        $cek = $this->m_fm->get_by(array('id' => $id));
        if ($cek) {
            
            $tipenyafile = pathinfo($cek->asli ,PATHINFO_EXTENSION );
            // die(print_r($tipenyafile));  


            // if ($tipenyafile == 'jpg') {
            //     $patfile = realpath(APPPATH . '../files/file/foto/' . $cek->dir);    
            
            // }elseif ($tipenyafile == 'pdf' || $tipenyafile == 'doc' || $tipenyafile == 'docx' || $tipenyafile == 'ppt' || $tipenyafile == 'pptx' || $tipenyafile == 'rar' || $tipenyafile == 'zip' ) {
            $patfile = realpath(APPPATH . '../files/gorontalo/file/file/' . $cek->dir);
            // }else {
            //     redirect('adminweb/file.asp','refresh');
            // }


            if ($cek->asli != '') {
                unlink($patfile . '/' . $cek->asli);
            }
            $this->m_fm->delete($id);
            redirect('adminweb/file.asp','refresh');
        }else{
            redirect('adminweb/file.asp','refresh');
        }
    }

}