<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah File</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">

		<?php echo form_open_multipart('adminweb/file/simpan',array('class'=>'form-horizontal')); ?>
			<div class="row form-group">
					<label class="control-label col-sm-2">Direktori</label>
					<div class="col-sm-10">
						<?php echo form_input('dir',set_value('dir'),'class="form-control" required placeholder="direktory"'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">File</label>
					<div class="col-sm-10">
						<input type="file" name="nama" value="" required="required" class="form-control">
					</div>
			</div>
			<div class="row form-group">
					
					<div class="col-sm-12">
						<a href="<?= site_url('adminweb/file.asp') ?>" class="btn btn-default pull-right">Kembali</a>
						<label class="col-sm-2"></label>
						<?php echo form_submit('submit','Tambah File','class="btn btn-primary "'); ?>
					</div>
			</div>
		<?php echo  form_close(); ?>
		</section>
	</div>
</div>


