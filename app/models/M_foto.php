<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_foto extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('foto', 'id');
    }

    public function get_slide_foto($limit = 5){
        $d = $this->db->query("SELECT * FROM foto WHERE setuju = '1' ORDER BY ts DESC LIMIT 0 , $limit");
        return $d->result();
    }



    public function get_page($limit, $start) {
        $this->db->limit($limit, $start);
        return $this->get_all_v_k();
    }

    public function get_all_v_k()
    {
        $d = $this->db->select('a.* , b.nama as nama_kategori , c.nama as foto , c.tanggal')
                ->from('foto a')
                ->join('kategori b' , 'a.kategori = b.id' , 'left')
                ->join('fm c' , 'a.ts = c.id' , 'left' )
                ->where('a.setuju' , '1')
                ->order_by('a.ts' ,'desc')
                ->get();
        return $d->result();
    }


    public function get_all_v()
    {
    	$d = $this->db->select('a.* , b.nama as nama_kategori , c.nama as foto')
    			->from('foto a')
    			->join('kategori b' , 'a.kategori = b.id' , 'left')
    			->join('fm c' , 'a.ts = c.id' , 'left' )
    			->order_by('a.id' ,'desc')
    			->get();
    	return $d->result();
    }

    public function get_by_v($data)
    {
    	$d = $this->db->select('a.* , b.nama as nama_kategori , c.nama as foto')
    			->from('foto a')
    			->join('kategori b' , 'a.kategori = b.id' , 'left')
    			->join('fm c' , 'a.ts = c.id' , 'left' )
    			->where($data)
    			->get();
    	return $d->row();
    }




    

}
