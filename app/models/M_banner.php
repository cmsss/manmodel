<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_banner extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('banner', 'id');
    }


     public function get_limit($limit) {
        $this->db->limit($limit);
        return parent::get_many_by(array('tampil' => '1'));
    }


    public function get_page($limit, $start) {
        $this->db->limit($limit, $start);
        return $this->get_all_v_k();
    }

    public function get_all_v_k()
    {
        $this->db->where(array('tampil' => '1' , 'grup' => '10'));
    	return parent::get_all();
    }



    public function get_limit_g_10($limit) {
        $this->db->limit($limit);
        return parent::get_many_by(array('tampil' => '1' , 'grup' => '10'));
    }
    

    

}
