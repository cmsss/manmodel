<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_audio extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('audio', 'id');
    }

    public function get_page($limit, $start) {
        $this->db->limit($limit, $start);
        return $this->get_all_v_k();
    }

    public function get_all_v_k()
    {
        $d = $this->db->select('a.* , b.nama as nama_kategori , c.nama as audio , c.tanggal')
                ->from('audio a')
                ->join('kategori b' , 'a.kategori = b.id' , 'left')
                ->join('fm c' , 'a.ts = c.id' , 'left' )
                ->where('a.setuju' , '1')
                ->order_by('c.tanggal' ,'desc')
                ->get();
        return $d->result();
    }

    public function get_all_v()
    {
    	$d = $this->db->select('a.* , b.nama as nama_kategori , c.nama as audio')
    			->from('audio a')
    			->join('kategori b' , 'a.kategori = b.id' , 'left')
    			->join('fm c' , 'a.ts = c.id' , 'left' )
    			->order_by('c.tanggal' ,'desc')
    			->get();
    	return $d->result();
    }

    public function get_by_v($data)
    {
    	$d = $this->db->select('a.* , b.nama as nama_kategori , c.nama as audio')
    			->from('audio a')
    			->join('kategori b' , 'a.kategori = b.id' , 'left')
    			->join('fm c' , 'a.ts = c.id' , 'left' )
    			->where($data)
    			->get();
    	return $d->row();
    }

    

}
