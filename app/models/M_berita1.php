<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_berita1 extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('berita', 'id');
    }

    public function get_page($limit, $start) {
        $this->db->limit($limit, $start);
        return $this->get_all_v_k();
    }

    public function get_all_v_k()
    {
        $d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
                ->from('berita a ')
                ->join('fm b', 'a.ts = b.id' ,'left')
                ->join('kategori c' , 'a.kategori =  c.id' , 'left' )
                ->where('hapus' , '0')
                ->where(array('a.setuju' => '1'))
                // ->order_by('a.tgl_input' , 'desc')
                ->order_by('a.ts' ,'desc')
                ->get();
        return $d->result();
    }

    public function get_all_v(){
    	$d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
    			->from('berita a ')
    			->join('fm b', 'a.ts = b.id' ,'left')
    			->join('kategori c' , 'a.kategori =  c.id' , 'left' )
    			->where(array('a.hapus' => '0'))
    			// ->order_by('a.tgl_input' ,'desc')
                ->order_by('a.ts' ,'desc')
    			->get();
    	return $d->result();
    }

     public function get_all_v_w_u($id_penulis){
        $d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
                ->from('berita a ')
                ->join('fm b', 'a.ts = b.id' ,'left')
                ->join('kategori c' , 'a.kategori =  c.id' , 'left' )
                ->where(array('a.hapus' => '0'))
                ->where('a.iduser'  , $id_penulis)
                // ->order_by('a.tgl_input' ,'desc')
                ->order_by('a.ts' ,'desc')
                ->get();
        return $d->result();
    }
    

    public function get_by_v($data)
    {
    	$d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
    			->from('berita a ')
    			->join('fm b', 'a.ts = b.id' ,'left')
    			->join('kategori c' , 'a.kategori =  c.id' , 'left' )
    			->where($data)
    			->get();
    	return $d->row();
    }

    public function get_many_by_v($data , $limit = 50)
    {
        $d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
                ->from('berita a ')
                ->join('fm b', 'a.ts = b.id' ,'left')
                ->join('kategori c' , 'a.kategori =  c.id' , 'left' )
                ->where('hapus' , '0')
                ->where($data)
                // ->order_by('a.tgl_input' , 'desc')
                ->order_by('a.ts' ,'desc')
                ->limit($limit)
                ->get();
        return $d->result();
    }

    public function  gat_all_terpopuler($limit = 10)
    {
        $d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
                ->from('berita a ')
                ->join('fm b', 'a.ts = b.id' ,'left')
                ->join('kategori c' , 'a.kategori =  c.id' , 'left' )
                ->where(array('a.hapus' => '0' , 'a.setuju' => '1'))
                ->order_by('a.counters' ,'desc')
                ->limit($limit)
                ->get();
        return $d->result();
    }

    public function get_all_v_kategori($like ,$limit = 3)
    {
       $d = $this->db->query("SELECT * FROM berita WHERE kategori LIKE '$like' AND hapus = '0' AND tampil = '1' AND setuju = '1'  ORDER BY ts DESC LIMIT 0 , $limit");
       return $d->result();
    }


    public function get_all_v_kategori_kanwil($limit = 3){
        $d = $this->db->query("SELECT * FROM berita WHERE kategori NOT LIKE 'Bonbol' AND kategori NOT LIKE 'Gor' AND kategori NOT LIKE 'Kabgor' AND kategori NOT LIKE 'Boa' AND kategori NOT LIKE 'Poh' AND kategori NOT LIKE 'Gorut' AND  hapus = '0' AND tampil = '1' AND setuju = '1'  ORDER BY ts DESC LIMIT 0 , $limit");

        return $d->result();
    }



    public function get_all_v_k_berita_lainya($id , $limit = 5)
    {

        $d  =   $this->db->query("SELECT * FROM berita WHERE hapus = '0' AND setuju = '1' AND id != '$id' ORDER BY ts DESC LIMIT 0 , $limit");
        return $d->result();
        
    }

        // ""
    
}
